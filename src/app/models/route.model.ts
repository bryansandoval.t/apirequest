import { SidenavItem } from "../modules/core/sidenav/sidenav-item/sidenav-item.model";

export class Route {
    constructor(
        public id?: string,
        public name = '',
        public route = {},
        public created_at?: string,
        public updated_at?: string,
        public deleted_at?: string
    ) {
    }

}

export const dashboard = new SidenavItem({
    name: 'Gestión Usuarios',
    icon: 'person',
    subItems: [],
    position: 1
});

export const bikesharingNav = new SidenavItem({
    name: 'Bikesharing',
    icon: 'directions_bike',
    subItems: [],
    position: 2
});
export const locksInteligents = new SidenavItem({
    name: 'Candados Inteligentes',
    icon: 'lock',
    subItems: [],
    position: 3
});
// Sub Items for the Top Level Item (The items shown when you clicked on the dropdown item)
// Note: The Top Level Item is added as "parent" in those items, here "dashboard" (variable from above)
export const gestionSubItems = [
    new SidenavItem({
        name: 'Usuario final',
        permissionId: '615f825639fa4929e0fd4b55',
        route: '/app/users',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),
    new SidenavItem({
        name: 'Tarjetas NFC',
        permissionId: '615f826139fa4929e0fd4b56',
        route: '/app/nfccards',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Admins sistema',
        permissionId: '615f826c39fa4929e0fd4b57',
        route: '/app/adminsystems',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Organización',
        permissionId: '615f82e039fa4929e0fd4b61',
        route: '/app/organization',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

];




/* 
 *List belonging to the navigation menu
 */

// Top Level Item (The item to click on so the dropdown opens)
export const bikesharingSubItems = [
    new SidenavItem({
        name: 'Prestamos',
        permissionId: '615f828b39fa4929e0fd4b58',
        route: '/app/trips',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Bicicletas',
        permissionId: '615f829239fa4929e0fd4b59',
        route: '/app/bikes',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Mapa',
        permissionId: '615f829b39fa4929e0fd4b5a',
        route: '/app/maps',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Estaciones',
        permissionId: '615f829f39fa4929e0fd4b5b',
        route: '/app/stations',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Rebalanceo',
        permissionId: '615f82d639fa4929e0fd4b60',
        route: '/app/rebalancing',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Configuración',
        permissionId: '615f82a739fa4929e0fd4b5c',
        route: '/app/setting',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Tickets',
        permissionId: '615f82b039fa4929e0fd4b5d',
        route: '/app/tickets',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

];

export const locksSubItems = [
    new SidenavItem({
        name: 'Candado',
        permissionId: '615f82bf39fa4929e0fd4b5e',
        route: '/app/locks',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

    new SidenavItem({
        name: 'Comandos Candados',
        permissionId: '615f82c339fa4929e0fd4b5f',
        route: '/app/commandslocks',
        subItems: [],
        position: 1,
        routerLinkActiveOptions: {
            exact: true
        }
    }),

];