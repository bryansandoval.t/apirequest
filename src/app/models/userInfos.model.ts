import { User } from './user.model';

export class UserInfos {
    constructor(
        public id?,
        public name = '',
        public category?: number,
        public totalDistance?: number,
        public totalAltitude?: number,
        public averageSpeed?: number,
        public averagePace?: number,
        public skills?: string,
        public achievement?: string,
        public photo?: string,
        public birthday?: Date,
        public description?: string,
        public gender?: "male" | "female",
        public membership?: string,
        public activity?: "run" | "bike",
        public clubId?: string,
        public locationId?: string,
        public userId?: string,
        public created_at?: Date,
        public updated_at?: Date,
        public deleted_at?: Date,
        public user?: User
    ) { }
}
