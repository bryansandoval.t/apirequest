export class User {
  constructor(
    public id?,
    public username = '',
    public email = '',
    public password = '',
    public origin = 'dashboard',
    public rol = '',
    public userInfo?,
    public created_at?: Date,
    public updated_at?: Date,
    public deleted_at?: Date,
  ) {
  }
}
