import { Component, OnInit } from '@angular/core';
import { FormModel, DispatchPosibleActions } from '../../../../interfaces/form.interfaces';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { loginFormModel } from '../../../../constants/form.constants'
import { Store } from '@ngrx/store';
import { State } from '../../../../config/global.reducer';
import { loginUser } from '../../../../config/global.actions'

@Component({
  selector: 'elastic-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  loginFormModel: FormModel = loginFormModel;

  dispatchValidateFormActions: DispatchPosibleActions = {
    success: loginUser,
    whereErrorsSave: 'auth.loginFormErrors'
  };

  errors$ = this.store$.select((state: State) => state.auth.loginFormErrors);

  constructor(
    private router: Router,
    private store$: Store
  ) { }

  ngOnInit() {
  }

}
