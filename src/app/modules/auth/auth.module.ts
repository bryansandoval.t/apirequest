import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginModule } from './components/login/login.module';
import { ForgotPasswordModule } from './components/forgot-password/forgot-password.module';

@NgModule({
  imports: [
    CommonModule,
    LoginModule,
    ForgotPasswordModule
  ],
  declarations: []
})
export class AuthModule { }
