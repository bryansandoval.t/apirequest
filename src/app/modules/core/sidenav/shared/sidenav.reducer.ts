import * as sidenav from './sidenav.action';
import { SidenavItem } from '../sidenav-item/sidenav-item.model';
import find from 'lodash-es/find';
import each from 'lodash-es/each';

export interface State {
  sidenavItems: SidenavItem[];
  currentlyOpen: SidenavItem[];
}

const initialState: State = {
  sidenavItems: [],
  currentlyOpen: []
};

export function reducer(state = initialState, action: sidenav.Actions): State {
  switch (action.type) {
    case sidenav.ADD_SIDENAV_ITEM: {
      const item = action.payload as SidenavItem;

      if (state.sidenavItems.indexOf(item) > -1) {
        return state;
      }
      return Object.assign({}, state, {
        sidenavItems: addIdentifiers([...state.sidenavItems, item])
      });
    }

    case sidenav.REMOVE_SIDENAV_ITEM: {
      const item = action.payload as SidenavItem;

      return Object.assign({}, state, {
        sidenavItems: state.sidenavItems.filter(stateItem => stateItem !== item)
      });
    }

    case sidenav.TOGGLE_OPEN_SIDENAV_ITEM: {
      const item = action.payload as SidenavItem;
      let currentlyOpen = state.currentlyOpen;
      if (state.currentlyOpen.indexOf(item) > -1) {
        if (currentlyOpen.length > 1) {
          currentlyOpen = currentlyOpen.slice(0, currentlyOpen.indexOf(item));
        } else {
          currentlyOpen = [];
        }
      } else {
        currentlyOpen = getAllParentItems(item, state.sidenavItems);
      }

      return Object.assign({}, state, {
        currentlyOpen: currentlyOpen
      });
    }

    case sidenav.SET_CURRENTLY_OPEN_BY_ROUTE: {
      const route = action.payload as string;
      let currentlyOpen = [];
      const item = findByRouteRecursive(route, state.sidenavItems);
      if (item && item.hasParent()) {
        currentlyOpen = getAllParentItems(getElementById(item.id.slice(0, -2), state.sidenavItems), state.sidenavItems);
      } else if (item) {
        currentlyOpen = [item];
      }

      return Object.assign({}, state, {
        currentlyOpen: currentlyOpen
      });
    }

    default: {
      return state;
    }
  }
}

function getElementById(id: string, sidenavItems: Array<SidenavItem>) {
  const perentIds = (id || "").match(/.{1,2}(.$)?/g) || [];
  let childElements = sidenavItems;
  let element;
  perentIds.forEach((id) => {
    element = childElements[Number(id)];
    childElements = childElements[Number(id)].subItems;
  })
  return element;
}

function addIdentifiers(sideNavItems: Array<SidenavItem>, fatherIdentifier: string = ""): Array<SidenavItem> {
  const sideNavItemsCopy = [];
  for (let index in sideNavItems) {
    const id = fatherIdentifier + createTwoDigitNumber(Number(index));
    const sideNavItemCopy = Object.assign({}, sideNavItems[index], {
      id: id,
      subItems: addIdentifiers(sideNavItems[index].subItems, id)
    });
    sideNavItemsCopy.push(new SidenavItem(sideNavItemCopy));
  }
  return sideNavItemsCopy;
}

function createTwoDigitNumber(number: number): string {
  if (number < 10) return '0' + String(number);
  return String(number)
}

function getAllParentItems(item: SidenavItem, sidenavItems: Array<SidenavItem>) {
  const perentIds = (item.id || "").match(/.{1,2}(.$)?/g) || [];
  let sidenavItemsCopy = [...sidenavItems];
  const parentsArray = [];
  perentIds.forEach((id) => {
    parentsArray.push(sidenavItemsCopy[Number(id)]);
    sidenavItemsCopy = sidenavItemsCopy[Number(id)].subItems;
  })
  return parentsArray;
}

function findByRouteRecursive(route: string, collection: SidenavItem[] = this.sidenavItems): SidenavItem | null {
  let result = collection.find(item => item.route === route);

  if (!result) {
    collection.forEach(item => {
      if (item.hasSubItems()) {
        const found = findByRouteRecursive(route, item.subItems || []);

        if (found) {
          result = found;
          return false;
        }
      }
    });
  }

  return result;
}

export const getSidenavItems = (state: State) => state.sidenavItems;
export const getSidenavCurrentlyOpen = (state: State) => state.currentlyOpen;
