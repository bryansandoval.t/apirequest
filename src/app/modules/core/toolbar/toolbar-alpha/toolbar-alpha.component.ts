import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { Store } from '@ngrx/store';
import { refreshTokenInit } from 'src/app/config/global.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'elastic-toolbar-alpha',
  templateUrl: './toolbar-alpha.component.html',
  styleUrls: ['./toolbar-alpha.component.scss']
})
export class ToolbarAlphaComponent implements OnInit {

  @Input() sidenavCollapsed: boolean;
  @Input() quickpanelOpen: boolean;
  @Output() toggledSidenav = new EventEmitter();
  @Output() toggledQuickpanel = new EventEmitter();

  dataInfoUser$: Observable<User>;
  userInfo: User;
  public username = '';
  constructor(
    private store: Store<any>,
    private router: Router,
  ) {
   }

  ngOnInit() {
      this.store.select((state) => state['apiData']['user']).subscribe((elem: User) => {
        this.userInfo = elem;
        if (this.userInfo) {
          this.username = this.userInfo.username;
        }
      });
  }

  toggleSidenav() {
    this.toggledSidenav.emit();
  }

  toggleQuickpanel() {
    this.toggledQuickpanel.emit();
  }
}
