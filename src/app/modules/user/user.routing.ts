import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './components/users/users.component';
import { NfcCardsComponent } from './components/nfc-cards/nfc-cards.component';
import { TripsComponent } from './components/trips/trips.component';
import { LocksComponent } from './components/locks/locks.component';
import { AdminSystemsComponent } from './components/admin-systems/admin-systems.component';
import { OrganizationComponent } from './components/organization/organization.component';
import { BikesComponent } from './components/bikes/bikes.component';
import { MapsComponent } from './components/maps/maps.component';
import { StationsComponent } from './components/stations/stations.component';
import { CommandsLocksComponent } from './components/commands-locks/commands-locks.component';
import { BikeRebalancingComponent } from './components/bike-rebalancing/bike-rebalancing.component';
import { SettingComponent } from './components/setting/setting.component';
import { TicketsComponent } from './components/tickets/tickets.component';

export const userRoutes: Routes = [
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'nfccards',
    component: NfcCardsComponent
  },
  {
    path: 'trips',
    component: TripsComponent
  },
  {
    path: '*',
    redirectTo: 'users'
  },
  {
    path: 'locks',
    component: LocksComponent
  },

  {
    path: 'adminsystems',
    component: AdminSystemsComponent
  },

  {
    path: 'organization',
    component: OrganizationComponent
  },

  {
    path: 'bikes',
    component: BikesComponent
  },

  {
    path: 'maps',
    component: MapsComponent
  },

  {
    path: 'stations',
    component: StationsComponent
  },
  {
    path: 'rebalancing',
    component: BikeRebalancingComponent
  },
  {
    path: 'setting',
    component: SettingComponent
  },
  {
    path: 'tickets',
    component: TicketsComponent
  },
  {
    path: 'commandslocks',
    component: CommandsLocksComponent
  }

];

