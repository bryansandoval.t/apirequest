import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralModule } from '../../shared/general.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AgmCoreModule } from '@agm/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatExpansionModule } from '@angular/material/expansion';
//Component Imports
import { UsersComponent } from './components/users/users.component';
import { UserFormDialogComponent } from './components/users/components/user-form-dialog/user-form-dialog.component';
import { NfcCardsComponent } from './components/nfc-cards/nfc-cards.component';
import { TripsComponent } from './components/trips/trips.component';
import { LocksComponent } from './components/locks/locks.component';
import { AdminSystemsComponent } from './components/admin-systems/admin-systems.component';
import { OrganizationComponent } from './components/organization/organization.component';
import { BikesComponent } from './components/bikes/bikes.component';
import { MapsComponent } from './components/maps/maps.component';
import { StationsComponent } from './components/stations/stations.component';
import { CommandsLocksComponent } from './components/commands-locks/commands-locks.component';
import { StateUserComponent } from './components/state-user/state-user.component';
import { PenaltyUserComponent } from './components/penalty-user/penalty-user.component';
import { TripUserComponent } from './components/trip-user/trip-user.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { BikeRebalancingComponent } from './components/bike-rebalancing/bike-rebalancing.component';
import { SettingComponent } from './components/setting/setting.component';
import { TicketsComponent } from './components/tickets/tickets.component';
import { StationsModalComponent } from './components/stations/stations-modal/stations-modal.component';
import { ModalOrganizationComponent } from './components/organization/modal-organization/modal-organization.component';
import { LocksBikeComponent } from './components/bikes/locks-bike/locks-bike.component';
import { ModalsBikeComponent } from './components/bikes/modals-bike/modals-bike.component';
import { EditTripStateComponent } from './components/edit-trip-state/edit-trip-state.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { ReportUsersModalComponent } from './components/users/components/report-users-modal/report-users-modal.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { ReportTripsModalComponent } from './components/trips/report-trips-modal/report-trips-modal.component';
import { ModalAdminSystemsComponent } from './components/admin-systems/modal-admin-systems/modal-admin-systems.component';
import { ModalAddAdminSystemsComponent } from './components/admin-systems/modal-add-admin-systems/modal-add-admin-systems.component';
import { ModalLocksComponent } from './components/locks/modal-locks/modal-locks.component';
import { ModalEditLockComponent } from './components/locks/modal-edit-lock/modal-edit-lock.component';
import { ModalTicketComponent } from './components/tickets/modal-ticket/modal-ticket.component';

@NgModule({
  declarations: [
    StationsModalComponent,
    StationsModalComponent,
    EditTripStateComponent,
    ConfirmationDialogComponent,
    UsersComponent,
    UserFormDialogComponent,
    NfcCardsComponent,
    TripsComponent,
    LocksComponent,
    AdminSystemsComponent,
    OrganizationComponent,
    BikesComponent,
    MapsComponent,
    StationsComponent,
    CommandsLocksComponent,
    StateUserComponent,
    PenaltyUserComponent,
    TripUserComponent,
    BikeRebalancingComponent,
    SettingComponent,
    TicketsComponent,
    ModalOrganizationComponent,
    LocksBikeComponent,
    ModalsBikeComponent,
    ReportUsersModalComponent,
    ReportTripsModalComponent,
    ModalAdminSystemsComponent,
    ModalAddAdminSystemsComponent,
    ModalLocksComponent,
    ModalEditLockComponent,
    ModalTicketComponent
  ],
  imports: [
    MatRadioModule,
    MatExpansionModule,
    NgxMaterialTimepickerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDRZlRjs1Ccqkoz030F8ZwfjVTZS-2Rf08'
      //apiKey: 'AIzaSyBlZxpoD3sYb4r4QKNiLoTW9uWXhT4V1Vo'
    }),
    CommonModule,
    GeneralModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    UserFormDialogComponent,
    PenaltyUserComponent,
    StateUserComponent,
    StationsModalComponent,
    ModalAdminSystemsComponent,
    TripUserComponent,
    ModalOrganizationComponent,
    ModalTicketComponent,
    ModalsBikeComponent,
    ModalLocksComponent,
    LocksBikeComponent,
    EditTripStateComponent,
    ConfirmationDialogComponent,
    ReportUsersModalComponent,
    ReportTripsModalComponent,
    ModalAddAdminSystemsComponent,
    ModalEditLockComponent
  ],
  exports: [
    PenaltyUserComponent,
    StateUserComponent,
    TripUserComponent,
    StationsModalComponent,
    ModalAdminSystemsComponent,
    ModalAddAdminSystemsComponent,
    UserFormDialogComponent,
    LocksBikeComponent,
    ModalsBikeComponent,
    ModalOrganizationComponent,
    ReportTripsModalComponent,
    EditTripStateComponent,
    ConfirmationDialogComponent,
    ReportUsersModalComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class UserModule { }
