import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripUserComponent } from './trip-user.component';

describe('TripUserComponent', () => {
  let component: TripUserComponent;
  let fixture: ComponentFixture<TripUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
