import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from "@ngrx/store";
import { tripUserModal } from '../../../../interfaces/table.interfaces';
import { ListColumn } from 'src/app/modules/core/list/list-column.model'
import { paginatorFinalUserTrip,userPaginationTrip, userPagination, totalGetTrip, searchTermTrip, paginatorFinalUserSearchTrip, showPageSearchTrip, totalGetUsers, getStationsTrip } from 'src/app/config/global.actions';
@Component({
  selector: 'elastic-trip-user',
  templateUrl: './trip-user.component.html',
  styleUrls: ['./trip-user.component.scss']
})
export class TripUserComponent implements OnInit {

  tableName: string = "Trips";
  columns: ListColumn[] = tripUserModal.tableColumns as ListColumn[];
  filterLength: number = 0;
  tableData = { store: 'trip', storeState: 'userTrips' };
  tableDataSearch = { store: 'trip', storeState: 'userTrips' };
  filterInclude = tripUserModal.filterInclude;
  searchTerm: any = ''
  valueSearch = "";
  stations: any = [];
  selectStation: any = "";
  selectFromDate: any = "";
  selectToDate: any = "";
  termSearch: any = [];
  search:boolean=false;

  userTrips$: Observable<any> = this.store.select(state => state['user']['userTrips']);
  userTripsSubscription: Subscription;
  userTrips: any;

  userPaginationTrip$: Observable<any> = this.store.select(state => state['user']['userPaginationTrip']);
  userPaginationTripSubscription: Subscription;
  userPaginationTrip: any

  searchTermTrip$: Observable<any> = this.store.select(state => state['user']['searchTermTrip']);
  searchTermTripSubscription: Subscription;
  searchTermTrip: any

  datasourceSearchTrip$: Observable<any> = this.store.select(state => state['user']['datasourceSearchTrip']);
  datasourceSearchTripSubscription: Subscription;
  datasourceSearchTrip: any=[];

  totalGetTrip$: Observable<any> = this.store.select(state => state['user']['totalGetTrip']);
  totalGetTripSubscription: Subscription;
  totalGetTrip: any

  stationsTrips$: Observable<any> = this.store.select(state => state['user']['stationsTrips']);
  stationsTripsSubscription: Subscription;
  stationsTrips: any



  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialogRef: MatDialogRef<TripUserComponent>,
    private store: Store<any>,
    private ref: ChangeDetectorRef
  ) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.userTripsSubscription = this.userTrips$.subscribe((trips) => {
      this.userTrips = trips
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);

    })
    this.userPaginationTripSubscription = this.userPaginationTrip$.subscribe((pages) => {
      this.userPaginationTrip = pages
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);
    })
    this.searchTermTripSubscription = this.searchTermTrip$.subscribe((search) => {
      this.searchTermTrip = search;
      if (this.valueSearch == "") {
        this.searchTermTrip = "";
      }
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);

    })

    this.datasourceSearchTripSubscription = this.datasourceSearchTrip$.subscribe((searchDatabase) => {
      this.datasourceSearchTrip = searchDatabase;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);

    })

    this.totalGetTripSubscription = this.totalGetTrip$.subscribe((userId) => {
      this.totalGetTrip = userId
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);

    })

    this.stationsTripsSubscription = this.stationsTrips$.subscribe((stations) => {
      this.stationsTrips = stations;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);
      /*  this.stationsTrips.forEach(element => {
         this.stations.push(element.name);
       }) */


    })
  }
  closeModal() {
    this.dialogRef.close()
    this.store.dispatch(new userPaginationTrip({
      pagination:
      {
        totalPage: 0,
        currentPage: 1,
        cantLeft: false,
        cantRigth: true,
        pageSize: 20,
        limit: 20,
        skip: 0
      }
    }));
    //this.store.dispatch(new totalGetUsers());

  }



  paginator(direction) {
    this.store.dispatch(new paginatorFinalUserTrip({ direction: direction }))
  }

  paginatorsearch(direction) {
    this.store.dispatch(new paginatorFinalUserSearchTrip({ direction: direction }))
  }

  onInputChange() {
    if (this.valueSearch == "") {
      this.store.dispatch(new userPaginationTrip({
        pagination:
        {
          totalPage: 0,
          currentPage: 1,
          cantLeft: false,
          cantRigth: true,
          pageSize: 20,
          limit: 20,
          skip: 0
        }
      }));
      this.store.dispatch(new totalGetTrip({ userId: this.totalGetTrip }));
      this.store.dispatch(new searchTermTrip({ term: "" }));
    }
  }



  searchTrip() {
    this.termSearch.stationId = this.selectStation
    this.termSearch.toDate = this.selectToDate
    this.termSearch.fromDate = this.selectFromDate
    if (this.termSearch) {
      this.search=true;
      this.store.dispatch(new userPaginationTrip({
        pagination:
        {
          totalPage: 0,
          currentPage: 1,
          cantLeft: false,
          cantRigth: true,
          pageSize: 20,
          limit: 20,
          skip: 0
        }
      }));
      this.store.dispatch(new showPageSearchTrip({ termSearch: this.termSearch }));

    }
    this.termSearch=[];
  }

  deleteFilterSearch() {
    this.selectStation = "";
    this.selectToDate = "";
    this.selectFromDate = "";
    this.termSearch = []
    this.search=false;
    this.store.dispatch(new userPaginationTrip({
      pagination:
      {
        totalPage: 0,
        currentPage: 1,
        cantLeft: false,
        cantRigth: true,
        pageSize: 20,
        limit: 20,
        skip: 0
      }
    }));
    this.store.dispatch(new totalGetTrip({ userId: this.totalGetTrip }))
   // this.store.dispatch(new getStationsTrip())

  }

  ngOnDestroy() {
    this.userTripsSubscription.unsubscribe();
    this.userPaginationTripSubscription.unsubscribe();
    this.searchTermTripSubscription.unsubscribe();
    this.datasourceSearchTripSubscription.unsubscribe();
    this.totalGetTripSubscription.unsubscribe();
    this. stationsTripsSubscription.unsubscribe();

  }

}
