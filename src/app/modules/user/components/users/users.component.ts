import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { TableInfo, TestInfoTable } from '../../../../interfaces/table.interfaces';
import { MatDialog } from '@angular/material/dialog';
import { UserFormDialogComponent } from './components/user-form-dialog/user-form-dialog.component';
import { DeleteUser, modalUser,userPaginationTrip, totalGetTrip, totalGetUsers, paginatorFinalUser, showPageSearch,  searchTerm, paginatorFinalUserSearch, userPagination, getStationsTrip, getPenaltyTypes } from 'src/app/config/global.actions';
import { ListColumn } from 'src/app/modules/core/list/list-column.model';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { PenaltyUserComponent } from '../../components/penalty-user/penalty-user.component';
import { StateUserComponent } from '../../components/state-user/state-user.component';
import { TripUserComponent } from '../../components/trip-user/trip-user.component';
import { ReportUsersModalComponent } from './components/report-users-modal/report-users-modal.component';



@Component({
  selector: 'elastic-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})

export class UsersComponent implements OnInit {

  tableName: string = "Usuarios";
  columns: ListColumn[] = TestInfoTable.tableColumns as ListColumn[];
  filterLength: number = 0;
  filterInclude = TestInfoTable.filterInclude;
  valueSearch = "";


  userFinalUser$: Observable<any> = this.store.select(state => state['user']['usersFinalUser']);
  userFinalUserSubscription: Subscription;
  userFinalUser: MatTableDataSource<Element[]>;
  userFinalUserLength: number;

  userPagination$: Observable<any> = this.store.select(state => state['user']['userPagination']);
  userPaginationSubscription: Subscription;
  userPagination: any

  searchTerm$: Observable<any> = this.store.select(state => state['user']['searchTerm']);
  searchTermSubscription: Subscription;
  searchTerm: any

  datasourceSearch$: Observable<any> = this.store.select(state => state['user']['datasourceSearch']);
  datasourceSearchSubscription: Subscription;
  datasourceSearch: any





  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>,
    private ref: ChangeDetectorRef
  ) { }



  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.store.dispatch(new getStationsTrip());
    this.store.dispatch(new getPenaltyTypes())
    this.store.dispatch(new totalGetUsers());

    this.userFinalUserSubscription = this.userFinalUser$.subscribe((users) => {
      this.userFinalUser = new MatTableDataSource(users);
      this.userFinalUserLength = users.length
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);

    })

    this.userPaginationSubscription = this.userPagination$.subscribe((pages) => {
      this.userPagination = pages
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })

    this.searchTermSubscription = this.searchTerm$.subscribe((search) => {
      this.searchTerm = search;
      if(this.valueSearch==""){
        this.searchTerm="";
      }
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);

    })

    this.datasourceSearchSubscription = this.datasourceSearch$.subscribe((searchDatabase) => {
      this.datasourceSearch = searchDatabase;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);

    })

  }

  create() {
    this.dialog.open(UserFormDialogComponent, { data: null });
  }

  delete(row) {
    const item = row.item;
    this.store.dispatch(new DeleteUser({ user: item.user, userId: item.userId, userInfoId: item.id }));
  }

  searchUser() {
    this.store.dispatch(new userPagination({ pagination:
      {totalPage: 0,
      currentPage: 1,
      cantLeft: false,
      cantRigth: true,
      pageSize: 20,
      limit: 20,
      skip: 0 }}));
    this.store.dispatch(new showPageSearch({ termSearch: this.valueSearch }))
    this.store.dispatch(new searchTerm({ term: this.valueSearch }))
  }

  openModal(modal: string, user: any) {
    if (modal == "trip") {
      this.dialog.open(TripUserComponent, { width: '80vw', height: '85vh' })
      this.store.dispatch(new userPaginationTrip({ pagination:
        {totalPage: 0,
        currentPage: 1,
        cantLeft: false,
        cantRigth: true,
        pageSize: 20,
        limit: 20,
        skip: 0 }}));

      this.store.dispatch(new totalGetTrip({ userId: user.id }))


    } else if (modal == "state") {
      this.dialog.open(StateUserComponent, { width: '40vw', height: '80vh' })
      this.store.dispatch(new modalUser({ user: user }))
    }
    else {
      this.dialog.open(PenaltyUserComponent, { width: '40vw' })
      /* this.store.dispatch(new getPenaltyTypes()) */
      this.store.dispatch(new modalUser({ user: user }))
    }

  }
  paginator(direction) {
    this.store.dispatch(new paginatorFinalUser({ direction: direction }))
  }

  paginatorsearch(direction) {
    this.store.dispatch(new paginatorFinalUserSearch({ direction: direction }))
  }

  onInputChange(){
    if(this.valueSearch==""){
      this.store.dispatch(new userPagination({ pagination:
        {totalPage: 0,
        currentPage: 1,
        cantLeft: false,
        cantRigth: true,
        pageSize: 20,
        limit: 20,
        skip: 0 }}));
      this.store.dispatch(new totalGetUsers());
      this.store.dispatch(new searchTerm({term:""}));
    }
  }
  reportUsers(){
    this.dialog.open(ReportUsersModalComponent, { width: '60vw' })
  }

  ngOnDestroy() {
    this.userFinalUserSubscription.unsubscribe();
    this.userPaginationSubscription.unsubscribe();
    this.searchTermSubscription.unsubscribe();
    this.datasourceSearchSubscription.unsubscribe();
  }
}


