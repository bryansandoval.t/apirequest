import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/models/user.model';
import { Store } from '@ngrx/store';
import { State } from 'src/app/config/global.reducer';
import { UserInfos } from '../../../../../../models/userInfos.model';
import { AddUser } from 'src/app/config/global.actions';

@Component({
  selector: 'elastic-user-form-dialog',
  templateUrl: './user-form-dialog.component.html',
  styleUrls: ['./user-form-dialog.component.scss']
})
export class UserFormDialogComponent implements OnInit, OnDestroy {

  USERNAME_LABEL = 'Username';
  USERNAME_PH = 'Enter a username for this account';
  USER_NAME_LABEL = 'last name';
  USER_NAME_PH = "Enter the user's last name";
  USER_EMAIL_LABEL = 'Email';
  USER_EMAIL_PH = 'Enter user email';
  USER_PASS_LABEL = 'Password';
  USER_PASS_PH = '********';
  USER_GENDER_LABEL = 'Select gender';
  USER_GENDER_OPTION_A = 'Male';
  USER_GENDER_OPTION_B = 'Female';
  USER_SUBMIT_BTN = 'Add admin user';
  REQUIRED_FIELD_ERR_MSG = 'This field is required';
  update = false;

  userForm: FormGroup;
  subscriptions: Subscription;

  get nameCtr() { return this.userForm.get('nameCtr'); }
  get userNameCtr() { return this.userForm.get('userNameCtr'); }
  get userGenderCtr() { return this.userForm.get('userGenderCtr'); }
  get userEmailCtr() { return this.userForm.get('userEmailCtr'); }
  get userPassCtr() { return this.userForm.get('userPassCtr'); }

  constructor(
    @Inject(MAT_DIALOG_DATA) public userInfos: UserInfos,
    private dialogRef: MatDialogRef<UserFormDialogComponent>,
    private store$: Store<State>
  ) {
    this.update = false;
    this.USER_SUBMIT_BTN = 'Añadir usuario administrador';
    this.subscriptions = new Subscription();
    this.userForm = new FormGroup(
      {
        nameCtr: new FormControl(userInfos ? userInfos['item']['user.username'] : null, Validators.required),
        userNameCtr: new FormControl(userInfos ? userInfos['item'].name : null, Validators.required),
        userGenderCtr: new FormControl(userInfos ? userInfos['item'].gender : null, Validators.required),
        userEmailCtr: new FormControl(userInfos ? userInfos['item']['user.email'] : null, Validators.required),
        userPassCtr: new FormControl(userInfos ? userInfos['item']['user.password'] : null, Validators.required)
      }
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
  }

  submit() {
    if (this.userForm.valid) {
      const userData = {
        username: this.nameCtr.value,
        email: this.userEmailCtr.value,
        password: this.userPassCtr.value,
        rol: 'admin',
        origin: 'dashboard',
        created_at: new Date(),
        updated_at: new Date()
      };
      const userInfosData = {
        name: this.userNameCtr.value,
        gender: this.userGenderCtr.value,
        created_at: new Date(),
        updated_at: new Date()
      };
      this.store$.dispatch(new AddUser({ user: userData, userInfos: userInfosData }));
    }
  }

  close() {
    this.dialogRef.close();
  }

}
