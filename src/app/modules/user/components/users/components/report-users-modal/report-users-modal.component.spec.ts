import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportUsersModalComponent } from './report-users-modal.component';

describe('ReportUsersModalComponent', () => {
  let component: ReportUsersModalComponent;
  let fixture: ComponentFixture<ReportUsersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportUsersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportUsersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
