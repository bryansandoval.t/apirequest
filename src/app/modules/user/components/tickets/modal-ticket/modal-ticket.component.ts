import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getStationsTicket, patchTicket, setLoadingTicketsTable } from 'src/app/config/global.actions';

@Component({
  selector: 'elastic-modal-ticket',
  templateUrl: './modal-ticket.component.html',
  styleUrls: ['./modal-ticket.component.scss']
})
export class ModalTicketComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ModalTicketComponent>,
    private store: Store<any>,
  ) { }

  selectedTicketInfo$: Observable<any> = this.store.select(state => state.apiData.selectedTicketInfo);
  selectedTicketInfoSubscription: Subscription;
  selectedTicketInfo: any;

  masterListTickets$: Observable<any> = this.store.select(state => state.apiData.masterListTicket);
  masterListTicketsSubscription: Subscription;
  masterListTickets: any;

  closeModalTickets$: Observable<any> = this.store.select(state => state.apiData.closeModalTickets);
  closeModalTicketsSubscription: Subscription;

  stateTicket = new FormControl('', Validators.required);

  ngOnInit() {
    this.masterListTicketsSubscription = this.masterListTickets$.subscribe((masterListTickets) => {
      this.masterListTickets = masterListTickets
    });
    this.closeModalTicketsSubscription = this.closeModalTickets$.subscribe((closeModal) => {
      closeModal?this.closeModal():null
    });
    this.selectedTicketInfoSubscription = this.selectedTicketInfo$.subscribe((selectedTicketInfo: any) => {
      this.selectedTicketInfo = selectedTicketInfo;
      this.stateTicket.setValue(selectedTicketInfo.state)
      this.methodGetStationsTicket();
    });
  }

  methodGetStationsTicket() {
    if (this.selectedTicketInfo['trip'] && this.selectedTicketInfo['trip'].startStationId && !this.selectedTicketInfo['trip'].startStation) {
      this.store.dispatch(new getStationsTicket())
    }
  }

  submit() {
    this.store.dispatch(new setLoadingTicketsTable({ loadingTicketsTable: true }))
    this.store.dispatch(new patchTicket({ state: this.stateTicket.value }))
  }

  closeModal() {
    this.dialogRef.close();
  };

  ngOnDestroy() {
    this.selectedTicketInfoSubscription.unsubscribe()
    this.masterListTicketsSubscription.unsubscribe()
    this.closeModalTicketsSubscription.unsubscribe()
  }

}
