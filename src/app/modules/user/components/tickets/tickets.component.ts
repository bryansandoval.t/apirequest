import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { tableTypes } from 'src/app/constants/table.constants';
import { Column, ticketsColumns } from 'src/app/interfaces/table.interfaces';
import { ROUTE_TRANSITION } from 'src/app/app.animation';
import { ModalTicketComponent } from './modal-ticket/modal-ticket.component';
import { getMasterListTicket, getTicketsPage, getTotalTickets, searchTickets, setCloseModalTickets, setLoadingTicketsTable, setSearchWordTickets, setSelectedTicketInfo, setTicketPageSize, setTicketSearchStatus, showPageTicket } from 'src/app/config/global.actions';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'elastic-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class TicketsComponent implements OnInit {
  onFilterChangeValue: string;
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>,
    private ref: ChangeDetectorRef)
  { }

  ticketColumns: Column[] = ticketsColumns;
  ticketPageSize = 20;
  //TEMP volar
  allTickets: any[] = [];
  title: string = "Tickets";

  currentPageTickets$: Observable<any> = this.store.select(state => state.apiData.currentPageTickets);
  currentPageTicketsSubscription: Subscription;
  currentPageTickets: any[]; //Pagina actual

  ticketSearchStatus$: Observable<any> = this.store.select(state => state.apiData.ticketSearchStatus);
  ticketSearchStatusSubscription: Subscription;
  ticketSearchStatus: boolean;

  totalTickets$: Observable<any> = this.store.select(state => state.apiData.totalTickets);
  totalTicketsSubscription: Subscription;
  totalTickets: number;

  loadingTicketsTable$: Observable<any> = this.store.select(state => state.apiData.loadingTicketsTable);
  loadingTicketsTableSubscription: Subscription;
  loadingTicketsTable: any;

  ngOnInit() {
    this.store.dispatch(new setTicketSearchStatus({ ticketSearchStatus: false }))
    this.store.dispatch(new setTicketPageSize({
      ticketPageSIze: {
        limit: this.ticketPageSize,
        pageNumber: 0
      }
    }));
    this.store.dispatch(new setLoadingTicketsTable({ loadingTicketsTable: true }));
    this.store.dispatch(new getMasterListTicket());
    this.store.dispatch(new getTotalTickets());
    this.totalTicketsSubscription = this.totalTickets$.subscribe((totalTickets) => {
      this.totalTickets = totalTickets
    });
    this.loadingTicketsTableSubscription = this.loadingTicketsTable$.subscribe((loading) => {
      this.loadingTicketsTable = loading
    });
    this.ticketSearchStatusSubscription = this.ticketSearchStatus$.subscribe((ticketSearchStatus) => {
      this.ticketSearchStatus = ticketSearchStatus
    });
    this.currentPageTicketsSubscription = this.currentPageTickets$.subscribe((currentPageTickets) => {
      this.currentPageTickets = currentPageTickets //Pagina actual
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    });


    //inicializar parametros tabla
    this.loadingTicketsTable = false;
  }

  tableAction(event) {
    this.store.dispatch(new setCloseModalTickets({ closeModalTickets: false }));
    this.store.dispatch(new setSelectedTicketInfo({ selectedTicketInfo: event.row }));
    this.dialog.open(ModalTicketComponent, { width: '70vw', height: '80vh' })
  }

  changePage(event) {
    this.store.dispatch(new setLoadingTicketsTable({ loadingTicketsTable: true }));
    this.store.dispatch(new setTicketPageSize({
      ticketPageSIze: {
        limit: this.ticketPageSize,
        pageNumber: event.first
      }
    }));
    this.store.dispatch(new showPageTicket({ skip: event.first }));
  }
  showAll() {
    this.store.dispatch(new setTicketPageSize({
      ticketPageSIze: {
        limit: this.ticketPageSize,
        pageNumber: 0
      }
    }));
    this.store.dispatch(new setLoadingTicketsTable({ loadingTicketsTable: true }));
    this.store.dispatch(new setTicketSearchStatus({ ticketSearchStatus: false }))
    this.store.dispatch(new getTotalTickets());
  }
  onFilterChange(event: Event) {
    const value = (event.target as HTMLInputElement).value;
    this.onFilterChangeValue = value
    if (event['code'] == 'Enter') {
      this.searchTickets()
    }
  }

  searchTickets() {
    if (this.onFilterChangeValue.length > 2) {
      this.store.dispatch(new setLoadingTicketsTable({ loadingTicketsTable: true }));
      this.store.dispatch(new setTicketSearchStatus({ ticketSearchStatus: true }))
      this.store.dispatch(new setSearchWordTickets({ searchWordTickets: this.onFilterChangeValue }))
      this.store.dispatch(new searchTickets())
    };
  };

  ngOnDestroy() {
    this.currentPageTicketsSubscription.unsubscribe()
    this.ticketSearchStatusSubscription.unsubscribe()
    this.totalTicketsSubscription.unsubscribe()
    this.loadingTicketsTableSubscription.unsubscribe()
  }
}
