import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { TableInfo, NfcCardsTable } from '../../../../interfaces/table.interfaces';

@Component({
  selector: 'elastic-nfc-cards',
  templateUrl: './nfc-cards.component.html',
  styleUrls: ['./nfc-cards.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class NfcCardsComponent implements OnInit {

  @Input() tableData: TableInfo = NfcCardsTable;
  storeInfo = { store: 'user', storeState: 'user-infos' };
  storeInfoSearch = { store: 'user', storeState: 'user-infosSearch' };

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>) { }

  ngOnInit() {
  }

  create() {
    console.log("Crear item");
  }

  delete(row) {
    console.log("Delete item",row);
  }
}
