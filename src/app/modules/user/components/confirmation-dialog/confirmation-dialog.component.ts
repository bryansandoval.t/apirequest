import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { Store } from "@ngrx/store";
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { patchEndTrip, setLoadingTripsPage } from 'src/app/config/global.actions';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'elastic-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private store: Store<any>,
    private ref: ChangeDetectorRef,
    private dialog: MatDialog,
  ) { }

  closeTripsModal$: Observable<any> = this.store.select(state => state['apiData']['closeTripsModal']);
  closeTripsModalSubscription: Subscription;

  ngOnInit() {
    this.closeTripsModalSubscription = this.closeTripsModal$.subscribe((closeModal) => {
      if (closeModal) { this.closeModal() }
    })
  }

  endTrip() {
    this.store.dispatch(new setLoadingTripsPage({ loadingTripsPage: true }));
    this.store.dispatch(new patchEndTrip())
  }

  closeModal() {
    this.dialogRef.close();
  };
  
  ngOnDestroy() {
    this.closeTripsModalSubscription.unsubscribe();
  }

}
