import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenaltyUserComponent } from './penalty-user.component';

describe('PenaltyUserComponent', () => {
  let component: PenaltyUserComponent;
  let fixture: ComponentFixture<PenaltyUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenaltyUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenaltyUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
