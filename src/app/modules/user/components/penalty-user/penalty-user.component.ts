
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from "@ngrx/store";
import { calculationDates, penaltyDates, postPenaltys,modalUser } from 'src/app/config/global.actions';
import * as _ from 'lodash';


@Component({
  selector: 'elastic-penalty-user',
  templateUrl: './penalty-user.component.html',
  styleUrls: ['./penalty-user.component.scss']
})
export class PenaltyUserComponent implements OnInit {

  lastname: string = "";
  penaltys: any = [];
  startDate: any = "";
  endDate: any = "";
  form: FormGroup;
  save: boolean = true;

  modalUser$: Observable<any> = this.store.select(state => state['user']['modalUser']);
  modalUserSubscription: Subscription;
  modalUser: any;

  penaltyTypes$: Observable<any> = this.store.select(state => state['user']['penaltyTypes']);
  penaltyTypesSubscription: Subscription;
  penaltyTypes: any

  penaltyDates$: Observable<any> = this.store.select(state => state['user']['penaltyDates']);
  penaltyDatesSubscription: Subscription;
  penaltyDates: any

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialogRef: MatDialogRef<PenaltyUserComponent>,
    private store: Store<any>,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.modalUserSubscription = this.modalUser$.subscribe((user) => {
      this.modalUser = user;
      this.lastname = this.modalUser.firstLastname + ' ' + this.modalUser.secondLastname
      if (this.modalUser.penaltyInfo != "Sin sanciones") {
        this.save = false;
      }
    })
    this.penaltyTypesSubscription = this.penaltyTypes$.subscribe((typePenaltys) => {
      this.penaltyTypes = typePenaltys;
      this.penaltyTypes.forEach(element => {
        this.penaltys.push(element.value);
      });
    })
    this.penaltyDatesSubscription = this.penaltyDates$.subscribe((dates) => {
      this.penaltyDates = _.cloneDeep(dates);
      this.penaltyDates.startDate = new Date();
      this.startDate = this.penaltyDates.startDate
      var fecha = new Date(this.startDate);
      var month = fecha.getUTCMonth() + 1; //months from 1-12
      var day = fecha.getUTCDate();
      var year = fecha.getUTCFullYear();
      this.startDate = year + "-" + month + "-" + day;
      if (this.penaltyDates.endDate) {
        this.endDate = this.penaltyDates.endDate
        var fecha = new Date(this.endDate);
        var month = fecha.getUTCMonth() + 1; //months from 1-12
        var day = fecha.getUTCDate();
        var year = fecha.getUTCFullYear();
        this.endDate = year + "-" + month + "-" + day;
      }
    })
    this.form = this.formBuilder.group({
      type: ['', Validators.required],
      /* dateStart:[{value:null, disabled:this.startDate}],
      endDate:[{value:null, disabled:this.penaltyDates.endDate}], */
      reason: ['', Validators.required],
    });
  }
  closeModal() {
    this.store.dispatch(new modalUser({ user: {} }))
    this.store.dispatch(new penaltyDates({ dates: {} }));
    this.dialogRef.close()
  }

  saveModal() {
    if (this.form.valid) {
      this.form.value.startDate = this.penaltyDates.startDate;
      this.form.value.endDate = this.penaltyDates.endDate;
      this.store.dispatch(new postPenaltys({ penalty: this.form.value }));
      this.dialogRef.close()
    }

    //se despacha accion
    // this.dialogRef.close()
  }
  typePenalty(type) {
    this.store.dispatch(new calculationDates({ penaltyType: type }));
  }

}
