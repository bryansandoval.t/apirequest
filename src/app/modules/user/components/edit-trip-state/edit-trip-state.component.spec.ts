import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTripStateComponent } from './edit-trip-state.component';

describe('EditTripStateComponent', () => {
  let component: EditTripStateComponent;
  let fixture: ComponentFixture<EditTripStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTripStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTripStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
