import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { Store } from "@ngrx/store";
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import * as moment from 'moment';
import { setCloseTripsModal } from 'src/app/config/global.actions';


@Component({
  selector: 'elastic-edit-trip-state',
  templateUrl: './edit-trip-state.component.html',
  styleUrls: ['./edit-trip-state.component.scss']
})
export class EditTripStateComponent implements OnInit {

  picker: any = "";
  picker1: any = "";
  selected: any = "";
  displayFn: any;

  modalTrip$: Observable<any> = this.store.select(state => state['user']['modalTrip']);
  modalTripSubscription: Subscription;
  modalTrip: any;

  tableStations$: Observable<any> = this.store.select(state => state['apiData']['tableStations']);
  tableStationsSubscription: Subscription;
  tableStations: any[];

  closeTripsModal$: Observable<any> = this.store.select(state => state['apiData']['closeTripsModal']);
  closeTripsModalSubscription: Subscription;

  formTrips: FormGroup = new FormGroup({
    starDate: new FormControl({ value: '', disabled: true }),
    endDate: new FormControl({ value: '', disabled: true }),
    startStationId: new FormControl({ value: '', disabled: true }),
    endStationId: new FormControl({ value: '', disabled: true }),
    userId: new FormControl({ value: '', disabled: true }),
    bikeId: new FormControl({ value: '', disabled: true }),
    state: new FormControl({ value: '', disabled: true })
  })

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialogRef: MatDialogRef<EditTripStateComponent>,
    private store: Store<any>,
    private ref: ChangeDetectorRef,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.store.dispatch(new setCloseTripsModal({ closeTripsModal: false }));

    this.tableStationsSubscription = this.tableStations$.subscribe((tableStations) => {
      this.tableStations = tableStations
    })

    this.closeTripsModalSubscription = this.closeTripsModal$.subscribe((closeModal) => {
      if(closeModal){this.closeModal()}
    })
    this.modalTripSubscription = this.modalTrip$.subscribe((modalTrip) => {
      this.modalTrip = modalTrip;
      console.log('modalTrip',modalTrip)
      this.formTrips.setValue({
        starDate: new Date(modalTrip.startDate).toISOString().slice(0, 16),
        endDate: modalTrip.endDate? new Date(modalTrip.endDate).toISOString().slice(0, 16): '',
        startStationId: modalTrip.startStation.name,
        endStationId: modalTrip.endStation?modalTrip.endStation.name:'',
        userId: `${modalTrip.user.name} ${modalTrip.user.firstLastname}`,
        bikeId: modalTrip.bike.number,
        state: modalTrip.state
      });
    })
  }

  tripConfirmation() {
    this.dialog.open(ConfirmationDialogComponent, { width: '35vw', height: '40vh' })
  }

  closeModal() {
    this.dialogRef.close();
  };

  ngOnDestroy() {
    this.modalTripSubscription.unsubscribe();
    this.tableStationsSubscription.unsubscribe();
    this.closeTripsModalSubscription.unsubscribe();
  }
}

