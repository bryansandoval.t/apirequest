import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeRebalancingComponent } from './bike-rebalancing.component';

describe('BikeRebalancingComponent', () => {
  let component: BikeRebalancingComponent;
  let fixture: ComponentFixture<BikeRebalancingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeRebalancingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeRebalancingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
