import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { TableInfo, RebalancingTable } from '../../../../interfaces/table.interfaces';

@Component({
  selector: 'elastic-bike-rebalancing',
  templateUrl: './bike-rebalancing.component.html',
  styleUrls: ['./bike-rebalancing.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class BikeRebalancingComponent implements OnInit {

  @Input() tableData: TableInfo = RebalancingTable;
  storeInfo = { store: 'user', storeState: 'user-infos' };
  storeInfoSearch = { store: 'user', storeState: 'user-infosSearch' };

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>) { }

  ngOnInit() {
  }

  create() {
    console.log("Crear item");
  }

  delete(row) {
    console.log("Delete item",row);
  }

}
