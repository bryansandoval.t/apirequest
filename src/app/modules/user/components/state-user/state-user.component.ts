
import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from "@ngrx/store";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { formatDate } from '@angular/common';
import * as moment from 'moment';
import * as _ from 'lodash';
import { patchUser ,modalUser} from 'src/app/config/global.actions';

@Component({
  selector: 'elastic-state-user',
  templateUrl: './state-user.component.html',
  styleUrls: ['./state-user.component.scss']
})
export class StateUserComponent implements OnInit {

  slide: string = "";
  //accountState: string = "creado";
  lastname: string = "";
  stateModal: string = "";

  modalUser$: Observable<any> = this.store.select(state => state['user']['modalUser']);
  modalUserSubscription: Subscription;
  modalUser: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialogRef: MatDialogRef<StateUserComponent>,
    private store: Store<any>,
    private ref: ChangeDetectorRef) { }

  ngOnInit() {
    /* if (this.accountState == "creado") {
      this.slide = "Creado"
    } */
    this.modalUserSubscription = this.modalUser$.subscribe((user) => {
      this.modalUser = _.cloneDeep(user);
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
      this.lastname = this.modalUser.firstLastname + ' ' + this.modalUser.secondLastname
      var fecha= new Date(this.modalUser.birthdate);
      var month = fecha.getUTCMonth() + 1; //months from 1-12
      var day = fecha.getUTCDate();
      var year = fecha.getUTCFullYear();
      this.modalUser.birthdate=year + "-" + month + "-" + day;
      // this.modalUser.birthdate=moment(this.modalUser.birthDate).format("YYYY-MM-DD")
      if (this.modalUser.accountState == "created") {
        this.slide = "Creado"
      } else if (this.modalUser.accountState == "active") {
        this.slide = "Activo"
      } else {
        this.slide = "Inactivo"
      }
    })
  }


  slideDecision() {

    if (this.slide == "Creado") {
      this.slide = "Activo";
      this.stateModal = "active";
    }
    else if (this.slide == "Activo") {
      this.slide = "Inactivo";
      this.stateModal = "inactive";
    }
    else if (this.slide == "Inactivo") {
      this.slide = "Activo";
      this.stateModal = "active";
    }

  }

  closeModal() {
    this.store.dispatch(new modalUser({ user: {} }))
    this.dialogRef.close()
  }

  saveModal() {

    this.store.dispatch(new patchUser({ stateUser:this.stateModal }));
    this.dialogRef.close()
  }

  ngOnDestroy() {
    this.modalUserSubscription.unsubscribe();
  }

}
