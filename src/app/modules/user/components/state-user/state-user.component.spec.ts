import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateUserComponent } from './state-user.component';

describe('StateUserComponent', () => {
  let component: StateUserComponent;
  let fixture: ComponentFixture<StateUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
