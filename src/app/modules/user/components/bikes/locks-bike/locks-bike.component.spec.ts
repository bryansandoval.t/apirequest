import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocksBikeComponent } from './locks-bike.component';

describe('LocksBikeComponent', () => {
  let component: LocksBikeComponent;
  let fixture: ComponentFixture<LocksBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocksBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocksBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
