import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from 'src/app/app.animation';
import { Store } from '@ngrx/store';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
//Informacion sobre la tabla
import { tableTypes } from 'src/app/constants/table.constants';
import { Column, TableInfo, LocksTable } from 'src/app/interfaces/table.interfaces';
import { skipPageLocks, showPageLocksBikes, searchLocksBikesForm, getModalLocksSearch, getCountLocksBikes,selectedLock, locksBikesSearchResult } from 'src/app/config/global.actions';


@Component({
  selector: 'elastic-locks-bike',
  templateUrl: './locks-bike.component.html',
  styleUrls: ['./locks-bike.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class LocksBikeComponent implements OnInit {

  LocksColumns: Column[];
  currentPageLocks: any[];
  loading: boolean;
  totalTrips: number;
  tripsPageSize: number;
  //TEMP volar
  allTrips: any[];
  title: string = "Candados";
  valueSearchLocks: any = "";
  form: any = {}


  locksBikes$: Observable<any> = this.store.select(state => state['user']['saveLocksBikes']);
  locksBikesSubscription: Subscription;
  locksBikes: any;

  locksPageSize$: Observable<any> = this.store.select(state => state['user']['locksPageSize']);
  locksPageSizeSubscription: Subscription;
  locksPageSize: any;

  totalLocks$: Observable<any> = this.store.select(state => state['user']['totalLockBikes']);
  totalLocksSubscription: Subscription;
  totalLocks: any;



  @Input() tableData: TableInfo = LocksTable;
  storeInfo = { store: 'user', storeState: 'user-infos' };
  storeInfoSearch = { store: 'user', storeState: 'user-infosSearch' };

  constructor(private dialog: MatDialog,
    private dialogRef: MatDialogRef<LocksBikeComponent>,
    private router: Router,
    private store: Store<any>,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit() {

    this.locksBikesSubscription = this.locksBikes$.subscribe((locks) => {
      this.locksBikes = locks;
      this.currentPageLocks = this.locksBikes
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })

    this.locksPageSizeSubscription = this.locksPageSize$.subscribe((pageSizeLock) => {
      this.locksPageSize = pageSizeLock;
    })

    this.totalLocksSubscription = this.totalLocks$.subscribe((totalLocks) => {
      this.totalLocks = totalLocks;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);

    })


    this.LocksColumns = [
      {
        header: 'Imei',
        propertyPath: 'imei',
        type: tableTypes.string,
      },
      {
        header: 'QR',
        propertyPath: 'qrNumber',
        type: tableTypes.string,
      },
      {
        header: 'Mac',
        propertyPath: 'mac',
        type: tableTypes.string
      },
      {
        header: 'Seleccionar',
        propertyPath: '',
        type: tableTypes.buttonAdd,
        width: "100px",
      }

    ]

    this.loading = false;
  }

  changePage(event) {
    this.store.dispatch(new skipPageLocks({ skip: event.first }));
    this.store.dispatch(new showPageLocksBikes());

  }

  tableAction(event) {
    if(event.type=='addLock'){
      this.store.dispatch(new selectedLock({lock:event.row}));
      this.store.dispatch(new locksBikesSearchResult({ locksSearch: {} }))
      this.store.dispatch(new searchLocksBikesForm({form:{isSearch:false, imeiOqr:""}}));
      this.dialogRef.close()
    }
  }


  search() {
    this.form = {}
    if (this.valueSearchLocks != "") {
      this.form.isSearch = true
      this.form.imeiOqr = this.valueSearchLocks
      this.store.dispatch(new searchLocksBikesForm({ form: this.form }));
      this.store.dispatch(new skipPageLocks({ skip: 0 }));
      this.store.dispatch(new getModalLocksSearch());

    }
  }


  onInputChange(){
    if(this.valueSearchLocks==""){
      this.store.dispatch(new getCountLocksBikes());
    }
  }

  closeModal() {
    this.dialogRef.close()
    this.valueSearchLocks="";
    this.store.dispatch(new searchLocksBikesForm({form:{isSearch:false, imeiOqr:""}}));
    this.store.dispatch(new locksBikesSearchResult({ locksSearch: {} }))

  }

  ngOnDestroy() {
    this.locksBikesSubscription.unsubscribe();
    this.locksPageSizeSubscription.unsubscribe();
    this.totalLocksSubscription.unsubscribe();
  }
}
