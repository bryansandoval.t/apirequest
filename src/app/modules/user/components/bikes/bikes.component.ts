import { Component, OnInit, Input, Inject, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
//Informacion sobre la tabla
import { tableTypes } from 'src/app/constants/table.constants';
import { Column } from 'src/app/interfaces/table.interfaces';
import { TableInfo, BikesTable } from '../../../../interfaces/table.interfaces';
import { ModalsBikeComponent } from '../bikes/modals-bike/modals-bike.component';
import { getTotalBikes, skipPageBike, showPageBikes, searchBikeForm, filterSearchLocksBikes, modalTypeBike, modalBike, selectedLock, bikesSearchResult, getStationsBikes, getMasterListTypesBikes, getMasterListStateBikes } from 'src/app/config/global.actions';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import * as _ from 'lodash';


@Component({
  selector: 'elastic-bikes',
  templateUrl: './bikes.component.html',
  styleUrls: ['./bikes.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class BikesComponent implements OnInit {

  bikeColumns: Column[];
  currentPageBike: any[];
  loading: boolean;
  colapsable: boolean = false
  imeiOqr: any = "";
  selectStation: any = "";
  form: any = {};

  //TEMP volar
  allBike: any[];
  title: string = "Bicicletas";
  stationsSelect: any = []


  stationsBikes$: Observable<any> = this.store.select(state => state['user']['stationsBikes']);
  stationsBikesSubscription: Subscription;
  stationsBikes: any;

  setBikes$: Observable<any> = this.store.select(state => state['user']['setBikes']);
  setBikesSubscription: Subscription;
  setBikes: any;

  bikePageSize$: Observable<any> = this.store.select(state => state['user']['bikePageSize']);
  bikePageSizeSubscription: Subscription;
  bikePageSize: any;

  bikesSearch$: Observable<any> = this.store.select(state => state.user.bikesSearchResult);
  bikesSearchSubscription: Subscription;
  bikesSearch: any;

  totalBikes$: Observable<any> = this.store.select(state => state['user']['totalBikes']);
  totalBikesSubscription: Subscription;
  totalBikes: any;

  searchBikesForm$: Observable<any> = this.store.select(state => state['user']['searchBikesForm']);
  searchBikesFormSubscription: Subscription;
  searchBikesForm: any;


  @Input() tableData: TableInfo = BikesTable;
  storeInfo = { store: 'user', storeState: 'user-infos' };
  storeInfoSearch = { store: 'user', storeState: 'user-infosSearch' };

  constructor(
    // @Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>,
    private formBuilder: FormBuilder,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.store.dispatch(new getStationsBikes());
    this.store.dispatch(new getMasterListTypesBikes());
    this.store.dispatch(new getMasterListStateBikes());
    this.store.dispatch(new getTotalBikes());

    this.stationsBikesSubscription = this.stationsBikes$.subscribe((stations) => {
      this.stationsBikes = stations;
    })

    this.bikesSearchSubscription = this.bikesSearch$.subscribe((bikesSearch) => {
      if(bikesSearch.length>0){
        this.bikesSearch = true;
      }else{
        this.bikesSearch = false;
      }
    })

    this.setBikesSubscription = this.setBikes$.subscribe((bikes) => {
      this.setBikes = bikes;
      this.currentPageBike = this.setBikes
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })

    this.bikePageSizeSubscription = this.bikePageSize$.subscribe((pageSize) => {
      this.bikePageSize = pageSize;
    })

    this.totalBikesSubscription = this.totalBikes$.subscribe((total) => {
      this.totalBikes = total;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })

    this.searchBikesFormSubscription = this.searchBikesForm$.subscribe((searchBikes) => {
      this.searchBikesForm = searchBikes;
      this.imeiOqr=this.searchBikesForm.imeiOqr
      this.selectStation=this.searchBikesForm.station
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })

    //inicializar columnas
    this.bikeColumns = [
      {
        header: 'Numero',
        propertyPath: 'number',
        type: tableTypes.string
      },
      {
        header: 'Tipo',
        propertyPath: 'type',
        type: tableTypes.label,
        labels: [
          { value: 'mechanic', label: 'Mecanica' },
          { value: 'electric', label: 'Electrica' },
        ]
        /*  labels: [
           { value: 'mechanic', label: 'Mecanica', bgcolor: 'rgb(165 241 181)', fontColor: '#c63737' },
           { value: 'electric', label: 'Electrica', bgcolor: '#ffcdd2', fontColor: '#c63737' },
         ] */
      },
      {
        header: 'Imei candado',
        propertyPath: 'lock.imei',
        type: tableTypes.string
      },
      {
        header: 'Qr candado',
        propertyPath: 'lock.qrNumber',
        type: tableTypes.string,
      },
      {
        header: 'Viaje realizado',
        propertyPath: 'tripCount',
        type: tableTypes.number,
        width: "195px"
      },
      {
        header: 'Estado',
        propertyPath: 'state',
        type: tableTypes.label,
        labels: [
          { value: 'active', label: 'Activo' },
          { value: 'trip', label: 'Viaje' },
          { value: 'finished', label: 'Finalizado' },
        ]
        /* labels: [
          { value: 'active', label: 'Activo', bgcolor: 'rgb(165 241 181)', fontColor: '#c63737' },
          { value: 'trip', label: 'Viaje', bgcolor: '#ffcdd2', fontColor: '#c63737' },
          { value: 'finished', label: 'Finalizado', bgcolor: '#ffcdd2', fontColor: '#c63737' },
        ] */
      },
      {
        header: 'Bateria',
        propertyPath: 'battery',
        type: tableTypes.number,
      },
      {
        header: 'Estacion actual',
        propertyPath: 'station.name',
        type: tableTypes.string,

      },
      {
        header: 'Acciones',
        propertyPath: '',
        type: tableTypes.actions,
        width: "100px",
        actions: [
          { label: 'Editar', type: 'edit' },
        ]
      }
    ];

    this.loading = false;
    /* this.totalTrips = this.allTrips.length; */

    /*  this.tripsPageSize = 2; */
  }

  tableAction(event) {
    if (event.type === 'edit') {
      if (event.row.lock) {
        this.store.dispatch(new selectedLock({ lock: event.row.lock }));
      } else {
        this.store.dispatch(new selectedLock({ lock: {} }));
      }
      let infoModal = _.cloneDeep(event.row)
      infoModal.number = String(infoModal.number)
      this.store.dispatch(new modalTypeBike({ type: "edit" }));
      this.store.dispatch(new modalBike({ bike: infoModal }));
      this.dialog.open(ModalsBikeComponent, { width: '50vw', height: '27rem' })
    };
  }

  openModal(event) {
    this.store.dispatch(new modalTypeBike({ type: "add" }));
    this.store.dispatch(new modalBike({ bike: {} }));
    this.store.dispatch(new selectedLock({ lock: {} }));
    this.dialog.open(ModalsBikeComponent, { width: '50vw', height: '27rem' })

  }


  changePage(event) {
    this.store.dispatch(new skipPageBike({ skip: event.first }));
    this.store.dispatch(new showPageBikes());
  }
  search() {
    this.form = {}
    if (this.imeiOqr != "" || this.selectStation != "") {
      this.form.isSearch = true
      this.form.imeiOqr = this.imeiOqr
      this.form.station = this.selectStation
      this.store.dispatch(new searchBikeForm({ form: this.form }));
      this.store.dispatch(new filterSearchLocksBikes());
      this.store.dispatch(new skipPageBike({ skip: 0 }));

    }
  }

  viewAll() {
    this.store.dispatch(new getTotalBikes());
    this.store.dispatch(new skipPageBike({ skip: 0 }));
    this.store.dispatch(new bikesSearchResult({ bikesSearch: [] }));
    this.store.dispatch(new searchBikeForm({ form: { isSearch: false, imeiOqr: "", station: "" } }));
    /* this.imeiOqr = "";
    this.selectStation = ""; */
  }


  ngOnDestroy() {
    this.stationsBikesSubscription.unsubscribe();
    this.setBikesSubscription.unsubscribe();
    this.bikePageSizeSubscription.unsubscribe();
    this.totalBikesSubscription.unsubscribe();
    this.searchBikesFormSubscription.unsubscribe();
    this.bikesSearchSubscription.unsubscribe();
  }

}
