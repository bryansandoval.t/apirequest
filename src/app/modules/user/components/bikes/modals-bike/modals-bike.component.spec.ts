import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalsBikeComponent } from './modals-bike.component';

describe('ModalsBikeComponent', () => {
  let component: ModalsBikeComponent;
  let fixture: ComponentFixture<ModalsBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalsBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalsBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
