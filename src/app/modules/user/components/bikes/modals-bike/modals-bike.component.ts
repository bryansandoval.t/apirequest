import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LocksBikeComponent } from '../../bikes/locks-bike/locks-bike.component'
import { modalBike, postBike, selectedLock, patchBike, getCountLocksBikes, searchBikeForm } from 'src/app/config/global.actions';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'elastic-modals-bike',
  templateUrl: './modals-bike.component.html',
  styleUrls: ['./modals-bike.component.scss']
})
export class ModalsBikeComponent implements OnInit {

  stateBikeSelect: any = ""
  stateBike: any = ""

  stationsBikes$: Observable<any> = this.store.select(state => state['user']['stationsBikes']);
  stationsBikesSubscription: Subscription;
  stationsBikes: any;

  typesBikes$: Observable<any> = this.store.select(state => state['user']['typesBikes']);
  typesBikesSubscription: Subscription;
  typesBikes: any

  stateBikes$: Observable<any> = this.store.select(state => state['user']['stateBikes']);
  stateBikesSubscription: Subscription;
  stateBikes: any

  modalTypeBike$: Observable<any> = this.store.select(state => state['user']['modalTypeBike']);
  modalTypeBikeSubscription: Subscription;
  modalTypeBike: any

  selectedLock$: Observable<any> = this.store.select(state => state['user']['selectedLock']);
  selectedLockSubscription: Subscription;
  selectedLock: any

  modalBike$: Observable<any> = this.store.select(state => state['user']['modalBike']);
  modalBikeSubscription: Subscription;
  modalBike: any


  form: FormGroup;
  formEdit: FormGroup;
  stationSelect: any = "";
  selectedStation: any
  selectedType: any
  selectedState: any
  numberEdit: any
  /*  stationNameModal:any=""
   stationIdModal:any="" */

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ModalsBikeComponent>,
    private router: Router,
    private store: Store<any>,
    private formBuilder: FormBuilder,
    private ref: ChangeDetectorRef) { }



  ngOnInit() {

    this.form = this.formBuilder.group({
      number: ['', [Validators.required, Validators.min(1)]],
      stationId: ['', Validators.required],
      type: ['', Validators.required],
      state: ['', Validators.required],
    });

    this.formEdit = this.formBuilder.group({
      number: ['', [Validators.required, Validators.min(1)]],
      stationId: ['', Validators.required],
      type: ['', Validators.required],
      state: ['', Validators.required],
    });





    this.typesBikesSubscription = this.typesBikes$.subscribe((types) => {
      this.typesBikes = types;
    })

    this.stateBikesSubscription = this.stateBikes$.subscribe((states) => {
      this.stateBikes = states;
    })

    this.stationsBikesSubscription = this.stationsBikes$.subscribe((stations) => {
      this.stationsBikes = stations;
    })

    this.modalTypeBikeSubscription = this.modalTypeBike$.subscribe((modal) => {
      this.modalTypeBike = modal;
    })

    this.selectedLockSubscription = this.selectedLock$.subscribe((lock) => {
      this.selectedLock = lock;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);

    })

    this.modalBikeSubscription = this.modalBike$.subscribe((modalBike) => {
      this.modalBike = modalBike;
      if(modalBike.station){
        this.formEdit.setValue({
          number: this.modalBike.number,
          stationId: this.modalBike.station.id,
          type: this.modalBike.type,
          state: this.modalBike.state
        });
      }
    })
  }

  modalLockBike() {
    console.log('--------------------')
    this.store.dispatch(new getCountLocksBikes());
    this.dialog.open(LocksBikeComponent, { width: '90vw', height: '38rem' })
  }


  closeModal() {
    this.dialogRef.close()
    this.store.dispatch(new modalBike({ bike: {} }));
  }

  delete() {
    this.store.dispatch(new selectedLock({ lock: {} }));
  }

  saveModal() {
    if (this.form.valid) {
      this.dialogRef.close()
      this.store.dispatch(new postBike({ bike: this.form.value }));
    }
  }

  saveModalEdit() {
    if (this.formEdit.valid) {
      this.store.dispatch(new patchBike({ idBike: this.modalBike.id, formBike: this.formEdit.value }));
      this.dialogRef.close()
    }
  }


  ngOnDestroy() {
    this.stationsBikesSubscription.unsubscribe();
    this.typesBikesSubscription.unsubscribe();
    this.stateBikesSubscription.unsubscribe();
    this.modalTypeBikeSubscription.unsubscribe();
    this.selectedLockSubscription.unsubscribe();
    this.modalBikeSubscription.unsubscribe();
  }
}
