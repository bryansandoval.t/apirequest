import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { setMosalStationInfo, stationFormValidation } from 'src/app/config/global.actions';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'elastic-stations-modal',
  templateUrl: './stations-modal.component.html',
  styleUrls: ['./stations-modal.component.scss']
})
export class StationsModalComponent implements OnInit {
  checked1: boolean = true;
  checked2: boolean = true;
  
  constructor(
    private globalService: GlobalService,
    private store: Store<any>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<StationsModalComponent>,
  ) { }

  modalType$: Observable<any> = this.store.select(state => state['apiData']['stationModalType']);
  modalTypeSubscription: Subscription;
  modalType: any;

  stationFormErrors$: Observable<any> = this.store.select(state => state['apiData']['stationFormErrors']);
  stationFormErrorsSubscription: Subscription;
  stationFormErrors: any = {};

  modalStationInfo$: Observable<any> = this.store.select(state => state['apiData']['modalStationInfo']);
  modalStationInfoSubscription: Subscription;
  modalStationInfo: any;

  startLat = 1.2;
  startLgn = -77.27;
  latitude: number = this.startLat;
  longitude: number = this.startLgn;

  hoursA: any = '';
  hoursB: any = '';

  formGroup: FormGroup = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    openingTime: new FormControl(''),
    closingTime: new FormControl(''),
    bikesCapacity: new FormControl(''),
    state: new FormControl('true'),
    latitude: new FormControl(''),
    longitude: new FormControl(''),
  })


  listSelectRadio = [
    { name: "Activo ", value: true, checked: true },
    { name: "Inactivo ", value: false, checked: false }
  ]

  ngOnInit() {
    this.modalTypeSubscription = this.modalType$.subscribe((type) => {
      this.modalType = type
    })
    this.stationFormErrorsSubscription = this.stationFormErrors$.subscribe((formErrors) => {
      this.stationFormErrors = formErrors
    })
    this.modalStationInfoSubscription = this.modalStationInfo$.subscribe((stationInfo) => {
      if (stationInfo) {
        this.modalStationInfo = stationInfo;
        this.hoursA = stationInfo.openingTime;
        this.hoursB = stationInfo.closingTime;
        this.listSelectRadio[0].checked = stationInfo.state?true:false;
        this.listSelectRadio[1].checked = stationInfo.state?false:true;


        this.latitude = parseFloat(stationInfo.latitude) ? parseFloat(stationInfo.latitude) : this.latitude;
        this.longitude = parseFloat(stationInfo.longitude) ? parseFloat(stationInfo.longitude) : this.longitude;
        this.formGroup.setValue({
          name: stationInfo.name,
          address: stationInfo.address,
          openingTime: stationInfo.openingTime,
          closingTime: stationInfo.closingTime,
          bikesCapacity: stationInfo.bikesCapacity,
          state: stationInfo.state,
          latitude: this.latitude != this.startLat ? this.latitude : "",
          longitude: this.longitude != this.startLgn ? this.longitude : ""
        });
      };
    })
  }

  changeValidation() {
    this.store.dispatch(new setMosalStationInfo({ modalStationInfo: { ...this.modalStationInfo, ...this.formGroup.value }, stationModalType: { modalType: this.modalType.modalType, status: false } }));
    this.store.dispatch(new stationFormValidation());
  }

  ubication(event: any) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.formGroup.controls['latitude'].setValue(this.latitude);
    this.formGroup.controls['longitude'].setValue(this.longitude);
    this.formGroup.controls['latitude'].markAsTouched();
    this.formGroup.controls['longitude'].markAsTouched();
    this.store.dispatch(new setMosalStationInfo({ modalStationInfo: { ...this.modalStationInfo, ...this.formGroup.value }, stationModalType: { modalType: this.modalType.modalType, status: false } }));
    this.store.dispatch(new stationFormValidation());
  };

  addFormGroup(event, property) {
    this.formGroup.controls[property].setValue(event);
    this.formGroup.controls[property].markAsTouched();
  };

  onSubmit() {
    const objectsKey: any = Object.keys(this.formGroup.controls);
    this.store.dispatch(new setMosalStationInfo({ modalStationInfo: { ...this.modalStationInfo, ...this.formGroup.value }, stationModalType: { modalType: this.modalType.modalType, status: true } }));
    this.store.dispatch(new stationFormValidation());


    objectsKey.find(control => {
      this.formGroup.controls[control].markAsTouched();
    });
    setTimeout(() => {
      if (Object.keys(this.stationFormErrors).length == 0) {
        this.closeModal()
      }else{
        this.globalService.topEndSweetAlert('Hay errores en el formulario', 'error', 2000)
      }
    }, 200);
  };


  closeModal() {
    this.dialogRef.close();
  };

  ngOnDestroy(): void {
    this.modalStationInfoSubscription.unsubscribe();
    this.stationFormErrorsSubscription.unsubscribe();
    this.modalTypeSubscription.unsubscribe();
  }

}
