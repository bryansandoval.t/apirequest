import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
//Informacion sobre la tabla
import { StationsModalComponent } from './stations-modal/stations-modal.component';
import { getStations, setMosalStationInfo } from 'src/app/config/global.actions';
import { Observable, Subscription } from 'rxjs';
import { StationsTable, TableInfo } from 'src/app/interfaces/table.interfaces';
import { ListColumn } from 'src/app/modules/core/list/list-column.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'elastic-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class StationsComponent implements OnInit {
  tableName: string = "Estaciones"
  filterLength: number = 0;
  columns: ListColumn[] = StationsTable.tableColumns;
  
  tableStations$: Observable<any> = this.store.select(state => state['apiData']['tableStations']);
  tableStationsSubscription: Subscription;
  tableStations: MatTableDataSource<Element[]>;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(
    private store: Store<any>,
    private dialog: MatDialog,
  ) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  ngOnInit() {
    this.store.dispatch(new getStations())
    this.tableStationsSubscription = this.tableStations$.subscribe((tableStation) => {
      this.tableStations = new MatTableDataSource(tableStation);
      this.tableStations.paginator = this.paginator;
      this.tableStations.sort = this.sort;
    })
  }

  onFilterChange(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableStations.filter = filterValue.trim().toLowerCase();
  }

  openModal(modalType, stationInfo) {
    this.store.dispatch(new setMosalStationInfo({modalStationInfo: stationInfo, stationModalType: {modalType, status: true}}))
    this.dialog.open(StationsModalComponent, {width: '70vw', height: '80vh'})
  }

  ngOnDestroy(): void {
    this.tableStationsSubscription.unsubscribe();
  }

}