import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { dispatch } from 'rxjs/internal/observable/pairs';
import { ListColumn } from 'src/app/modules/core/list/list-column.model';
//Informacion sobre la tabla
import { TableInfo, OrganizationTable } from '../../../../interfaces/table.interfaces';
import { ModalOrganizationComponent } from './modal-organization/modal-organization.component';
import { getOrganizations, setCloseModalOrganization, setModalOrganization, setModalOrganizationType, setOrganizationFormErrors } from 'src/app/config/global.actions';


@Component({
  selector: 'elastic-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class OrganizationComponent implements OnInit {

  @Input() tableData: TableInfo = OrganizationTable;
  storeInfo = { store: 'user', storeState: 'user-infos' };
  storeInfoSearch = { store: 'user', storeState: 'user-infosSearch' };
  columns: ListColumn[] = OrganizationTable.tableColumns;
  tableName: string = 'Organizaciones';

  tableOrganizations$: Observable<any> = this.store.select(state => state['user']['tableOrganizations']);
  tableOrganizationsSubscription: Subscription;
  tableOrganizations: MatTableDataSource<Element[]>;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.store.dispatch(new getOrganizations());
    this.tableOrganizationsSubscription = this.tableOrganizations$.subscribe((tableOrganizations) => {
      this.tableOrganizations = new MatTableDataSource(tableOrganizations);
      this.tableOrganizations.paginator = this.paginator;
      this.tableOrganizations.sort = this.sort;
    })
  }

  onFilterChange(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableOrganizations.filter = filterValue.trim().toLowerCase();
  }

  openModal(modalType, organizationsInfo) {
    this.store.dispatch(new setCloseModalOrganization({closeModalOrganization: false}));
    this.store.dispatch(new setModalOrganization({ modalOrganization: organizationsInfo }));
    this.store.dispatch(new setModalOrganizationType({ modalOrganizationType: modalType }));
    this.store.dispatch(new setOrganizationFormErrors({ organizationFormErrors: {} }));

    this.dialog.open(ModalOrganizationComponent, { width: '70vw', height: '80vh' })
  }

  ngOnDestroy(): void {
    this.tableOrganizationsSubscription.unsubscribe();
  }

}
