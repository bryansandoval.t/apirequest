import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { organizationFormValidation, setOrganizationLogo } from 'src/app/config/global.actions';

@Component({
  selector: 'elastic-modal-organization',
  templateUrl: './modal-organization.component.html',
  styleUrls: ['./modal-organization.component.scss']
})
export class ModalOrganizationComponent implements OnInit {

  images: any = [];
  loadImages: any = '';
  shouldLoad: boolean = false;
  deleteImages: any = [];
  selectedCategories: any = [];
  logoTouched: boolean = false;

  constructor(
    private store: Store<any>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<ModalOrganizationComponent>,
  ) { }

  closeModalOrganization$: Observable<any> = this.store.select(state => state['user']['closeModalOrganization']);
  closeModalOrganizationSubscription: Subscription;

  modalOrganization$: Observable<any> = this.store.select(state => state['user']['modalOrganization']);
  modalOrganizationSubscription: Subscription;
  modalOrganization: any;

  modalOrganizationType$: Observable<any> = this.store.select(state => state['user']['modalOrganizationType']);
  modalOrganizationTypeSubscription: Subscription;
  modalOrganizationType: any;

  organizationFormErrorsErrors$: Observable<any> = this.store.select(state => state['user']['organizationFormErrors']);
  organizationFormErrorsErrorsSubscription: Subscription;
  organizationFormErrorsErrors: any = {};

  formGroup: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    linkPlaystore: new FormControl('', Validators.required),
    linkAppstore: new FormControl('', Validators.required),
  })

  ngOnInit() {
    this.modalOrganizationSubscription = this.modalOrganization$.subscribe((organizations) => {
      this.modalOrganization = organizations;
      if (organizations) {
        this.formGroup.setValue({
          name: this.modalOrganization.name,
          linkPlaystore: this.modalOrganization.linkPlaystore,
          linkAppstore: this.modalOrganization.linkAppstore
        });
        this.loadImages = this.modalOrganization.logo;
      }
    })
    this.modalOrganizationTypeSubscription = this.modalOrganizationType$.subscribe((organizationsType) => {
      this.modalOrganizationType = organizationsType
    })
    this.closeModalOrganizationSubscription = this.closeModalOrganization$.subscribe((closeModal) => {
      closeModal?this.closeModal():null
    })
    this.organizationFormErrorsErrorsSubscription = this.organizationFormErrorsErrors$.subscribe((errors) => {
      this.organizationFormErrorsErrors = errors
    })
  }

  changeValidation() {
    this.store.dispatch(new setOrganizationLogo({ organizationLogo: { logoInfo: this.images[0], shouldLoad: this.shouldLoad } }))
    this.store.dispatch(new organizationFormValidation({
      modalOrganization: this.formGroup.value,
      modalOrganizarionType: this.modalOrganizationType,
      shouldSubmit: "chenge"
    }))
  }

  closeModal() {
    this.dialogRef.close();
  };

  deleteImage(url: any) {
    this.images = [];
    this.logoTouched = true;
  }

  deleteLoadImage(url: any) {
    this.shouldLoad = true;
    this.loadImages = '';
    this.deleteImages.push(url);
    this.logoTouched = true;
  }

  uploadFile(event) {
    this.logoTouched = true;
    this.shouldLoad = true;
    this.loadImages = '';
    this.images = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length
      let filesAll = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.images.push({
            data: filesAll[i],
            image: event.target.result
          });
          this.changeValidation()
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  onSubmit() {
    const objectsKey: any = Object.keys(this.formGroup.controls);
    this.store.dispatch(new setOrganizationLogo({ organizationLogo: { logoInfo: this.images[0], shouldLoad: this.shouldLoad } }))
    this.store.dispatch(new organizationFormValidation({
      modalOrganization: this.formGroup.value,
      modalOrganizarionType: this.modalOrganizationType,
      shouldSubmit: "submit"
    }))

    this.logoTouched = true;
    objectsKey.find(control => {
      this.formGroup.controls[control].markAsTouched();
    });
  }

}

