import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
//import { TableInfo, TripsTable } from 'src/app/interfaces/table.interfaces';
import { tableTypes } from 'src/app/constants/table.constants';
import { Column, tripsColumns } from 'src/app/interfaces/table.interfaces';
import { EditTripStateComponent } from '../edit-trip-state/edit-trip-state.component';
import { MatAccordion } from '@angular/material/expansion';
import { getStations, getTotalTrips, loadTripStates, setLoadingTripsPage, setModalTrip, setSearchTripsForm, setTripsFormStatus, setTripsPageNumber, showGlobalPageTrips, showPageTrips, tripFormValidation } from 'src/app/config/global.actions';
import { Observable, Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ReportTripsModalComponent } from './report-trips-modal/report-trips-modal.component';


@Component({
  selector: 'elastic-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class TripsComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>,
    private ref: ChangeDetectorRef
  ) { }

  tripsColumns: Column[] = tripsColumns; //inicializar columnas
  loadingTable: boolean;
  tripsPageSize: number = 20;
  selected: any;

  formGroup: FormGroup = new FormGroup({
    idNumber: new FormControl(''),
    stateTrip: new FormControl(''),
    nameStation: new FormControl(''),
    numberBike: new FormControl(''),
    endDate: new FormControl(''),
    startDate: new FormControl(''),
  })

  title: string = "Prestamos";
  currentPageTrips$: Observable<any> = this.store.select(state => state['user']['currentPageTrips']);
  currentPageTripsSubscription: Subscription;
  currentPageTrips: any[]; //Pagina actual

  totalTrips$: Observable<any> = this.store.select(state => state['user']['totalTrips']);
  totalTripsSubscription: Subscription;
  totalTrips: any;

  tripStates$: Observable<any> = this.store.select(state => state['user']['tripStates']);
  tripStatesSubscription: Subscription;
  tripStates: any;

  tripsFormErrors$: Observable<any> = this.store.select(state => state['user']['tripsFormErrors']);
  tripsFormErrorsSubscription: Subscription;
  tripsFormErrors: any;

  tripsFormStatus$: Observable<any> = this.store.select(state => state['apiData']['tripsFormStatus']);
  tripsFormStatusSubscription: Subscription;
  tripsFormStatus: any;

  loadingTripsPage$: Observable<any> = this.store.select(state => state['apiData']['loadingTripsPage']);
  loadingTripsPageSubscription: Subscription;
  loadingTripsPage: any;

  tableStations$: Observable<any> = this.store.select(state => state['apiData']['tableStations']);
  tableStationsSubscription: Subscription;
  tableStations: any[];

  allTrips: any = []; //TEMP volar


  ngOnInit() {
    this.store.dispatch(new setLoadingTripsPage({ loadingTripsPage: true }));
    this.store.dispatch(new getStations());
    this.store.dispatch(new loadTripStates());
    this.store.dispatch(new getTotalTrips());

    this.loadingTripsPageSubscription = this.loadingTripsPage$.subscribe((loading) => {
      this.loadingTable = loading
    });

    this.tripStatesSubscription = this.tripStates$.subscribe((states) => {
      this.tripStates = states
    });

    this.tripsFormErrorsSubscription = this.tripsFormErrors$.subscribe((errors) => {
      this.tripsFormErrors = errors
    });

    this.tableStationsSubscription = this.tableStations$.subscribe((tableStations) => {
      this.tableStations = tableStations
    });

    this.totalTripsSubscription = this.totalTrips$.subscribe((totalTrips) => {
      this.totalTrips = totalTrips;
    });

    this.tripsFormStatusSubscription = this.tripsFormStatus$.subscribe((tripsFormStatus) => {
      this.tripsFormStatus = tripsFormStatus
    });

    this.currentPageTripsSubscription = this.currentPageTrips$.subscribe((currentPage: any) => {
      this.currentPageTrips = [...currentPage].splice(0, 20);

      if (this.tripsFormStatus) {
        this.allTrips = currentPage;
      } else {
        this.allTrips = this.allTrips.length == '0' ? currentPage : this.allTrips;
      }

      setTimeout(() => {
        this.ref.detectChanges();
      }, 500);
    });
  }

  onSubmitAccordeon() {
    this.store.dispatch(new setSearchTripsForm({ searchTripsForm: this.formGroup.value }));
    this.store.dispatch(new tripFormValidation({ searchTripsForm: 'submit' }));
  }

  onFilterChange(event: Event) {
    const value = (event.target as HTMLInputElement).value;
  }

  addFormGroup(event, property) {
    this.formGroup.controls[property].setValue(event);
    this.formGroup.controls[property].markAsTouched();
  };

  changeValidation() {
    this.store.dispatch(new setSearchTripsForm({ searchTripsForm: this.formGroup.value }));
    this.store.dispatch(new tripFormValidation({ searchTripsForm: 'change' }));
  }

  tableAction(event) {
    this.store.dispatch(new setModalTrip({ modalTrip: event.row }));
    if (event.type === 'edit') {
      this.dialog.open(EditTripStateComponent, { width: '50vw', height: '80vh' })
    };
  }
  clearFilter() {
    this.formGroup.setValue({
      idNumber: "",
      stateTrip: "",
      nameStation: "",
      numberBike: "",
      endDate: "",
      startDate: "",
    });
  }
  showAll() {
    this.store.dispatch(new setLoadingTripsPage({ loadingTripsPage: true }));
    this.store.dispatch(new setTripsFormStatus({ tripsFormStatus: false }));
    this.store.dispatch(new setSearchTripsForm({ searchTripsForm: this.formGroup.value }));
    this.store.dispatch(new getTotalTrips());
  }

  changePage(event) {
    if (!this.tripsFormStatus && event.first != 0) {
      this.store.dispatch(new setLoadingTripsPage({ loadingTripsPage: true }));
    }
    if (event.first !== 0 && !this.tripsFormStatus) {
      this.store.dispatch(new showPageTrips({ tripsPageNumber: event.first }));
    }
    this.currentPageTrips = this.allTrips.slice(event.first, (event.first + event.rows));
  }

  reportTrips(){
    this.dialog.open(ReportTripsModalComponent, { width: '60vw' })
  }

  ngOnDestroy() {
    this.currentPageTripsSubscription.unsubscribe();
    this.totalTripsSubscription.unsubscribe();
    this.tableStationsSubscription.unsubscribe();
    this.tripsFormStatusSubscription.unsubscribe();
    this.loadingTripsPageSubscription.unsubscribe();
    this.tripsFormErrorsSubscription.unsubscribe();
    this.tripStatesSubscription.unsubscribe();
  }

}
