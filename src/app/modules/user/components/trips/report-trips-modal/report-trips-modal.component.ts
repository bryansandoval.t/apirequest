import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getReportTrips, setLoadingDownload } from 'src/app/config/global.actions';
import { errorDateValidator } from 'src/app/interfaces/form.interfaces';

@Component({
  selector: 'elastic-report-trips-modal',
  templateUrl: './report-trips-modal.component.html',
  styleUrls: ['./report-trips-modal.component.scss']
})
export class ReportTripsModalComponent implements OnInit {
  formGroup: any;
  btnOnSubmit: boolean = false;
  maxDate: any;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ReportTripsModalComponent>,
    private store: Store<any>,
  ) { }

  loadingDownload$: Observable<any> = this.store.select(state => state['user']['loadingDownload']);
  loadingDownloadSubscription: Subscription;
  loadingDownload: any;

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      startDate: new FormControl('', [Validators.required]),
      endDate: new FormControl('', [Validators.required]),
    },  { validator: errorDateValidator });

    this.loadingDownloadSubscription = this.loadingDownload$.subscribe((loading) => {
      this.loadingDownload = loading;
    })
  }
  get startDate() { return this.formGroup.get('startDate'); }
  get endDate() { return this.formGroup.get('endDate'); }
  onPasswordInput() {
    if (this.formGroup.hasError('erorDate'))
      this.endDate.setErrors([{ 'erorDate': true }]);
    else
      this.endDate.setErrors(null);
  }

  onSubmit() {
    this.btnOnSubmit = true;
    if(this.formGroup.status == 'VALID'){
      this.store.dispatch(new setLoadingDownload({loadingDownload: true}))
      this.store.dispatch(new getReportTrips({dateRangeTrips: this.formGroup.value}))
    }
  }

  closeModal() {
    this.dialogRef.close();
  };
}
