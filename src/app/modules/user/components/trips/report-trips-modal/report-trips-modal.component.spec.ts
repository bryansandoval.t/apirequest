import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTripsModalComponent } from './report-trips-modal.component';

describe('ReportTripsModalComponent', () => {
  let component: ReportTripsModalComponent;
  let fixture: ComponentFixture<ReportTripsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTripsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTripsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
