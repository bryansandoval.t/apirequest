import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { setCurrentPageLockCommands, setLoadingCommandsTable, setLockCommandsPageNumber, setTotalLockCommands, showPageLockCommands } from 'src/app/config/global.actions';
import { tableTypes } from 'src/app/constants/table.constants';
import { Column, historysColumns } from 'src/app/interfaces/table.interfaces';


@Component({
  selector: 'elastic-modal-locks',
  templateUrl: './modal-locks.component.html',
  styleUrls: ['./modal-locks.component.scss']
})
export class ModalLocksComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<ModalLocksComponent>,
    private dialog: MatDialog,
    private router: Router,
    private ref: ChangeDetectorRef,
    private store: Store<any>,
  ) { }

  historysColumns: Column[] = historysColumns;
  historysPageSize = 20;

  //TEMP volar
  title: string = "Historial";

  currentPageLockCommands$: Observable<any> = this.store.select(state => state.user.currentPageLockCommands);
  currentPageLockCommandsSubscription: Subscription;
  currentPageLockCommands: Array<any>;

  loadingCommandsTable$: Observable<any> = this.store.select(state => state.user.loadingCommandsTable);
  loadingCommandsTableSubscription: Subscription;
  loadingCommandsTable: boolean;

  totalLockCommands$: Observable<any> = this.store.select(state => state.user.totalLockCommands);
  totalLockCommandsSubscription: Subscription;
  totalLockCommands: number;

  ngOnInit() {
    this.currentPageLockCommandsSubscription = this.currentPageLockCommands$.subscribe((lockCommands) => {
      this.currentPageLockCommands = lockCommands;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    });
    this.totalLockCommandsSubscription = this.totalLockCommands$.subscribe((totalLockCommands) => {
      this.totalLockCommands = totalLockCommands;
    });
    this.loadingCommandsTableSubscription = this.loadingCommandsTable$.subscribe((loading) => {
      this.loadingCommandsTable = loading;
    });
  }

  changePage(event) {
    this.store.dispatch(new setLockCommandsPageNumber({ lockCommandsPageNumber: event.first }))
    this.store.dispatch(new setLoadingCommandsTable({ loadingCommandsTable: true }))
    this.store.dispatch(new showPageLockCommands({ lockCommandsPageNumber: event.first }))
  }

  closeModal() {
    this.store.dispatch(new setTotalLockCommands({ totalLockCommands: 0 }))
    this.store.dispatch(new setCurrentPageLockCommands({ currentPageLockCommands: [] }))
    this.dialogRef.close();
  };

  ngOnDestroy() {
    this.currentPageLockCommandsSubscription.unsubscribe()
    this.loadingCommandsTableSubscription.unsubscribe()
    this.totalLockCommandsSubscription.unsubscribe()
  }

  tableAction(e) {
  }
}
