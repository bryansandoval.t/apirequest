import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalLocksComponent } from './modal-locks.component';

describe('ModalLocksComponent', () => {
  let component: ModalLocksComponent;
  let fixture: ComponentFixture<ModalLocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalLocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
