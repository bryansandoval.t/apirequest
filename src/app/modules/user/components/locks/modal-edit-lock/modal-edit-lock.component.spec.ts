import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditLockComponent } from './modal-edit-lock.component';

describe('ModalEditLockComponent', () => {
  let component: ModalEditLockComponent;
  let fixture: ComponentFixture<ModalEditLockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditLockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditLockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
