import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { patchLock, setSelectedLockInfo, validateEditLockForm } from 'src/app/config/global.actions';

@Component({
  selector: 'elastic-modal-edit-lock',
  templateUrl: './modal-edit-lock.component.html',
  styleUrls: ['./modal-edit-lock.component.scss']
})
export class ModalEditLockComponent implements OnInit {
  btnOnSubmit: boolean = false;

  constructor(
    private store: Store<any>,
    private dialogRef: MatDialogRef<ModalEditLockComponent>,
  ) { }

  closeModalLocks$: Observable<any> = this.store.select(state => state['user']['closeModalLocks']);
  closeModalLocksSubscription: Subscription;

  tableOrganizations$: Observable<any> = this.store.select(state => state['user']['listOrganizations']);
  tableOrganizationsSubscription: Subscription;
  tableOrganizations: any = [];

  selectedLockInfo$: Observable<any> = this.store.select(state => state.user.selectedLockInfo);
  selectedLockInfoSubscription: Subscription;
  selectedLockInfo: any;

  formGroup: FormGroup = new FormGroup({
    qrNumber: new FormControl('', [Validators.required, Validators.minLength(5)]),
    mac: new FormControl('', Validators.required),
    simNumber: new FormControl('', [Validators.required, Validators.minLength(10)]),
    organizationId: new FormControl('', Validators.required),
  })

  ngOnInit() {
    this.closeModalLocksSubscription = this.closeModalLocks$.subscribe((closeModal) => {
      closeModal?this.closeModal():null
    })
    this.tableOrganizationsSubscription = this.tableOrganizations$.subscribe((tableOrganizations) => {
      this.tableOrganizations = tableOrganizations;
    })
    this.selectedLockInfoSubscription = this.selectedLockInfo$.subscribe((selectedLockInfo) => {
      console.log(selectedLockInfo)
      this.selectedLockInfo = selectedLockInfo
      this.formGroup.setValue({
        qrNumber: selectedLockInfo.qrNumber,
        mac: selectedLockInfo.mac,
        simNumber: selectedLockInfo.simNumber,
        organizationId: selectedLockInfo.organizationId,
      });
    })
  }

  onSubmit(){
    this.btnOnSubmit = true;
    console.log(this.formGroup)
    if(this.formGroup.status == "VALID"){
      this.store.dispatch(new validateEditLockForm({lockPatch: {id: this.selectedLockInfo.id, ...this.formGroup.value}}))
    }
  }

  closeModal() {
    this.dialogRef.close();
  };
}
