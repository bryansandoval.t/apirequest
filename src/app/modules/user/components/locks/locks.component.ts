import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { tableTypes } from 'src/app/constants/table.constants';
import { Column, locksColumns } from 'src/app/interfaces/table.interfaces';
import { ModalLocksComponent } from './modal-locks/modal-locks.component';
import { getListOrganizations, getTotalLockCommands, getTotalLocks, searchBikesByNumber, setCloseModalLocks, setLoadingCommandsTable, setLoadingLockTable, setLockPageNumber, setLocksSearchParams, setLocksSearchResult, setSelectedLockInfo, showPageLocks, showSweetAlert } from 'src/app/config/global.actions';
import { Observable, Subscription } from 'rxjs';
import { Alert } from 'selenium-webdriver';
import { ModalEditLockComponent } from './modal-edit-lock/modal-edit-lock.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'elastic-locks',
  templateUrl: './locks.component.html',
  styleUrls: ['./locks.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class LocksComponent implements OnInit {
  onFilterChangeValue: string;
  lockWithoutBike: boolean = false;
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private ref: ChangeDetectorRef,
    private store: Store<any>) { }

  locksColumns: Column[] = locksColumns;
  locksPageSize = 20;

  //TEMP volar
  allLocks: any[];
  title: string = "Candados";

  locksWithoutBikeState = new FormControl('');

  currentPageLocks$: Observable<any> = this.store.select(state => state.user.currentPageLocks);
  currentPageLocksSubscription: Subscription;
  currentPageLocks: Array<any>;

  totalLocks$: Observable<any> = this.store.select(state => state.user.totalLocks);
  totalLocksSubscription: Subscription;
  totalLocks: number;

  locksSearchParams$: Observable<any> = this.store.select(state => state.user.locksSearchParams);
  locksSearchParamsSubscription: Subscription;
  locksSearchParams: any;

  loadingLockTable$: Observable<any> = this.store.select(state => state.user.loadingLockTable);
  loadingLockTableSubscription: Subscription;
  loadingLockTable: boolean;

  ngOnInit() {
    this.store.dispatch(new setLoadingLockTable({ loadingLockTable: true }))
    this.store.dispatch(new getListOrganizations());
    this.store.dispatch(new getTotalLocks())

    this.loadingLockTableSubscription = this.loadingLockTable$.subscribe((loading) => {
      this.loadingLockTable = loading;
    })

    this.locksSearchParamsSubscription = this.locksSearchParams$.subscribe((locksSearchParams) => {
      this.locksSearchParams = locksSearchParams;
      this.locksWithoutBikeState.setValue(locksSearchParams.locksWithoutBikeState)
      this.lockWithoutBike = locksSearchParams.locksWithoutBikeState
    })

    this.totalLocksSubscription = this.totalLocks$.subscribe((totalLocks) => {
      this.totalLocks = totalLocks;
    })

    this.currentPageLocksSubscription = this.currentPageLocks$.subscribe((locks) => {
      this.currentPageLocks = locks;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    })
  }

  showAll() {
    this.store.dispatch(new setLocksSearchParams({
      locksSearchParams: {
        searchTerm: '',
        isSearching: false,
        locksWithoutBikeState: false
      }
    }))
    this.store.dispatch(new setLoadingLockTable({ loadingLockTable: true }))
    this.store.dispatch(new setLocksSearchResult({ locksSearchResult: [] }))
    this.store.dispatch(new getTotalLocks())
  }
  searchLocks() {
    if (this.onFilterChangeValue.length > 2) {
      this.store.dispatch(new setLoadingLockTable({ loadingLockTable: true }))
      this.store.dispatch(new setLocksSearchParams({
        locksSearchParams: {
          searchTerm: this.onFilterChangeValue,
          isSearching: true,
          locksWithoutBikeState: this.lockWithoutBike
        }
      }))
      this.store.dispatch(new searchBikesByNumber({ searchTerm: this.onFilterChangeValue }))
    } else {
      this.store.dispatch(new showSweetAlert({ title: 'Error', text: 'la busqueda debe ser mayor a 3 caracteres', type: 'error', buttonText: 'Aceptar' }))
    }
  }

  tableAction(event) {
    this.store.dispatch(new setSelectedLockInfo({ selectedLockInfo: event.row }))
    if (event.type == "history") {
      this.store.dispatch(new setLoadingCommandsTable({ loadingCommandsTable: true }))
      this.store.dispatch(new getTotalLockCommands())
      this.dialog.open(ModalLocksComponent, { width: '100vw', height: '80vh' })
    } else {
      this.store.dispatch(new setCloseModalLocks({ closeModalLocks: false }))
      this.dialog.open(ModalEditLockComponent, { width: '50vw', height: '80vh' })
    }
  }

  changePage(event) {
    this.store.dispatch(new setLoadingLockTable({ loadingLockTable: true }))
    this.store.dispatch(new setLockPageNumber({ lockPageNumber: event.first }))
    this.store.dispatch(new showPageLocks({ lockPageNumber: event.first }))
  }
  onFilterChange(event) {
    const value = (event.target as HTMLInputElement).value;
    this.onFilterChangeValue = value
    if (event['code'] == 'Enter') {
      this.searchLocks()
    }
  }

  locksWithoutBike() {
    this.lockWithoutBike = this.locksWithoutBikeState.value
    this.store.dispatch(new setLoadingLockTable({ loadingLockTable: true }))
    this.store.dispatch(new setLocksSearchParams({
      locksSearchParams: {
        searchTerm: this.locksSearchParams.isSearching ? this.onFilterChangeValue : "",
        isSearching: this.locksSearchParams.isSearching,
        locksWithoutBikeState: this.locksWithoutBikeState.value
      }
    }))

    if (this.locksSearchParams.isSearching) {
      this.store.dispatch(new searchBikesByNumber({ searchTerm: this.onFilterChangeValue }))
    } else {
      this.store.dispatch(new getTotalLocks())
    }
  }
  ngOnDestroy() {
    this.currentPageLocksSubscription.unsubscribe()
    this.totalLocksSubscription.unsubscribe()
    this.locksSearchParamsSubscription.unsubscribe()
    this.loadingLockTableSubscription.unsubscribe()
  }

}
