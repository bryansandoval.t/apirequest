import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { TableInfo, MapsTable } from '../../../../interfaces/table.interfaces';

@Component({
  selector: 'elastic-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class MapsComponent implements OnInit {
  onFilterChangeValue: string;
  title: string = "Mapa";
  latitude: number = 1.2;
  longitude: number = -77.27;
  latitude2: number = 1.22;
  longitude2: number = -77.29;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>) { }

  ngOnInit() {
  }

  onFilterChange(event) {
    const value = (event.target as HTMLInputElement).value;
    this.onFilterChangeValue = value
    if (event['code'] == 'Enter') {
      this.searchMaps()
    }
  }

  searchMaps() {
    if (this.onFilterChangeValue.length > 2) {
      console.log('this.onFilterChangeValue',this.onFilterChangeValue)
    } else {
      console.log('la busqueda debe ser mayor a 3 caracteres')
    }
  }

  showAll() {
    console.log('Mostrar todo')
  }
}
