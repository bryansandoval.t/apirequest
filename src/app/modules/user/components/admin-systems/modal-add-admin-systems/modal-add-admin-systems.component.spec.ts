import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddAdminSystemsComponent } from './modal-add-admin-systems.component';

describe('ModalAddAdminSystemsComponent', () => {
  let component: ModalAddAdminSystemsComponent;
  let fixture: ComponentFixture<ModalAddAdminSystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddAdminSystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddAdminSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
