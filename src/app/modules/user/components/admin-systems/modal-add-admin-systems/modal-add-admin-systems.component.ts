import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { createAdminUser, getOrganizations, setInfoFormAdmin } from 'src/app/config/global.actions';
import { passwordMatchValidator } from 'src/app/config/reducer/interface.reducer';

@Component({
  selector: 'elastic-modal-add-admin-systems',
  templateUrl: './modal-add-admin-systems.component.html',
  styleUrls: ['./modal-add-admin-systems.component.scss']
})
export class ModalAddAdminSystemsComponent implements OnInit {
  formGroup: any;
  btnOnSubmit: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<any>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<ModalAddAdminSystemsComponent>,
  ) { }

  selected: any;
  rols: Array<any> = [
    {name: "admin", id: "6142c9b6d97a767dbd8ad12f"},
    {name: "rebalanceador", id: "615f83b163996c6107cb0896"},
    {name: "superAdmin", id: "615f83e663996c6107cb0897"},
  ]

  tableOrganizations$: Observable<any> = this.store.select(state => state['user']['tableOrganizations']);
  tableOrganizationsSubscription: Subscription;
  tableOrganizations: any = [];

  closeAdminSystemsModal$: Observable<any> = this.store.select(state => state['user']['closeAdminSystemsModal']);
  closeAdminSystemsModalSubscription: Subscription;

  ngOnInit() {
    this.store.dispatch(new getOrganizations());
    this.tableOrganizationsSubscription = this.tableOrganizations$.subscribe((tableOrganizations) => {
      this.tableOrganizations = tableOrganizations;
    })
    this.closeAdminSystemsModalSubscription = this.closeAdminSystemsModal$.subscribe((closeModal) => {
      if(closeModal){this.closeModal()}
    })
    this.formGroup = this.formBuilder.group({
      name: new FormControl('', [Validators.required]),
      firstLastname: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      roles: new FormControl('', [Validators.required]),
      idNumber: new FormControl('', [Validators.required, Validators.minLength(6)]),
      organizationId: new FormControl('', [Validators.required]),
      accountState: new FormControl('', [Validators.required]),
      birthdate: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatPassword: new FormControl('', [Validators.required]),
    }, {validator: passwordMatchValidator});
  }

  get password() { return this.formGroup.get('password'); }
  get repeatPassword() { return this.formGroup.get('repeatPassword'); }
  onPasswordInput() {
    if (this.formGroup.hasError('passwordMismatch'))
      this.repeatPassword.setErrors([{'passwordMismatch': true}]);
    else
      this.repeatPassword.setErrors(null);
  }

  changeValidation(){

  }

  onSubmit(){
    this.btnOnSubmit = true;
    if(this.formGroup.status == "VALID"){
      this.store.dispatch(new setInfoFormAdmin({infoFormAdmin: this.formGroup.value}))
      this.store.dispatch(new createAdminUser())
    }
  }

  closeModal() {
    this.dialogRef.close();
  };

}
