import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSystemsComponent } from './admin-systems.component';

describe('AdminSystemsComponent', () => {
  let component: AdminSystemsComponent;
  let fixture: ComponentFixture<AdminSystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
