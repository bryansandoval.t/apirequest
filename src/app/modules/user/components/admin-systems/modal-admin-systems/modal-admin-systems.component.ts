import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { changePasswordAdminUser, getOrganizations, setInfoFormAdmin, setLoadingAdminSystemsPage, updateAdminUser } from 'src/app/config/global.actions';
import { passwordMatchValidator } from 'src/app/config/reducer/interface.reducer';

@Component({
  selector: 'elastic-modal-admin-systems',
  templateUrl: './modal-admin-systems.component.html',
  styleUrls: ['./modal-admin-systems.component.scss']
})
export class ModalAdminSystemsComponent implements OnInit {
  formGroup: any;
  roles: any = [];
  btnOnSubmit: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<any>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<ModalAdminSystemsComponent>,
  ) { }

  selected: any;
  rols: Array<any> = [
    { name: "admin", id: "6142c9b6d97a767dbd8ad12f" },
    { name: "rebalanceador", id: "615f83b163996c6107cb0896" },
    { name: "superAdmin", id: "615f83e663996c6107cb0897" },
  ]

  modalAdminSystems$: Observable<any> = this.store.select(state => state.user.modalAdminSystems);
  modalAdminSystemsSubscription: Subscription;
  modalAdminSystems: any;

  infoFormAdmin$: Observable<any> = this.store.select(state => state.user.infoFormAdmin);
  infoFormAdminSubscription: Subscription;
  infoFormAdmin: any;

  closeAdminSystemsModal$: Observable<any> = this.store.select(state => state.user.closeAdminSystemsModal);
  closeAdminSystemsModalSubscription: Subscription;

  ngOnInit() {
    this.modalAdminSystemsSubscription = this.modalAdminSystems$.subscribe((modalAdminSystems) => {
      this.modalAdminSystems = modalAdminSystems;
      if (modalAdminSystems == "edit") {
        this.formGroup = this.formBuilder.group({
          name: new FormControl({ value: '', disabled: true }, [Validators.required]),
          firstLastname: new FormControl({ value: '', disabled: true }, [Validators.required]),
          email: new FormControl({ value: '', disabled: true }, [Validators.required]),
          roles: new FormControl('', [Validators.required]),
          idNumber: new FormControl({ value: '', disabled: true }, [Validators.required, Validators.minLength(6)]),
          accountState: new FormControl('', [Validators.required]),
        });
      } else {
        this.formGroup = this.formBuilder.group({
          name: new FormControl({ value: '', disabled: true }, [Validators.required]),
          firstLastname: new FormControl({ value: '', disabled: true }, [Validators.required]),
          email: new FormControl({ value: '', disabled: true }, [Validators.required]),
          idNumber: new FormControl({ value: '', disabled: true }, [Validators.required, Validators.minLength(6)]),
          password: new FormControl('', [Validators.required, Validators.minLength(6)]),
          repeatPassword: new FormControl('', [Validators.required]),
        }, { validator: passwordMatchValidator });
      }
    });
    this.infoFormAdminSubscription = this.infoFormAdmin$.subscribe((infoFormAdmin) => {
      this.infoFormAdmin = infoFormAdmin;
      if (this.modalAdminSystems == "edit") {
        infoFormAdmin.roles.includes('6142c9b6d97a767dbd8ad12f')?this.roles.push("6142c9b6d97a767dbd8ad12f"):null;
        infoFormAdmin.roles.includes('615f83b163996c6107cb0896')?this.roles.push("615f83b163996c6107cb0896"):null;
        infoFormAdmin.roles.includes('615f83e663996c6107cb0897')?this.roles.push("615f83e663996c6107cb0897"):null;
        this.formGroup.setValue({
          name: infoFormAdmin.name,
          firstLastname: infoFormAdmin.firstLastname,
          email: infoFormAdmin.email,
          roles: this.roles[0],
          idNumber: infoFormAdmin.idNumber,
          accountState: infoFormAdmin.accountState,
        });
      } else {
        this.formGroup.setValue({
          name: infoFormAdmin.name?infoFormAdmin.name:"",
          firstLastname: infoFormAdmin.firstLastname?infoFormAdmin.firstLastname:"",
          email: infoFormAdmin.email?infoFormAdmin.email:"",
          idNumber: infoFormAdmin.idNumber?infoFormAdmin.idNumber:0,
          password: "",
          repeatPassword: ""
        });
      }
    });
    this.closeAdminSystemsModalSubscription = this.closeAdminSystemsModal$.subscribe((closeModal) => {
      if(closeModal){this.closeModal()}
    });
  }
  get password() { return this.formGroup.get('password'); }
  get repeatPassword() { return this.formGroup.get('repeatPassword'); }
  onPasswordInput() {
    if (this.formGroup.hasError('passwordMismatch'))
      this.repeatPassword.setErrors([{ 'passwordMismatch': true }]);
    else
      this.repeatPassword.setErrors(null);
  }

  changeValidation() {

  }

  onSubmit() {
    this.btnOnSubmit = true;
    if(this.formGroup.status == "VALID"){
      if (this.modalAdminSystems == "edit") {
        this.store.dispatch(new setInfoFormAdmin({infoFormAdmin: {...this.infoFormAdmin, ...this.formGroup.value}}))
        this.store.dispatch(new setLoadingAdminSystemsPage({loadingAdminSystemsPage: true}))
        this.store.dispatch(new updateAdminUser())
      }else{
        this.store.dispatch(new setInfoFormAdmin({infoFormAdmin: {...this.infoFormAdmin, ...this.formGroup.value}}))
        this.store.dispatch(new setLoadingAdminSystemsPage({loadingAdminSystemsPage: true}))
        this.store.dispatch(new changePasswordAdminUser())
      }
    }
  }

  closeModal() {
    this.dialogRef.close();
  };

}
