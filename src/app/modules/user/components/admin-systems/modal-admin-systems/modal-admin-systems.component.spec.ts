import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAdminSystemsComponent } from './modal-admin-systems.component';

describe('ModalAdminSystemsComponent', () => {
  let component: ModalAdminSystemsComponent;
  let fixture: ComponentFixture<ModalAdminSystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAdminSystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAdminSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
