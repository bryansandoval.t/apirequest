import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { TableInfo, AdminSystemsTable, Column, adminColumns } from '../../../../interfaces/table.interfaces';
import { tableTypes } from 'src/app/constants/table.constants';
import { ModalAdminSystemsComponent } from './modal-admin-systems/modal-admin-systems.component';
import { getListOrganizations, getListRoles, getTotalUsersAdmins, setAdminSystemsFormStatus, setCloseAdminSystemsModal, setInfoFormAdmin, setLoadingAdminSystemsPage, setModalAdminSystems, showGlobalPageAdmins, showPageAdmin, showSearchPageAdmins } from 'src/app/config/global.actions';
import { Observable, Subscription } from 'rxjs';
import { ModalAddAdminSystemsComponent } from './modal-add-admin-systems/modal-add-admin-systems.component';

@Component({
  selector: 'elastic-admin-systems',
  templateUrl: './admin-systems.component.html',
  styleUrls: ['./admin-systems.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class AdminSystemsComponent implements OnInit {
  [x: string]: any;


  adminColumns: Column[] = adminColumns;
  adminSystemsPageSize: number = 20;
  //TEMP volar
  allAdmin: any = [];
  timeout: any;
  title: string = "Administradores del sistema";


  constructor(
    private ref: ChangeDetectorRef,
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>) { }



  totalUsersAdmins$: Observable<any> = this.store.select(state => state.user.totalUsersAdmins);
  totalUsersAdminsSubscription: Subscription;
  totalUsersAdmins: any;

  loadingAdminSystemsPage$: Observable<any> = this.store.select(state => state.user.loadingAdminSystemsPage);
  loadingAdminSystemsPageSubscription: Subscription;
  loadingAdminSystemsPage: any;

  adminSystemsFormStatus$: Observable<any> = this.store.select(state => state.user.adminSystemsFormStatus);
  adminSystemsFormStatusSubscription: Subscription;
  adminSystemsFormStatus: any;

  currentPageAdmins$: Observable<any> = this.store.select(state => state.user.currentPageAdmins);
  currentPageAdminsSubscription: Subscription;
  currentPageAdmins: any[];

  ngOnInit() {
    this.store.dispatch(new setLoadingAdminSystemsPage({ loadingAdminSystemsPage: true }))
    this.store.dispatch(new getListOrganizations())
    this.store.dispatch(new getListRoles())
    this.store.dispatch(new getTotalUsersAdmins())


    this.totalUsersAdminsSubscription = this.totalUsersAdmins$.subscribe((totalUsersAdmins) => {
      this.totalUsersAdmins = totalUsersAdmins.count? totalUsersAdmins.count: 0;
    });
    this.adminSystemsFormStatusSubscription = this.adminSystemsFormStatus$.subscribe((searchStatus) => {
      this.adminSystemsFormStatus = searchStatus;
    });
    this.loadingAdminSystemsPageSubscription = this.loadingAdminSystemsPage$.subscribe((loading) => {
      this.loadingAdminSystemsPage = loading;
    });
    this.currentPageAdminsSubscription = this.currentPageAdmins$.subscribe((currentPageAdmins) => {
      this.currentPageAdmins = [...currentPageAdmins].splice(0, 20);
      this.allAdmin = this.allAdmin.length == '0' ? currentPageAdmins : this.allAdmin;
      if (this.currentPageAdmins && this.adminSystemsFormStatus) {
        this.totalUsersAdmins = currentPageAdmins.length
      }
      setTimeout(() => {
        this.ref.detectChanges();
      }, 100);
    });
  }

  tableAction(event) {
    if (event.type === 'edit' || event.type === 'changePassword') {
      this.store.dispatch(new setModalAdminSystems({modalAdminSystems: event.type}));
      this.store.dispatch(new setInfoFormAdmin({infoFormAdmin: event.row}));
      this.store.dispatch(new setCloseAdminSystemsModal({closeAdminSystemsModal: false}));
      this.dialog.open(ModalAdminSystemsComponent, { width: '50vw', height: '80vh' });
    };
  };

  onFilterChange(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (filterValue.length > 2) {
        this.store.dispatch(new setLoadingAdminSystemsPage({ loadingAdminSystemsPage: true }))
        this.store.dispatch(new setAdminSystemsFormStatus({ adminSystemsFormStatus: true }))
        this.store.dispatch(new showSearchPageAdmins({ searchTerm: filterValue }))
      } else {
        this.store.dispatch(new getTotalUsersAdmins())
        this.store.dispatch(new setLoadingAdminSystemsPage({ loadingAdminSystemsPage: false }))
        this.store.dispatch(new setAdminSystemsFormStatus({ adminSystemsFormStatus: false }))
      }
      clearTimeout(this.timeout);
    }, 1000);
  }

  openModal() {
    this.store.dispatch(new setCloseAdminSystemsModal({closeAdminSystemsModal: false}));
    this.dialog.open(ModalAddAdminSystemsComponent, { width: '50vw', height: '80vh' })
  }

  changePage(event) {
    this.currentPageAdmins = this.allAdmin.slice(event.first, (event.first + event.rows));
    if (!this.adminSystemsFormStatus && event.first != 0) {
      this.store.dispatch(new showPageAdmin({ adminPageNumber: event.first }));
      this.store.dispatch(new showGlobalPageAdmins());
    }
  }

  ngOnDestroy() {
    this.totalUsersAdminsSubscription.unsubscribe();
    this.currentPageAdminsSubscription.unsubscribe();
    this.loadingAdminSystemsPageSubscription.unsubscribe();
    this.adminSystemsFormStatusSubscription.unsubscribe();
  }

}
