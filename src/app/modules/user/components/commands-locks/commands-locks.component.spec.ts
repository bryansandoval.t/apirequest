import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandsLocksComponent } from './commands-locks.component';

describe('CommandsLocksComponent', () => {
  let component: CommandsLocksComponent;
  let fixture: ComponentFixture<CommandsLocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandsLocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandsLocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
