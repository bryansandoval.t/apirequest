import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../../app.animation';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
//Informacion sobre la tabla
import { TableInfo, ComandLocksTable } from '../../../../interfaces/table.interfaces';

@Component({
  selector: 'elastic-commands-locks',
  templateUrl: './commands-locks.component.html',
  styleUrls: ['./commands-locks.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class CommandsLocksComponent implements OnInit {

  @Input() tableData: TableInfo = ComandLocksTable;
  storeInfo = { store: 'user', storeState: 'user-infos' };
  storeInfoSearch = { store: 'user', storeState: 'user-infosSearch' };

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<any>) { }

  ngOnInit() {
  }

  create() {
    console.log("Crear item");
  }

  delete(row) {
    console.log("Delete item",row);
  }

}
