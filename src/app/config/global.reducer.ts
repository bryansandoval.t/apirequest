import { createSelector } from 'reselect';
import { MetaReducer, ActionReducerMap, ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import * as fromRouter from '@ngrx/router-store';
import * as fromLayout from '../modules/core/layout/shared/layout.reducer';
import * as fromSidenav from '../modules/core/sidenav/shared/sidenav.reducer';
import * as fromUser from './reducer/user.reducer';
import * as fromAuth from './reducer/auth.reducer';
import * as fromInterface from './reducer/interface.reducer';
import { ActionTypes, actions } from './global.actions';
import { RouterStateUrl } from './routes';
import * as _ from 'lodash';
import { User } from '../models/user.model';

export interface State {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
  auth: fromAuth.State;
  apiData: ApiDataState;
  global: GlobalState;
  layout: fromLayout.State;
  sidenav: fromSidenav.State;
  user: fromUser.State;
  interface: fromInterface.State;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
  auth: fromAuth.authReducer,
  apiData: apiDataReducer,
  global: globalReducer,
  layout: fromLayout.reducer,
  sidenav: fromSidenav.reducer,
  user: fromUser.userReducer,
  interface: fromInterface.interfaceReducer,
};

export interface ApiDataState {
  user: User;
  token: string;
  typeAccess: string;
  tableStations: Array<any>;
  stationFormErrors: Object;
  searchStation: string;
  modalStationInfo: Object;
  stationModalType: Object;
  organizationLogo: Object;
  tripsFormStatus: boolean;
  loadingTripsPage: boolean; //Trips_v_L
  closeTripsModal: boolean; //Trips_v_M

  loadingTicketsTable: boolean; //Support_v_A
  totalTickets: number; //Support_v_B
  currentPageTickets: Array<any>; //Support_v_C
  selectedTicketInfo: Object; //Support_v_D
  ticketPageNumber: number; //Support_v_E
  searchWordTickets: string; //Support_v_F
  masterListTicket: Object; //Support_v_G
  ticketsSearchResult: Array<any>; //Support_r_tsr
  ticketPageSIze: Object; //Support_v_I
  ticketSearchStatus: boolean; //Support_v_E
  closeModalTickets: boolean; //Support_v_E

  mapStationsSearchParams: Object; //Map_v_A
}

export const inicialStateApiData: ApiDataState = {
  user: null,
  token: null,
  typeAccess: null,
  tableStations: [],
  stationFormErrors: {},
  searchStation: '',
  modalStationInfo: {},
  stationModalType: {},
  organizationLogo: {
    shouldLoad: [],
    logoInfo: false
  },
  tripsFormStatus: false,
  loadingTripsPage: false, //Trips_v_L
  closeTripsModal: false, //Trips_v_M
  
  loadingTicketsTable: false, //Support_v_A
  totalTickets: 0, //Support_v_B
  currentPageTickets: [], //Support_v_C
  selectedTicketInfo: {}, //Support_v_D
  ticketPageNumber: 0, //Support_v_E
  searchWordTickets: "", //Support_v_F
  masterListTicket: [], //Support_v_G
  ticketsSearchResult: [], //Support_r_tsr
  ticketSearchStatus: false, //Support_v_E
  ticketPageSIze: {
    limit: 0,
    pageNumber: 0
  }, //Support_v_I
  closeModalTickets: false, //Support_v_

  mapStationsSearchParams: {
    isSearching: false,
    searchTerm: ""
  } //Map_v_A
};

export function apiDataReducer(state: ApiDataState = inicialStateApiData, action: actions): ApiDataState {
  switch (action.type) {
    case ActionTypes.saveApiData:
      return {
        ...state,
        [action.payload.dataName]: action.payload.data
      };
    case ActionTypes.setMapStationsSearchParams: //Map_r_B
      return {
        ...state,
        mapStationsSearchParams: action.payload.mapStationsSearchParams
      };
    case ActionTypes.saveStationsInfo:
      return {
        ...state,
        tableStations: action.payload.tableStations
      };
    case ActionTypes.setCloseTripsModal:
      return {
        ...state,
        closeTripsModal: action.payload.closeTripsModal
      };
    case ActionTypes.setTripsFormStatus:
      return {
        ...state,
        tripsFormStatus: action.payload.tripsFormStatus
      };
    case ActionTypes.setStationFormErrors:
      return {
        ...state,
        stationFormErrors: action.payload.stationFormErrors
      };
    case ActionTypes.setOrganizationLogo:
      return {
        ...state,
        organizationLogo: action.payload.organizationLogo
      };
    case ActionTypes.saveStationSearch:
      return {
        ...state,
        searchStation: action.payload.searchStation
      };
    case ActionTypes.setMosalStationInfo:
      return {
        ...state,
        modalStationInfo: action.payload.modalStationInfo,
        stationModalType: action.payload.stationModalType,
      };
    case ActionTypes.setLoadingTicketsTable: //support_r_A
      return {
        ...state,
        loadingTicketsTable: action.payload.loadingTicketsTable,
      };
    case ActionTypes.setTicketPageSize: //support_r_M
      return {
        ...state,
        ticketPageSIze: action.payload.ticketPageSIze,
      };
    case ActionTypes.setTicketsSearchResult: //support_r_tsr
      return {
        ...state,
        ticketsSearchResult: action.payload.ticketsSearchResult,
      };
    case ActionTypes.setTotalTickets: //support_r_B
      return {
        ...state,
        totalTickets: action.payload.totalTickets,
      };
    case ActionTypes.setCloseModalTickets: //support_r_
      return {
        ...state,
        closeModalTickets: action.payload.closeModalTickets,
      };
    case ActionTypes.setTicketSearchStatus: //support_r_K
      return {
        ...state,
        ticketSearchStatus: action.payload.ticketSearchStatus,
      };
    case ActionTypes.setSelectedTicketInfo: //support_r_D
      return {
        ...state,
        selectedTicketInfo: action.payload.selectedTicketInfo,
      };
    case ActionTypes.setTicketPageNumber: //support_r_E
      return {
        ...state,
        ticketPageNumber: action.payload.ticketPageNumber,
      };
    case ActionTypes.setSearchWordTickets: //support_r_swt
      return {
        ...state,
        searchWordTickets: action.payload.searchWordTickets,
      };
    case ActionTypes.setMasterListTicket: //support_r_J
      return {
        ...state,
        masterListTicket: action.payload.masterListTicket,
      };
    case ActionTypes.setCurrentPageTickets: //support_r_C
      return {
        ...state,
        currentPageTickets: action.payload.currentPageTickets,
      };
    case ActionTypes.setLoadingTripsPage:
      return {
        ...state,
        loadingTripsPage: action.payload.loadingTripsPage
      };
    default:
      return state;
  }
}

export interface GlobalState {
  test: any;
}

export const inicialStateGlobal: GlobalState = {
  test: []
};

export function globalReducer(state: GlobalState = inicialStateGlobal, action: actions): GlobalState {
  return state;
}

export function globalMetaReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state: State, action: any) {
    const newState = _.cloneDeep(state);
    switch (action.type) {
      case ActionTypes.setPasswordError:
        _.set(newState, action.payload.whereErrorsSave, action.payload.startupErrors);
        return {
          ...state,
          ...newState
        }
      case ActionTypes.clearValidationError:
        const currentErrors = _.get(newState, action.payload.whereErrorsSave);
        action.payload.errorsToClear.forEach((key) => {
          delete currentErrors[key]
        })
        _.set(newState, action.payload.whereErrorsSave, currentErrors);
        return {
          ...state,
          ...newState
        };
      case ActionTypes.saveData:
        let Data;
        let flattenObject = (obj, prefix = '') =>
          Object.keys(obj).reduce((acc, k) => {
            const pre = prefix.length ? prefix + '.' : '';
            if (typeof obj[k] === 'object') {
              if (k === 'adminId') {
                if (obj[k].adminsIds[0] !== null) {
                  Object.assign(acc, flattenObject(obj[k].adminsIds, pre + k));
                } else {
                  acc[pre + k] = '';
                }
              } else {
                if (obj[k] != null) {
                  Object.assign(acc, flattenObject(obj[k], pre + k));
                }
              }
            } else { acc[pre + k] = obj[k]; }
            return acc;
          }, {});
        if (Array.isArray(action.payload.data)) {
          Data = action.payload.data.map(data => { return { ...flattenObject(data), ...data } });
        } else {
          Data = action.payload.data;
        }
        _.set(newState, action.payload.dataName, Data);
        return {
          ...state,
          ...newState
        };
      default:
        return reducer(state, action);
    }

  };
}


export const getLayoutState = (state: State) => state.layout;

export const getSidenavOpen = createSelector(getLayoutState, fromLayout.getSidenavOpen);
export const getSidenavCollapsed = createSelector(getLayoutState, fromLayout.getSidenavCollapsed);
export const getSidenavAlign = createSelector(getLayoutState, fromLayout.getSidenavAlign);
export const getSidenavMode = createSelector(getLayoutState, fromLayout.getSidenavMode);
export const getSidenavDisableClose = createSelector(getLayoutState, fromLayout.getSidenavDisableClose);
export const getQuickpanelOpen = createSelector(getLayoutState, fromLayout.getQuickpanelOpen);
export const getLayout = createSelector(getLayoutState, fromLayout.getLayout);
export const getLayoutBoxed = createSelector(getLayoutState, fromLayout.getLayoutBoxed);
export const getSettingsOpen = createSelector(getLayoutState, fromLayout.getSettingsOpen);
export const getCardElevation = createSelector(getLayoutState, fromLayout.getCardElevation);
export const getSidenavState = (state: State) => state.sidenav;
export const getSidenavItems = createSelector(getSidenavState, fromSidenav.getSidenavItems);
export const getSidenavCurrentlyOpen = createSelector(getSidenavState, fromSidenav.getSidenavCurrentlyOpen);


export const metaReducers: MetaReducer<State>[] = !environment.production ? [storeFreeze, globalMetaReducer] : [globalMetaReducer];
