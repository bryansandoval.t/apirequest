import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { ApiService } from '../../services/api.service';
import {
  AddUser,
  AddUserInfo,
  RefreshUserTable,
  ActionTypes,
  serverError,
  ModifyUser,
  ModifyUserInfo,
  DeleteUser,
  ActiveUser,
  DeleteUserInfo,
  totalGetUsers,
  showPage,
  userPagination,
  userPaginationTrip,
  saveUsers,
  paginatorFinalUser,
  showPageSearch,
  setSearchTerm,
  paginatorFinalUserSearch,
  datasourceSearch,
  totalGetTrip,
  showPageTrip,
  saveUserTrips,
  showPageSearchTrip,
  setSearchTermTrip,
  datasourceSearchTrip,
  paginatorFinalUserSearchTrip,
  stationsTrips,
  getStationsTrip,
  patchUser,
  modalUser,
  calculationDates,
  getPenaltyTypes,
  penaltyTypes,
  penaltyDates,
  postPenaltys,
  getTotalBikes,
  totalBikes,
  setCurrentPageBike,
  getBikes,
  setBikes,
  showPageBikes,
  showSearchPageBike,
  getLocksSearch,
  filterSearchLocksBikes,
  bikesSearchResult,
  getMasterListTypesBikes,
  saveTypesBikes,
  getStationsBikes,
  getMasterListStateBikes,
  saveStationsBikes,
  saveStateBikes,
  postBike,
  patchLockBike,
  decisionPatchLockBike,
  patchDeleteLockBike,
  patchBike,
  modalBike,
  getCountLocksBikes,
  totalLockBikes,
  getLocksBikes,
  saveLocksBikes,
  showPageLocksBikes,
  showSearchPageLocksBike,
  getModalLocksSearch,
  locksBikesSearchResult,
  searchBikeForm,

  getTotalLocks,
  setTotalLocks,
  setLockPageNumber,
  showGlobalPageLocks,
  setCurrentPageLock,
  setLoadingLockTable,
  getTotalLockCommands,
  setTotalLockCommands,
  setLockCommandsPageNumber,
  showPageLockCommands,
  setCurrentPageLockCommands,
  setLoadingCommandsTable,
  showPageLocks,
  showSearchPageLocks,
  searchBikesByNumber,
  searchLocks,
  setLocksSearchResult,
  setEditLockFormErrors,
  validateUniqQrMac,
  validateEditLockForm,
  patchLock,
  setSelectedLockInfo,
  setCloseModalLocks,
  validateLockOrganization,
} from '../global.actions';
//import { State } from '../reducer/user.reducer';
import { State } from '../global.reducer';
import { exhaustMap, catchError, tap, switchMap, map, concatMap, withLatestFrom } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { SplashScreenService } from 'src/app/services/splash-screen.service';
import { ApiError, saveApiData, getTableDataLength, UpdateData, showSweetAlert, paginatorFinalUserTrip } from 'src/app/config/global.actions';
import { refreshTokenInit } from '../global.actions';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TestInfoTable } from 'src/app/interfaces/table.interfaces';
import { UserInfos } from '../../models/userInfos.model';
import { config } from 'process';
import { dispatch } from 'rxjs/internal/observable/pairs';
import * as _ from 'lodash';

@Injectable()

export class UserEffects {

  @Effect()
  showPageLocks$ = this.actions$.pipe(
    ofType<showPageLocks>(ActionTypes.showPageLocks),
    withLatestFrom((this.store$.select((state: State) => state.user.locksSearchParams))),
    map((action) => {
      if (action[1]['isSearching'] === true) {
        return new showSearchPageLocks({ lockPageNumber: action[0].payload.lockPageNumber });
      }
      else {
        return new showGlobalPageLocks({ lockPageNumber: action[0].payload.lockPageNumber });
      }

    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  @Effect() //Efecto logica
  validateEditLockForm$: Observable<Action> = this.actions$.pipe(
    ofType<validateEditLockForm>(ActionTypes.validateEditLockForm),
    map((action) => {
      this.spinner.show();
      let editLockFormErrors: any = {
        qr: '',
        mac: '',
        simNumber: ''
      };
      let hasErrors = false;
      if (action.payload.lockPatch.qrNumber.length < 5) {
        editLockFormErrors.qr = "El QR debe tener minimo 5 caracteres";
        hasErrors = true;
      }
      if (action.payload.lockPatch.mac == '') {
        editLockFormErrors.mac = "El Mac del candado no puede ser vacio";
        hasErrors = true;
      }
      if (action.payload.lockPatch.simNumber.length < 10) {
        editLockFormErrors.simNumber = "El numero de la sim debe tener minimo 10 digitos";
        hasErrors = true;
      }

      if (hasErrors) {
        return new setEditLockFormErrors({ editLockFormErrors });
      } else {
        return new validateUniqQrMac({ lockPatch: action.payload.lockPatch });
      }
    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  @Effect() //Efecto logica
  showSearchPageLocks$: Observable<Action> = this.actions$.pipe(
    ofType<showSearchPageLocks>(ActionTypes.showSearchPageLocks),
    withLatestFrom(
      (this.store$.select((state: State) => state.user.locksSearchResult)),
      (this.store$.select((state: State) => state.user.locksPageSize)),
    ),
    exhaustMap((action) => {
      let skip = action[0].payload.lockPageNumber;
      let dataSearch = action[1].slice(skip, (skip + action[2]))
      return [new setCurrentPageLock({ currentPageLocks: dataSearch }),
      new setLoadingLockTable({ loadingLockTable: false })
      ];
    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  @Effect()
  searchBikesByNumber$ = this.actions$.pipe(
    ofType<searchBikesByNumber>(ActionTypes.searchBikesByNumber),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filterBikes: any = {
        "where": {
          "and": [{ "number": { "like": action[0].payload.searchTerm, "options": "i" } }],
        },
        limit: 100
      };
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filterBikes.where.and.push({ organizationId: action[1]['organizationId'] })
      }
      return this.apiService.get('bikes', filterBikes).pipe(
        map((response) => {
          let bikesIds = [];
          response.forEach((bike: any) => {
            bikesIds.push(bike.id);
          });
          let dispatch = [
            new searchLocks({ bikesIds, searchTerm: action[0].payload.searchTerm })
          ];
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  searchLocks$ = this.actions$.pipe(
    ofType<searchLocks>(ActionTypes.searchLocks),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user),
      this.store$.select((state: State) => state.user.locksSearchParams)),
    exhaustMap((action) => {
      let filterLocks: any = {
        "where": {
          and: [
            {
              or: [
                { "qrNumber": { "like": action[0].payload.searchTerm, "options": "i" } },
                { "imei": { "like": action[0].payload.searchTerm, "options": "i" } },
                { "bikeId": { "inq": action[0]['payload']['bikesIds'] } },
              ]
            }
          ]
        },
        limit: 100,
        include: [{ relation: 'bike' }]
      };
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filterLocks.where.and.push({ organizationId: action[1]['organizationId'] })
      }
      if (action[2]['locksWithoutBikeState']) {
        filterLocks.where.and.push({ or: [{ bikeId: { inq: [null, undefined] } }, { bikeId: "" }] })
      }
      return this.apiService.get('locks', filterLocks).pipe(
        map((response) => {
          let dispatch = [
            new setTotalLocks({ totalLocks: response.length }),
            new setLockPageNumber({ lockPageNumber: 0 }),//_revisar Que es pagina 1 para Bryan
            new setLocksSearchResult({ locksSearchResult: response }),
            new showSearchPageLocks({ lockPageNumber: 0 }),
          ];
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  validateLockOrganization$ = this.actions$.pipe(
    ofType<validateLockOrganization>(ActionTypes.validateLockOrganization),
    withLatestFrom(this.store$.select((state: State) => state.user.selectedLockInfo)),
    exhaustMap((action) => {
      let dispatch: any = [];
      if (!action[1]['bike']) { //No tiene bici asociada
        dispatch.push(new patchLock({ lockPatch: action[0].payload.lockPatch }))
      } else if (!action[1]['organization'] || action[1]['organizationId'] == action[0].payload.lockPatch.organizationId) {
        dispatch.push(new patchLock({ lockPatch: action[0].payload.lockPatch }))
      } else {
        dispatch.push(new showSweetAlert({ title: 'Error', text: 'No es posible cambiar de organización a un candado asociado a una bici', type: 'error', buttonText: 'Accept' }))
      }
      return dispatch
    })
  );

  @Effect()
  validateUniqQrMac$ = this.actions$.pipe(
    ofType<validateUniqQrMac>(ActionTypes.validateUniqQrMac),
    exhaustMap((action) => {
      this.spinner.show();
      let filterLocks = {
        "where": {
          "and": [
            { "id": { "neq": action['payload']['id'] } },
            {
              "or": [
                { "qrNumber": action['payload']['qrNumber'] },
                { "mac": action['payload']['mac'] },
              ]
            },
          ]
        },
        limit: 10
      };
      return this.apiService.get('locks', filterLocks).pipe(
        map((response) => {
          let dispatch = [];
          dispatch.push(new setCloseModalLocks({ closeModalLocks: true }));
          if (response.length > 0) {
            let mensaje = 'Ya existe un candado con este QR, por favor revisa los datos';
            if (response[0].mac == action['payload']['mac']) {
              mensaje = 'Ya existe un candado con este Mac, por favor revisa los datos';
            }
            this.spinner.hide();
            dispatch.push(new showSweetAlert({ title: 'Error', text: mensaje, type: 'error', buttonText: 'Accept' }));
          } else {
            dispatch.push(new validateLockOrganization({ lockPatch: action.payload.lockPatch }));
            // dispatch.push(new patchLock({ lockPatch: action.payload.lockPatch }));
          }
          this.spinner.hide();
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  patchLock$ = this.actions$.pipe(
    ofType<patchLock>(ActionTypes.patchLock),
    withLatestFrom(this.store$.select((state: State) => state.user.selectedLockInfo),
      this.store$.select((state: State) => state.user.locksSearchResult),
      this.store$.select((state: State) => state.user.locksSearchParams),
      this.store$.select((state: State) => state.user.listOrganizations),
      this.store$.select((state: State) => state.user.lockPageNumber),
    ),
    exhaustMap((action) => {
      let lockUpdate: any = _.cloneDeep(action[0].payload.lockPatch);
      return this.apiService.patch('locks', lockUpdate, { setDeleteTime: false }).pipe(
        exhaustMap(() => {
          let locksSearchResult: any = _.cloneDeep(action[2]);
          if (action[3]['isSearching']) {
            locksSearchResult.forEach(lockSearch => {
              if (lockSearch.id == lockUpdate.id) {
                lockSearch.mac = lockUpdate.mac
                lockSearch.qrNumber = lockUpdate.qrNumber
                lockSearch.simNumber = lockUpdate.simNumber
                lockSearch.organizationId = lockUpdate.organizationId
                lockSearch.organization = action[4].find(org => {
                  return org.id == lockUpdate.organizationId;
                });
              }
            });
            return [
              new setLoadingLockTable({ loadingLockTable: false }),
              new setLocksSearchResult({ locksSearchResult: locksSearchResult }),
              new showSearchPageLocks({ lockPageNumber: action[5] })
            ]
          } else {
            return [
              new setLoadingLockTable({ loadingLockTable: false }),
              new setLockPageNumber({ lockPageNumber: action[5] }),
              new showPageLocks({ lockPageNumber: action[5] })
            ]
          }
        })
      )
    })
  );
  @Effect()
  showPageLockCommands$ = this.actions$.pipe(
    ofType<showPageLockCommands>(ActionTypes.showPageLockCommands),
    withLatestFrom(this.store$.select((state: State) => state.user.selectedLockInfo), this.store$.select((state: State) => state.user.lockCommandsPageNumber)),
    exhaustMap((action) => {
      let filter = {
        where: { lockId: action[1]['id'] },
        limit: 20, skip: action[2],
        include: [{ relation: 'lock' }],
      };
      return this.apiService.get("lock-commands", filter).pipe(
        exhaustMap((lockCommands) => {
          return [
            new setCurrentPageLockCommands({ currentPageLockCommands: lockCommands }),
            new setLoadingCommandsTable({ loadingCommandsTable: false }),
          ]
        })
      )
    })
  );
  @Effect()
  getTotalLockCommands$ = this.actions$.pipe(
    ofType<getTotalLockCommands>(ActionTypes.getTotalLockCommands),
    withLatestFrom(this.store$.select((state: State) => state.user.selectedLockInfo), this.store$.select((state: State) => state.user.lockCommandsPageNumber)),
    exhaustMap((action) => {
      let filter = {
        lockId: action[1]['id']
      };
      return this.apiService.get("lock-commands", filter, { count: true }).pipe(
        exhaustMap((lockCommands) => {
          return [
            new setTotalLockCommands({ totalLockCommands: lockCommands.count }),
            new setLockCommandsPageNumber({ lockCommandsPageNumber: action[2] }),
            new showPageLockCommands({ lockCommandsPageNumber: action[2] }),
          ]
        })
      )
    })
  );
  @Effect()
  getTotalLocks$ = this.actions$.pipe(
    ofType<getTotalLocks>(ActionTypes.getTotalLocks),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user),
      this.store$.select((state: State) => state.user.lockPageNumber),
      this.store$.select((state: State) => state.user.locksSearchParams)),
    exhaustMap((action) => {
      let filter: any = { and: [] };
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter.and.push({ or: [{ organizationId: action[1]['organizationId'] }, { organizationId: "" }, { organizationId: { inq: [null, undefined] } }] })
      }
      if (action[3]['locksWithoutBikeState']) {
        filter.and.push({ or: [{ bikeId: { inq: [null, undefined] } }, { bikeId: "" }] })
      }
      if (filter.and.length == 0) {
        filter = {}
      }
      return this.apiService.get("locks", filter, { count: true }).pipe(
        exhaustMap((locksCount) => {
          return [
            new setTotalLocks({ totalLocks: locksCount.count }),
            new setLockPageNumber({ lockPageNumber: action[2] }),
            new showGlobalPageLocks({ lockPageNumber: action[2] })
          ]
        })
      )
    })
  );

  @Effect()
  showGlobalPageLocks$ = this.actions$.pipe(
    ofType<showGlobalPageLocks>(ActionTypes.showGlobalPageLocks),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user),
      this.store$.select((state: State) => state.user.lockPageNumber),
      this.store$.select((state: State) => state.user.locksSearchParams)),
    exhaustMap((action) => {
      let filter: any = {
        limit: 20, skip: action[2],
        include: [{ relation: 'bike' }, { relation: 'organization' }],
        where: { and: [] }
      };
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter.where.and.push({ or: [{ organizationId: action[1]['organizationId'] }, { organizationId: "" }, { organizationId: { inq: [null, undefined] } }] })
      }
      if (action[3]['locksWithoutBikeState']) {
        filter.where.and.push({ or: [{ bikeId: { inq: [null, undefined] } }, { bikeId: "" }] })
      }
      if (filter.where.and.length == 0) {
        delete filter.where
      }

      return this.apiService.get("locks", filter).pipe(
        exhaustMap((locks) => {
          return [
            new setCurrentPageLock({ currentPageLocks: locks }),
            new setLoadingLockTable({ loadingLockTable: false })
          ]
        })
      )
    })
  );

  @Effect()
  addUser$: Observable<Action> = this.actions$.pipe(
    ofType<AddUser>(ActionTypes.addUser),
    exhaustMap((action) => {
      const userForm = { ...action.payload.user };
      return this.apiService.post('users', userForm).pipe(
        switchMap((response) => of(
          new saveApiData({ data: response, dataName: 'users' }),
          new AddUserInfo({ data: response, userInfos: action.payload.userInfos, name: 'user-infos' }),
          new refreshTokenInit()
        )),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          if (error.status == 404) { return of(new ApiError({ error }), new showSweetAlert({ title: 'error', text: 'This username or email is already in use', type: 'error', buttonText: 'Accept' })) }
          else { return of(new ApiError({ error })) };
        })
      );
    })
  );

  @Effect()
  addUserInfo$: Observable<Action> = this.actions$.pipe(
    ofType<AddUserInfo>(ActionTypes.addUserInfo),
    exhaustMap((action) => {
      const userInfoForm = { ...action.payload.userInfos };
      userInfoForm.userId = action.payload.data.id;
      return this.apiService.post('user-infos', userInfoForm).pipe(
        concatMap((resp) => of(
          new saveApiData({ data: resp, dataName: 'user-infos' }),
          new RefreshUserTable({ data: { store: 'user', storeState: 'user-infos' } })
        )),
        tap((response) => {
          this.spinner.hide();
          this.dialog.closeAll();
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  modifyUser$: Observable<Action> = this.actions$.pipe(
    ofType<ModifyUser>(ActionTypes.modifyUser),
    exhaustMap((action) => {
      const userForm = { ...action.payload.user };
      return this.apiService.patch('users', userForm, { setDeleteTime: false }).pipe(
        switchMap((response) => of(
          // new saveApiData({ data: response, dataName: 'users' }),
          new ModifyUserInfo({ userInfo: action.payload.userInfo, route: { store: 'user', storeState: 'user-infos' } })
        )),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  modifyUserInfo$: Observable<Action> = this.actions$.pipe(
    ofType<ModifyUserInfo>(ActionTypes.modifyUserInfo),
    exhaustMap((action) => {
      const userInfoForm = { ...action.payload.userInfo };
      return this.apiService.put('user-infos', userInfoForm, { setDeleteTime: false }).pipe(
        concatMap((resp) => of(
          new saveApiData({ data: resp, dataName: action.payload.route.storeState }),
          new RefreshUserTable({ data: { store: action.payload.route.store, storeState: action.payload.route.storeState } })
        )),
        tap((response) => {
          this.spinner.hide();
          this.dialog.closeAll();
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  deleteUser$: Observable<Action> = this.actions$.pipe(
    ofType<DeleteUser>(ActionTypes.deleteUser),
    exhaustMap((action) => {
      const userId = action.payload.userId;
      const userForm = { ...action.payload.user };
      return this.apiService.patch('users', { id: userId }, { setDeleteTime: true, setUpdateTime: true }).pipe(
        map((response) => {
          this.spinner.hide();
          return new DeleteUserInfo({ userInfoId: action.payload.userInfoId });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  activeUser$: Observable<Action> = this.actions$.pipe(
    ofType<ActiveUser>(ActionTypes.activeUser),
    exhaustMap((action) => {
      const user = { ...action.payload.user };
      delete user.deleted_at;
      const userInfo: any = Object.keys(new UserInfos()).reduce((prevValue, field) => {
        if (!!action.payload.userInfo[field]) {
          prevValue[field] = action.payload.userInfo[field];
        }
        return prevValue
      }, {});
      delete userInfo.deleted_at;
      delete userInfo.user;
      const requests = [
        // this.apiService.patch('users', { id: user.id, deleted_at: new Date(0)}, {setUpdateTime: true }),
        // this.apiService.patch('user-infos', { id: userInfo.id, deleted_at: new Date(0) }, {setUpdateTime: true }),
        this.apiService.put('users-reactivate', user, { setUpdateTime: true }),
        this.apiService.patch('user-infos', { id: userInfo.id, deleted_at: new Date() }, { setUpdateTime: true })
      ];
      return forkJoin(requests).pipe(
        map((response) => {
          return new RefreshUserTable({ data: { store: 'user', storeState: 'user-infos' } });
        }),
        tap(() => this.spinner.hide()),
        catchError((error) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  deleteUserInfo$: Observable<Action> = this.actions$.pipe(
    ofType<DeleteUserInfo>(ActionTypes.deleteUserInfo),
    exhaustMap((action) => {
      const userInfoId = action.payload.userInfoId;
      return this.apiService.patch('user-infos', { id: userInfoId }, { setDeleteTime: true, setUpdateTime: true }).pipe(
        map((response) => {
          this.spinner.hide();
          return new RefreshUserTable({ data: { store: 'user', storeState: 'user-infos' } });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  refreshUserTable$: Observable<Action> = this.actions$.pipe(
    ofType<RefreshUserTable>(ActionTypes.refreshUserTable),
    concatMap((action) => of(
      new UpdateData({
        data: action.payload.data.storeState,
        filter: {
          'where': {}, 'limit': 10, 'skip': 0, 'include': TestInfoTable.filterInclude, order: ['created_at DESC'],
        },
        concatResult: false, storeState: { store: action.payload.data.store, storeState: action.payload.data.storeState }
      }),
      new getTableDataLength({
        data: action.payload.data.storeState,
        filter: {}, storeState: { store: action.payload.data.store, storeState: action.payload.data.storeState + 'Count' }
      })
    )),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );


  @Effect()
  totalGetUsers$: Observable<Action> = this.actions$.pipe(
    ofType<totalGetUsers>(ActionTypes.totalGetUsers),
    withLatestFrom(this.store$.select((state: State) => state['user']['userPagination']), this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filter = {
        and: [{ roles: { like: '6142ca4bd97a767dbd8ad130' } }, { organizationId: action[2]['organizationId'] }]
      }
      return this.apiService.get('users', filter, { count: true }).pipe(
        map((response) => {
          let pagination = _.cloneDeep(action[1]);
          let totalPages = Math.ceil(response.count / pagination.pageSize)
          pagination.totalPage = totalPages
          let dispatch = [
            new userPagination({ pagination: pagination }),
            new showPage({ currentPage: pagination.currentPage }),
          ];
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );


  @Effect()
  showPage$: Observable<Action> = this.actions$.pipe(
    ofType<showPage>(ActionTypes.showPage),
    withLatestFrom((this.store$.select((state: State) => state['user']['userPagination'])), this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filter = {
        include: [{ relation: 'penalties' }],
        where: {
          and: [{ roles: { like: '6142ca4bd97a767dbd8ad130' } }, { organizationId: action[2]['organizationId'] }]
        },
        order: 'updated_at DESC',
        limit: action[1].limit,
        skip: action[1].skip

      }
      return this.apiService.get('users', filter).pipe(
        map((response) => {
          response.lastname = response.firstLastname + " " + response.secondLastname
          let now = new Date()
          response.forEach(user => {
            user.lastname = user.firstLastname + " " + user.secondLastname
            user.penaltyInfo = "Sin sanciones"
            if (user.penalties) {
              let consulta = user.penalties.find(penalty => {
                penalty.startDate = new Date(penalty.startDate);
                penalty.endDate = new Date(penalty.endDate);
                return penalty.startDate.valueOf() <= now.valueOf() && penalty.endDate.valueOf() >= now.valueOf()
              })
              if (consulta) {
                var date1 = new Date(consulta.startDate);
                var month1 = date1.getUTCMonth() + 1; //months from 1-12
                var day1 = date1.getUTCDate();
                var year1 = date1.getUTCFullYear();

                var date2 = new Date(consulta.endDate);
                var month2 = date2.getUTCMonth() + 1; //months from 1-12
                var day2 = date2.getUTCDate();
                var year2 = date2.getUTCFullYear();

                // user.penaltyInfo.=year + "-" + month + "-" + day;
                user.penaltyInfo = year1 + "-" + month1 + "-" + day1 + " - " + year2 + "-" + month2 + "-" + day2
              }
            }
          })
          return new saveUsers({ users: response });
          // return new showPage({ currentPage: action[1].currentPage });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  paginatorFinalUser$: Observable<Action> = this.actions$.pipe(
    ofType<paginatorFinalUser>(ActionTypes.paginatorFinalUser),
    withLatestFrom(this.store$.select((state: State) => state['user']['userPagination'])),
    map((action) => {
      let pagination = _.cloneDeep(action[1]);
      let dispatch = [];
      if (action[0].payload.direction == "skip_previous") {
        // pagination.limit = 5;
        pagination.skip = 0;
        pagination.currentPage = 1
        pagination.cantLeft = false
        pagination.cantRigth = true
        dispatch = [
          new userPagination({ pagination: pagination }),
          new showPage({ currentPage: pagination.currentPage }),
        ];
      } else if (action[0].payload.direction == "keyboard_arrow_left") {
        if (pagination.currentPage > 1) {
          pagination.cantLeft = true;
          pagination.cantRigth = true
          //pagination.limit = 5;
          pagination.skip = (pagination.skip - pagination.limit);
          pagination.currentPage = (pagination.currentPage - 1)
          dispatch = [
            new userPagination({ pagination: pagination }),
            new showPage({ currentPage: pagination.currentPage }),
          ];
        } else {
          pagination.cantLeft = false
          dispatch = [
            new userPagination({ pagination: pagination }),
            new showPage({ currentPage: pagination.currentPage }),
          ];
        }
      } else if (action[0].payload.direction == "keyboard_arrow_right") {
        if (pagination.currentPage < pagination.totalPage) {
          pagination.cantLeft = true
          pagination.cantRigth = true
          //pagination.limit = 5;
          pagination.skip = (pagination.skip + pagination.limit);
          pagination.currentPage = (pagination.currentPage + 1)
          dispatch = [
            new userPagination({ pagination: pagination }),
            new showPage({ currentPage: pagination.currentPage }),
          ];
        } else {
          pagination.cantRigth = false
          pagination.cantLeft = true
          dispatch = [
            new userPagination({ pagination: pagination }),
            new showPage({ currentPage: pagination.currentPage }),
          ];
        }
      } else {
        //pagination.limit = 5;
        pagination.skip = (pagination.totalPage - 1) * pagination.pageSize;
        pagination.currentPage = pagination.totalPage
        pagination.cantRigth = false
        pagination.cantLeft = true
        dispatch = [
          new userPagination({ pagination: pagination }),
          new showPage({ currentPage: pagination.currentPage }),
        ];
      }
      return dispatch;
    }),
    concatMap(dispatch => dispatch),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );


  @Effect()
  showPageSearch$: Observable<Action> = this.actions$.pipe(
    ofType<showPageSearch>(ActionTypes.showPageSearch),
    withLatestFrom((this.store$.select((state: State) => state['user']['userPagination'])), this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let term = action[0].payload.termSearch
      /* var pattern: any = new RegExp('.*' + term + '.*', "i"); /* case-insensitive RegExp search */
      //pattern = JSON.stringify(pattern) */
      let filter = {
        include: [{ relation: 'penalties' }],
        where: {
          or: [{ name: { like: term, options: "i" } }, { firstLastname: { like: term, options: "i" } }, { secondLastname: { like: term, options: "i" } }, { idNumber: { like: term, options: "i" } }, { email: { like: term, options: "i" } }],
          and: [{ roles: { like: '6142ca4bd97a767dbd8ad130' } }, { organizationId: action[2]['organizationId'] }]

        },
        order: 'updated_at DESC',
        limit: 100,
        // skip: action[1].skip
      }
      return this.apiService.get('users', filter).pipe(
        map((response) => {

          let now = new Date()
          response.forEach(user => {
            user.lastname = user.firstLastname + " " + user.secondLastname
            user.penaltyInfo = "Sin sanciones"
            if (user.penalties) {
              let consulta = user.penalties.find(penalty => {
                penalty.startDate = new Date(penalty.startDate);
                penalty.endDate = new Date(penalty.endDate);
                return penalty.startDate.valueOf() <= now.valueOf() && penalty.endDate.valueOf() >= now.valueOf()
              })
              if (consulta) {
                var date1 = new Date(consulta.startDate);
                var month1 = date1.getUTCMonth() + 1; //months from 1-12
                var day1 = date1.getUTCDate();
                var year1 = date1.getUTCFullYear();

                var date2 = new Date(consulta.endDate);
                var month2 = date2.getUTCMonth() + 1; //months from 1-12
                var day2 = date2.getUTCDate();
                var year2 = date2.getUTCFullYear();

                // user.penaltyInfo.=year + "-" + month + "-" + day;
                user.penaltyInfo = year1 + "-" + month1 + "-" + day1 + " - " + year2 + "-" + month2 + "-" + day2
              }
            }
          })

          let pagination = _.cloneDeep(action[1])
          let totalPages = Math.ceil(response.length / pagination.pageSize)
          pagination.totalPage = totalPages;
          pagination.currentPage = 1;
          let dispatch = [
            new userPagination({ pagination: pagination }),
            new setSearchTerm({ resultSearch: response }),
            new datasourceSearch({ sliceSearch: response.slice(0, pagination.pageSize) }),
          ];


          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );



  @Effect()
  paginatorFinalUserSearch$: Observable<Action> = this.actions$.pipe(
    ofType<paginatorFinalUserSearch>(ActionTypes.paginatorFinalUserSearch),
    withLatestFrom((this.store$.select((state: State) => state['user']['userPagination'])), (this.store$.select((state: State) => state['user']['setSearchTerm']))),
    map((action) => {
      let pagination = _.cloneDeep(action[1]);
      let dispatch = [];
      if (action[0].payload.direction == "skip_previous") {
        //pagination.limit = 5;
        pagination.skip = 0;
        pagination.currentPage = 1
        pagination.cantLeft = false
        pagination.cantRigth = true
        dispatch = [
          new userPagination({ pagination: pagination }),
          new datasourceSearch({ sliceSearch: action[2].slice(pagination.skip, pagination.pageSize) }),
        ];
      } else if (action[0].payload.direction == "keyboard_arrow_left") {
        if (pagination.currentPage > 1) {
          pagination.cantLeft = true;
          pagination.cantRigth = true
          pagination.skip = (pagination.skip - pagination.limit);
          pagination.currentPage = (pagination.currentPage - 1)
          dispatch = [
            new userPagination({ pagination: pagination }),
            new datasourceSearch({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
          ];
        } else {
          pagination.cantLeft = false
          dispatch = [
            new userPagination({ pagination: pagination }),
            new datasourceSearch({ sliceSearch: action[2].slice(0, pagination.limit) }),
          ];
        }
      } else if (action[0].payload.direction == "keyboard_arrow_right") {
        if (pagination.currentPage < pagination.totalPage) {
          pagination.cantLeft = true
          pagination.cantRigth = true
          // pagination.limit = 5;
          pagination.skip = (pagination.skip + pagination.limit);
          pagination.currentPage = (pagination.currentPage + 1)
          dispatch = [
            new userPagination({ pagination: pagination }),
            new datasourceSearch({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
          ];
        } else {
          pagination.cantRigth = false
          pagination.cantLeft = true
          dispatch = [
            new userPagination({ pagination: pagination }),
            new datasourceSearch({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
          ];
        }
      } else {
        //pagination.limit = 5;
        pagination.skip = (pagination.totalPage - 1) * pagination.pageSize;
        pagination.currentPage = pagination.totalPage
        pagination.cantRigth = false
        pagination.cantLeft = true
        dispatch = [
          new userPagination({ pagination: pagination }),
          new datasourceSearch({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
        ];
      }
      return dispatch;
    }),
    concatMap(dispatch => dispatch),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  @Effect()
  totalGetTrip$: Observable<Action> = this.actions$.pipe(
    ofType<totalGetTrip>(ActionTypes.totalGetTrip),
    withLatestFrom(this.store$.select((state: State) => state['user']['userPaginationTrip'])),
    exhaustMap((action) => {
      let filter = {
        userId: action[0].payload.userId
      };
      return this.apiService.get('trips', filter, { count: true }).pipe(
        map((response) => {
          let pagination = _.cloneDeep(action[1]);
          let totalPages = Math.ceil(response.count / pagination.pageSize)
          pagination.totalPage = totalPages
          let dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new showPageTrip({ currentPage: pagination.currentPage }),
          ];
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  showPageTrip$: Observable<Action> = this.actions$.pipe(
    ofType<showPageTrip>(ActionTypes.showPageTrip),
    withLatestFrom((this.store$.select((state: State) => state['user']['userPaginationTrip'])), (this.store$.select((state: State) => state['user']['totalGetTrip']))),
    exhaustMap((action) => {
      let filter = {
        include: [{ relation: 'bike' }, { relation: 'startStation' }, { relation: 'endStation' }],
        where: {
          userId: action[2],
        },
        order: 'updated_at DESC',
        limit: action[1].limit,
        skip: action[1].skip

      }
      return this.apiService.get('trips', filter).pipe(
        map((response) => {
          response.forEach(element => {
            element.numberBike = element.bike.number;
            element.typeBike = element.bike.type;
            element.fromStation = element.startStation.name
            element.toStation = element.endStation.name
          });
          return new saveUserTrips({ trips: response });
          // return new showPage({ currentPage: action[1].currentPage });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  paginatorFinalUserTrip$: Observable<Action> = this.actions$.pipe(
    ofType<paginatorFinalUserTrip>(ActionTypes.paginatorFinalUserTrip),
    withLatestFrom(this.store$.select((state: State) => state['user']['userPaginationTrip'])),
    map((action) => {
      let pagination = _.cloneDeep(action[1]);
      let dispatch = [];
      if (action[0].payload.direction == "skip_previous") {
        // pagination.limit = 5;
        pagination.skip = 0;
        pagination.currentPage = 1
        pagination.cantLeft = false
        pagination.cantRigth = true
        dispatch = [
          new userPaginationTrip({ pagination: pagination }),
          new showPageTrip({ currentPage: pagination.currentPage }),
        ];
      } else if (action[0].payload.direction == "keyboard_arrow_left") {
        if (pagination.currentPage > 1) {
          pagination.cantLeft = true;
          pagination.cantRigth = true
          //pagination.limit = 5;
          pagination.skip = (pagination.skip - pagination.limit);
          pagination.currentPage = (pagination.currentPage - 1)
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new showPageTrip({ currentPage: pagination.currentPage }),
          ];

        } else {
          pagination.cantLeft = false
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new showPageTrip({ currentPage: pagination.currentPage }),
          ];
        }
      } else if (action[0].payload.direction == "keyboard_arrow_right") {
        if (pagination.currentPage < pagination.totalPage) {
          pagination.cantLeft = true
          pagination.cantRigth = true
          //pagination.limit = 5;
          pagination.currentPage = (pagination.currentPage + 1)
          pagination.skip = (pagination.skip + pagination.limit);
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new showPageTrip({ currentPage: pagination.currentPage }),
          ];
        } else {
          pagination.cantRigth = false
          pagination.cantLeft = true
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new showPageTrip({ currentPage: pagination.currentPage }),
          ];
        }
      } else {
        //pagination.limit = 5;
        pagination.skip = (pagination.totalPage - 1) * pagination.pageSize;
        pagination.currentPage = pagination.totalPage
        pagination.cantRigth = false
        pagination.cantLeft = true
        dispatch = [
          new userPaginationTrip({ pagination: pagination }),
          new showPageTrip({ currentPage: pagination.currentPage }),
        ];
      }
      return dispatch;
    }),
    concatMap(dispatch => dispatch),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  @Effect()
  showPageSearchTrip$: Observable<Action> = this.actions$.pipe(
    ofType<showPageSearchTrip>(ActionTypes.showPageSearchTrip),
    withLatestFrom((this.store$.select((state: State) => state['user']['userPaginationTrip'])), (this.store$.select((state: State) => state['user']['totalGetTrip']))),
    exhaustMap((action) => {
      let and: any = [{ userId: action[2] }]
      let fromDate = new Date(action[0].payload.termSearch.fromDate).toJSON()
      //fromDate=fromDate.toISOs();
      let toDate = new Date(action[0].payload.termSearch.toDate).toJSON()
      //toDate=toDate.toJSON();
      if (action[0].payload.termSearch.fromDate) {
        and.push({ startDate: { gte: fromDate } })
      }
      if (action[0].payload.termSearch.toDate) {
        and.push({ endDate: { lte: toDate } })
      }
      if (action[0].payload.termSearch.stationId) {
        and.push({ startStationId: action[0].payload.termSearch.stationId })
      }
      let filter: any = {
        include: [{ relation: 'bike' }, { relation: 'startStation' }, { relation: 'endStation' }],
        where: { and: and }
      }

      return this.apiService.get('trips', filter).pipe(
        map((response) => {
          response.forEach(element => {
            element.numberBike = element.bike.number;
            element.typeBike = element.bike.type;
            element.fromStation = element.startStation.name
            element.toStation = element.endStation.name
          });

          let pagination = _.cloneDeep(action[1])
          let totalPages = Math.ceil(response.length / pagination.pageSize)
          pagination.totalPage = totalPages;
          pagination.currentPage = 1;
          let dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new setSearchTermTrip({ resultSearch: response }),
            new datasourceSearchTrip({ sliceSearch: response.slice(0, pagination.pageSize) }),
          ];


          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );
  @Effect()
  paginatorFinalUserSearchTrip$: Observable<Action> = this.actions$.pipe(
    ofType<paginatorFinalUserSearchTrip>(ActionTypes.paginatorFinalUserSearchTrip),
    withLatestFrom((this.store$.select((state: State) => state['user']['userPaginationTrip'])), (this.store$.select((state: State) => state['user']['setSearchTermTrip']))),
    map((action) => {

      let pagination = _.cloneDeep(action[1]);
      let dispatch = [];
      if (action[0].payload.direction == "skip_previous") {
        //pagination.limit = 5;
        pagination.skip = 0;
        pagination.currentPage = 1
        pagination.cantLeft = false
        pagination.cantRigth = true
        dispatch = [
          new userPaginationTrip({ pagination: pagination }),
          new datasourceSearchTrip({ sliceSearch: action[2].slice(pagination.skip, pagination.pageSize) }),
        ];
      } else if (action[0].payload.direction == "keyboard_arrow_left") {
        if (pagination.currentPage > 1) {
          pagination.cantLeft = true;
          pagination.cantRigth = true
          pagination.skip = (pagination.skip - pagination.limit);
          pagination.currentPage = (pagination.currentPage - 1)
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new datasourceSearchTrip({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
          ];
        } else {
          pagination.cantLeft = false
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new datasourceSearchTrip({ sliceSearch: action[2].slice(0, pagination.limit) }),
          ];
        }
      } else if (action[0].payload.direction == "keyboard_arrow_right") {
        if (pagination.currentPage < pagination.totalPage) {
          pagination.cantLeft = true
          pagination.cantRigth = true
          // pagination.limit = 5;
          pagination.skip = (pagination.skip + pagination.limit);
          pagination.currentPage = (pagination.currentPage + 1)
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new datasourceSearchTrip({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
          ];
        } else {
          pagination.cantRigth = false
          pagination.cantLeft = true
          dispatch = [
            new userPaginationTrip({ pagination: pagination }),
            new datasourceSearchTrip({ sliceSearch: action[2].slice(pagination.skip) }),
          ];

        }
      } else {
        //pagination.limit = 5;
        pagination.skip = (pagination.totalPage - 1) * pagination.pageSize;
        pagination.currentPage = pagination.totalPage
        pagination.cantRigth = false
        pagination.cantLeft = true

        dispatch = [
          new userPaginationTrip({ pagination: pagination }),
          new datasourceSearchTrip({ sliceSearch: action[2].slice(pagination.skip, pagination.skip + pagination.limit) }),
        ];

      }
      return dispatch;
    }),
    concatMap(dispatch => dispatch),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );


  @Effect()
  getStationsTrip$: Observable<Action> = this.actions$.pipe(
    ofType<getStationsTrip>(ActionTypes.getStationsTrip),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {

      let filter: any = {
        where: { organizationId: action[1]['organizationId'] }
      };
      return this.apiService.get('stations', filter).pipe(
        map((response) => {
          return new stationsTrips({ stations: response });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  patchUser$: Observable<Action> = this.actions$.pipe(
    ofType<patchUser>(ActionTypes.patchUser),
    withLatestFrom((this.store$.select((state: State) => state['user']['modalUser']))),
    exhaustMap((action) => {
      let filter: any = {};
      return this.apiService.patch('users', { id: action[1].id, accountState: action[0].payload.stateUser }, { setUpdateTime: false }).pipe(
        map((response) => {
          let dispatch = [
            new totalGetUsers(),
            new modalUser({ user: {} }),
          ];
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  calculationDates$: Observable<Action> = this.actions$.pipe(
    ofType<calculationDates>(ActionTypes.calculationDates),
    withLatestFrom((this.store$.select((state: State) => state['user']['penaltyDates']))),
    map((action) => {
      let penalty = _.cloneDeep(action[1])
      penalty.startDate = new Date()
      if (action[0].payload.penaltyType == "leve") {
        penalty.endDate = new Date(penalty.startDate.setDate(penalty.startDate.getDate() + 2))
        return new penaltyDates({ dates: penalty });
      }
      else if (action[0].payload.penaltyType == "medio") {
        penalty.endDate = new Date(penalty.startDate.setDate(penalty.startDate.getDate() + 6))
        return new penaltyDates({ dates: penalty });
      }
      else if (action[0].payload.penaltyType == "considerable") {
        penalty.endDate = new Date(penalty.startDate.setDate(penalty.startDate.getDate() + 15))
        return new penaltyDates({ dates: penalty });
      }
      else if (action[0].payload.penaltyType == "grave") {
        penalty.endDate = new Date(penalty.startDate.setDate(penalty.startDate.getDate() + 60))
        return new penaltyDates({ dates: penalty });
      }
      else if (action[0].payload.penaltyType == "gravisima") {
        penalty.endDate = new Date(penalty.startDate.setDate(penalty.startDate.getDate() + 120))
        return new penaltyDates({ dates: penalty });
      } else {
        penalty.endDate = new Date(penalty.startDate.setDate(penalty.startDate.getDate() + 730))
        return new penaltyDates({ dates: penalty });
      }

    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );


  @Effect()
  getPenaltyTypes$: Observable<Action> = this.actions$.pipe(
    ofType<getPenaltyTypes>(ActionTypes.getPenaltyTypes),

    exhaustMap((action) => {
      let filter: any = {
        where: {
          and: [{ table: "penalty" }, { field: "type" }]
        }
      };
      return this.apiService.get('master-lists', filter).pipe(
        map((response) => {
          return new penaltyTypes({ penaltys: response });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );
  @Effect()
  postPenaltys$: Observable<Action> = this.actions$.pipe(
    ofType<postPenaltys>(ActionTypes.postPenaltys),
    withLatestFrom((this.store$.select((state: State) => state['user']['modalUser']))),
    exhaustMap((action) => {
      let penalty = _.cloneDeep(action[0].payload.penalty)
      penalty.startDate = penalty.startDate.toISOString()
      penalty.endDate = penalty.endDate.toISOString()
      penalty.userId = action[1].id
      penalty.createdAt = new Date();
      penalty.updatedAt = new Date();
      return this.apiService.post('penalties', penalty, { setCreateTime: false }).pipe(
        map((response) => {
          return new totalGetUsers();
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  getTotalBikes$: Observable<Action> = this.actions$.pipe(
    ofType<getTotalBikes>(ActionTypes.getTotalBikes),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filter: any = {
      }
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter = { organizationId: action[1]['organizationId'] }
      }
      return this.apiService.get('bikes', filter, { count: true }).pipe(
        map((response) => {
          let dispatch = [
            new totalBikes({ totalBikes: response.count }),
            //new setCurrentPageBike({ currentPage: 1 }),
            new getBikes(),
          ];
          // return new showPage({ currentPage: action[1].currentPage });
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  getBikes$: Observable<Action> = this.actions$.pipe(
    ofType<getBikes>(ActionTypes.getBikes),
    withLatestFrom((this.store$.select((state: State) => state.apiData.user)), (this.store$.select((state: State) => state.user.bikePageSize)), (this.store$.select((state: State) => state.user.skipPageBike))),
    exhaustMap((action) => {
      let filter: any = {
        include: [{ relation: 'lock' }, { relation: 'station' }],
        order: 'updatedAt DESC',
        limit: action[2],
        skip: action[3]
      }
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter.where = { organizationId: action[1]['organizationId'] }
      }
      return this.apiService.get('bikes', filter).pipe(
        map((response) => {
          return new setBikes({ bikes: response });
          // return new showPage({ currentPage: action[1].currentPage });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  showPageBikes$: Observable<Action> = this.actions$.pipe(
    ofType<showPageBikes>(ActionTypes.showPageBikes),
    withLatestFrom((this.store$.select((state: State) => state.user.searchBikesForm))),
    map((action) => {
      if (action[1]['isSearch'] === true) {
        return new showSearchPageBike();
      }
      else {
        return new getBikes();
      }

    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );


  @Effect()
  filterSearchLocksBikes$: Observable<Action> = this.actions$.pipe(
    ofType<filterSearchLocksBikes>(ActionTypes.filterSearchLocksBikes),
    withLatestFrom((this.store$.select((state: State) => state.apiData.user)), (this.store$.select((state: State) => state.user.searchBikesForm))),
    exhaustMap((action) => {
      let imeiOqr2: any = action[2]['imeiOqr']
      let num = imeiOqr2;
      let filter = {
        where: {
          and: [
            {
              or: [{ imei: { like: num, "options": "i" } },
              { qrNumber: { like: num, "options": "i" } }]
            },
            { organizationId: action[1]['organizationId'] }
          ]
        }
      };
      return this.apiService.get('locks', filter).pipe(
        map((response) => {
          let bike: any = []
          if (response.length > 0) {
            response.forEach(id => {
              bike.push(id.bikeId)
            });
          }
          return new getLocksSearch({ bikeId: bike });
          // return new showPage({ currentPage: action[1].currentPage });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );

    })
  );

  @Effect()
  getLocksSearch$: Observable<Action> = this.actions$.pipe(
    ofType<getLocksSearch>(ActionTypes.getLocksSearch),
    withLatestFrom((this.store$.select((state: State) => state.apiData.user)), (this.store$.select((state: State) => state.user.searchBikesForm)), (this.store$.select((state: State) => state.user.bikePageSize))),
    exhaustMap((action) => {
      let where: any = {}
      if (action[2]['imeiOqr'] != '' && action[2]['station'] == '') {
        where = { and: [{ or: [{ number: { like: action[2]['imeiOqr'], "options": "i" } }, { id: { inq: action[0].payload.bikeId } }] }, { organizationId: action[1]['organizationId'] }] }
      }
      else if (action[2]['station'] != '' && action[2]['imeiOqr'] == '') {
        where = { and: [{ stationId: action[2]['station'] }, { organizationId: action[1]['organizationId'] }] }
      }
      else {
        where = { and: [{ or: [{ id: { inq: action[0].payload.bikeId } }, { number: { like: action[2]['imeiOqr'], "options": "i" } }] }, { stationId: action[2]['station'] }, { organizationId: action[1]['organizationId'] }] }
      }
      let filter = {
        include: [{ relation: 'lock' }, { relation: 'station' }],
        where,
        limit: 100,
      }
      return this.apiService.get('bikes', filter).pipe(
        exhaustMap((response) => {
          let dispatch = [
            new totalBikes({ totalBikes: response.length }),
            new bikesSearchResult({ bikesSearch: response }),
            new showSearchPageBike()

          ]
          return dispatch;
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );


  @Effect()
  showSearchPageBike$: Observable<Action> = this.actions$.pipe(
    ofType<showSearchPageBike>(ActionTypes.showSearchPageBike),
    withLatestFrom((this.store$.select((state: State) => state.user.bikesSearchResult)), (this.store$.select((state: State) => state.user.bikePageSize)), (this.store$.select((state: State) => state.user.skipPageBike))),
    map((action) => {
      let dataSearch = action[1].slice(action[3], (action[2] + action[3]))
      return new setBikes({ bikes: dataSearch });
      // return new showPage({ currentPage: action[1].currentPage });
    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  //Efectos de Bikes
  @Effect()
  getMasterListTypesBikes$ = this.actions$.pipe(
    ofType<getMasterListTypesBikes>(ActionTypes.getMasterListTypesBikes),
    exhaustMap((action) => {
      let filter: any = {
        where: {
          and: [{ table: "bike" }, { field: "type" }]
        }
      };
      return this.apiService.get('master-lists', filter).pipe(
        map((response) => {
          return new saveTypesBikes({ types: response });
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }));
        })
      )
    })
  );

  @Effect()
  getStationsBikes$ = this.actions$.pipe(
    ofType<getStationsBikes>(ActionTypes.getStationsBikes),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filter: any = {
        where: { organizationId: action[1]['organizationId'] }
      };
      return this.apiService.get('stations', filter).pipe(
        map((response) => {
          return new saveStationsBikes({ stations: response });
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }));
        })
      )
    })
  );

  @Effect()
  getMasterListStateBikes$ = this.actions$.pipe(
    ofType<getMasterListStateBikes>(ActionTypes.getMasterListStateBikes),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filter: any = {
        where: {
          and: [{ table: "bike" }, { field: "state" }]
        }
      };
      return this.apiService.get('master-lists', filter).pipe(
        map((response) => {
          return new saveStateBikes({ state: response });
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }));
        })
      )
    })
  );

  @Effect()
  postBike$ = this.actions$.pipe(
    ofType<postBike>(ActionTypes.postBike),
    withLatestFrom((this.store$.select((state: State) => state.apiData.user)), (this.store$.select((state: State) => state.user.selectedLock))),
    exhaustMap((action) => {
      let dispatch: any = []
      let bikeForm = _.cloneDeep(action[0])
      bikeForm.payload.bike.tripCount = 0
      bikeForm.payload.bike.latitude = "0"
      bikeForm.payload.bike.longitude = "0"
      bikeForm.payload.bike.battery = "0"
      bikeForm.payload.bike.organizationId = action[1]['organizationId']
      return this.apiService.post('bikes', bikeForm.payload.bike, { setCreateTime: true }).pipe(
        map((response) => {
          if (action[2]['id']) {
            dispatch = [
              new patchLockBike({ idLock: action[2]['id'], idBike: response.id }),
              // new getTotalBikes()
            ]
          }
          else {
            dispatch = [
              new getTotalBikes()
            ]
          }
          return dispatch
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          if (error.status == 404) { return of(new ApiError({ error }), new showSweetAlert({ title: 'error', text: 'This username or email is already in use', type: 'error', buttonText: 'Accept' })) }
          else { return of(new ApiError({ error })) };
        })
      );
    })
  );


  @Effect()
  decisionPatchLockBike$ = this.actions$.pipe(
    ofType<decisionPatchLockBike>(ActionTypes.decisionPatchLockBike),
    withLatestFrom((this.store$.select((state: State) => state.user.selectedLock)), (this.store$.select((state: State) => state.user.modalBike))),
    map((action) => {
      let dispatch: any = []

      //la bici tenia un candado seleccionado, quedo otro  y es diferente al que tenia.
      if (action[1]['id'] && action[2]['lock'] && action[1]['id'] != action[2]['lock']) {
        dispatch = [
          new patchDeleteLockBike({ idLock: action[2].lock.id }),
          new patchLockBike({ idLock: action[1].id, idBike: action[0].payload.idBike }),
          new searchBikeForm({ form: { isSearch: false, imeiOqr: "", station: "" } })
        ]
      }
      //la bici tenia un candado seleccionado y no quedo ninguno seleccionado

      else if (action[2]['lock'] && !action[1]['id']) {
        dispatch = [
          new patchDeleteLockBike({ idLock: action[2].lock.id }),
          new searchBikeForm({ form: { isSearch: false, imeiOqr: "", station: "" } })
        ]
      }
      //la bici no tenia candado seleccionado pero quedo un candado nuevo
      else if (!action[2]['lock'] && action[1]['id']) {
        dispatch = [
          new patchLockBike({ idLock: action[1].id, idBike: action[0].payload.idBike }),
          new searchBikeForm({ form: { isSearch: false, imeiOqr: "", station: "" } })
        ]
      }
      // la bici no queda con ningun candado y no tenia ningun candado seleccionado
      else if (!action[1]['id'] && !action[2]['lock']) {
        dispatch = [
          new searchBikeForm({ form: { isSearch: false, imeiOqr: "", station: "" } }),
          new getTotalBikes(),


        ]
      }

      return dispatch
    }),
    concatMap(dispatch => dispatch),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );



  @Effect()
  patchLockBike$ = this.actions$.pipe(
    ofType<patchLockBike>(ActionTypes.patchLockBike),
    exhaustMap((action) => {
      return this.apiService.patch('locks', { id: action.payload.idLock, bikeId: action.payload.idBike }, { setDeleteTime: false }).pipe(
        map((response) => {
          return new getTotalBikes()
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  patchDeleteLockBike$ = this.actions$.pipe(
    ofType<patchDeleteLockBike>(ActionTypes.patchDeleteLockBike),
    exhaustMap((action) => {
      return this.apiService.patch('locks', { id: action.payload.idLock, bikeId: "" }, { setDeleteTime: false }).pipe(
        map((response) => {
          return new getTotalBikes()
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  patchBike$ = this.actions$.pipe(
    ofType<patchBike>(ActionTypes.patchBike),
    withLatestFrom(this.store$.select((state: State) => state.user.selectedLock)),
    exhaustMap((action) => {
      /* let lockIdSelected:any="hola" */
      /* if(action[1].id){
        lockIdSelected=action[1].id
      } */
      return this.apiService.patch('bikes', { id: action[0].payload.idBike, number: action[0].payload.formBike.number, type: action[0].payload.formBike.type, stationId: action[0].payload.formBike.stationId, state: action[0].payload.formBike.state }, { setDeleteTime: false }).pipe(
        map((response) => {
          return new decisionPatchLockBike({ idBike: action[0].payload.idBike })
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );



  @Effect()
  getCountLocksBikes$ = this.actions$.pipe(
    ofType<getCountLocksBikes>(ActionTypes.getCountLocksBikes),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      let filter: any = {
        and: [{ or: [{ bikeId: { inq: [null, undefined] } }, { bikeId: "" }] }],
      }
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter.and.push({ organizationId: action[1]['organizationId'] })
      }
      return this.apiService.get('locks', filter, { count: true }).pipe(
        map((response) => {
          let dispatch = [
            new totalLockBikes({ totalLocks: response.count }),
            //new setCurrentPageBike({ currentPage: 1 }),
            new getLocksBikes(),
          ];

          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );


  @Effect()
  getLocksBikes$ = this.actions$.pipe(
    ofType<getLocksBikes>(ActionTypes.getLocksBikes),
    withLatestFrom((this.store$.select((state: State) => state.apiData.user)), (this.store$.select((state: State) => state.user.locksPageSize)), (this.store$.select((state: State) => state.user.skipPageLocks))),
    exhaustMap((action) => {
      let filter: any = {
        where: {
          and: [{ or: [{ bikeId: { inq: [null, undefined] } }, { bikeId: "" }] }],
        },
        order: 'updatedAt DESC',
        limit: action[2],
        skip: action[3]
      }
      if (action[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter.where.and.push({ organizationId: action[1]['organizationId'] })
      }
      return this.apiService.get('locks', filter).pipe(
        map((response) => {
          return new saveLocksBikes({ locks: response });
          // return new showPage({ currentPage: action[1].currentPage });
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );


  @Effect()
  showPageLocksBikes$ = this.actions$.pipe(
    ofType<showPageLocksBikes>(ActionTypes.showPageLocksBikes),
    withLatestFrom((this.store$.select((state: State) => state.user.searchLocksBikesForm))),
    map((action) => {
      if (action[1]['isSearch'] === true) {
        return new showSearchPageLocksBike();
      }
      else {
        return new getLocksBikes();
      }

    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );

  @Effect()
  showSearchPageLocksBike$: Observable<Action> = this.actions$.pipe(
    ofType<showSearchPageLocksBike>(ActionTypes.showSearchPageLocksBike),
    withLatestFrom((this.store$.select((state: State) => state.user.locksBikesSearchResult)), (this.store$.select((state: State) => state.user.locksPageSize)), (this.store$.select((state: State) => state.user.skipPageLocks))),
    map((action) => {
      let dataSearch = action[1].slice(action[3], (action[2] + action[3]))
      return new saveLocksBikes({ locks: dataSearch });
      // return new showPage({ currentPage: action[1].currentPage });
    }),
    catchError((error: HttpErrorResponse) => {
      this.spinner.hide();
      return of(new ApiError({ error }));
    })
  );


  @Effect()
  getModalLocksSearch$ = this.actions$.pipe(
    ofType<getModalLocksSearch>(ActionTypes.getModalLocksSearch),
    withLatestFrom((this.store$.select((state: State) => state.apiData.user)), (this.store$.select((state: State) => state.user.searchLocksBikesForm))),
    exhaustMap((action) => {

      let filter = {
        where: {
          and: [{ organizationId: action[1]['organizationId'] }, { or: [{ bikeId: { inq: [null, undefined] } }, { bikeId: "" }] }, { or: [{ imei: { like: action[2]['imeiOqr'] } }, { qrNumber: { like: action[2]['imeiOqr'] } }] }],
        },
        order: 'updatedAt DESC',
        limit: 100,
      }
      return this.apiService.get('locks', filter).pipe(
        map((response) => {
          let dispatch = [
            new totalLockBikes({ totalLocks: response.length }),
            new locksBikesSearchResult({ locksSearch: response }),
            new showSearchPageLocksBike()

          ]
          return dispatch;
        }),
        concatMap(dispatch => dispatch),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  constructor(
    private actions$: Actions,
    private apiService: ApiService,
    private store$: Store<any>,
    private spinner: SplashScreenService,
    private router: Router,
    private dialog: MatDialog
  ) { }
}
