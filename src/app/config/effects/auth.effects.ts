import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { Observable, of, interval } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { exhaustMap, switchMap, tap, catchError, filter, map, withLatestFrom, concatMap } from 'rxjs/operators';
import { ActionTypes, loginUser, refreshTokenInit, refreshToken, validateUser, getPermissions, setPermissions } from '../global.actions';
import { ApiError, saveApiData, showSweetAlert, logOutUser, getUserData } from '../global.actions';
import { AuthService } from '../../services/auth.service';
import { User } from 'src/app/models/user.model';
import { HttpErrorResponse } from '@angular/common/http';
import * as fromSidenav from '../../modules/core/sidenav/shared/sidenav.action';
import { SplashScreenService } from '../../services/splash-screen.service';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { SidenavItem } from 'src/app/modules/core/sidenav/sidenav-item/sidenav-item.model';
import { bikesharingNav, bikesharingSubItems, dashboard, gestionSubItems, locksInteligents, locksSubItems } from 'src/app/models/route.model';


@Injectable()

export class AuthEffects {
    @Effect()
    loginUser$: Observable<Action> = this.actions$.pipe(
        ofType<loginUser>(ActionTypes.loginUser),
        exhaustMap((action) => {
            return this.auth.loginUser(action.payload.form).pipe(
                map((response: any) => new getUserData({ userInfo: { id: response.id_user, ...action.payload.form }, token: response.token })),
                catchError((error: HttpErrorResponse) => {
                    this.spinner.hide();
                    this.globalService.clearStorage;
                    if (error.status == 404) { return of(new ApiError({ error }), new showSweetAlert({ title: 'error', text: 'Usuario ' + action.payload.form.email + ' no encontrado', type: 'error', buttonText: 'Aceptar' })) }
                    else {
                        if (error.status == 401) { return of(new ApiError({ error }), new showSweetAlert({ title: 'error', text: 'Contraseña incorrecta', type: 'error', buttonText: 'Aceptar' })) }
                    }
                })
            );
        })
    );

    @Effect()
    getUserData$: Observable<Action> = this.actions$.pipe(
        ofType<getUserData>(ActionTypes.getUserData),
        exhaustMap((action: any) => {
            return this.auth.getUserRoles(action.payload.token, action.payload.userInfo).pipe(
                map((response: any) => {
                    if (response.roles && response.user[0]) {
                        return new validateUser({ user: { ...response.user[0], password: response.password }, roles: response.roles, token: action.payload.token })
                    } else {
                        this.spinner.hide();
                        return new showSweetAlert({ title: 'Acceso denegado', text: 'No tiene cuenta administrativa', type: 'error', buttonText: 'Aceptar' })
                    }
                }),
            );
        })
    );

    @Effect()
    getPermissions$ = this.actions$.pipe(
        ofType<getPermissions>(ActionTypes.getPermissions),
        switchMap((action: any) => {
            return this.auth.getPermissionsData(action.payload).pipe(
                map((rolePermissions: any) => {
                    let navigationMenu: any = routesPermissions(rolePermissions);
                    let acceptedRoute = navigationMenu[1].find((route)=> route == this.router.url)
                    
                    if (this.router.url == '/' || this.router.url == '/auth/login' || !acceptedRoute) {
                        this.router.navigate(['/']);
                    }
                    this.spinner.hide();
                    
                    navigationMenu[0].push(new setPermissions({permissions: rolePermissions}))
                    return navigationMenu[0];
                }),
                concatMap((response) => response),
                catchError((error: HttpErrorResponse) => {
                    return of(new ApiError({ error }));
                })
            );
        })
    );

    @Effect()
    validateUser$: Observable<Action> = this.actions$.pipe(
        ofType<validateUser>(ActionTypes.validateUser),
        switchMap((action: any) => {
            if (action.payload.roles && action.payload.user) {
                let appAccess = false;
                var typeAccess: any;
                let rolesIds: any = [];
                action.payload.user.userRoles.forEach(userRole => {
                    let typeRole = action.payload.roles.find(role => role.id == userRole.roleId);
                    if (typeRole.name == 'admin' || typeRole.name == 'rebalanceador' || typeRole.name == 'superAdmin') {
                        appAccess = true;
                        typeAccess = userRole.roleId
                        rolesIds.push(userRole.roleId);
                    }
                });
                if (appAccess) {
                    this.globalService.setStorage('token', action.payload.token)
                    return [
                        new getPermissions(rolesIds),
                        new saveApiData({ data: action.payload.user, dataName: 'user' }),
                        new saveApiData({ data: action.payload.token, dataName: 'token' }),
                        new refreshTokenInit()
                    ];
                } else {
                    this.spinner.hide();
                    return [new logOutUser(), new showSweetAlert({ title: 'Acceso denegado', text: 'No tiene cuenta administrativa', type: 'error', buttonText: 'Aceptar' })]
                }
            } else {
                this.spinner.hide();
                return [new logOutUser(), new showSweetAlert({ title: 'Acceso denegado', text: 'No tiene cuenta administrativa', type: 'error', buttonText: 'Aceptar' })]
            }
        }),
        catchError((error: HttpErrorResponse) => {
            this.spinner.hide();
            this.globalService.clearStorage;
            if (error.status == 404) { return of(new logOutUser(), new ApiError({ error }), new showSweetAlert({ title: 'error', text: 'Usuario no encontrado', type: 'error', buttonText: 'Aceptar' })) }
            else {
                if (error.status == 401) { return of(new logOutUser(), new ApiError({ error }), new showSweetAlert({ title: 'error', text: 'Credenciales incorrectas', type: 'error', buttonText: 'Aceptar' })) }
            }
        })
    );

    @Effect({ dispatch: false })
    saveUserInLocalStore$ = this.actions$.pipe(
        ofType<saveApiData>(ActionTypes.saveApiData),
        filter((action) => action.payload.dataName === 'user'),
        map((action) => this.globalService.setStorage('user', action.payload.data))
    );


    @Effect()
    refreshToken$ = this.actions$.pipe(
        ofType<refreshToken>(ActionTypes.refreshToken),
        withLatestFrom(this.store$),
        switchMap((actionAndstore: Array<any>) => {
            const user = actionAndstore[1].apiData.user.email;
            const password = actionAndstore[1].apiData.user.password;
            return this.auth.refreshToken(user, password).pipe(
                map((token) => new saveApiData({ data: token, dataName: 'token' })),
                catchError((error: HttpErrorResponse) => {
                    return of(new ApiError({ error }));
                })
            );
        })
    );

    @Effect()
    refreshTokenInit$ = this.actions$.pipe(
        ofType<refreshTokenInit>(ActionTypes.refreshTokenInit),
        switchMap(() => interval(480000).pipe(// 8 minutes
            map(() => new refreshToken())
        ))
    );

    @Effect()
    effectsInit$ = this.actions$.pipe(
        ofType(ROOT_EFFECTS_INIT),
        exhaustMap(() => this.globalService.getStorage('user').pipe(
            filter(user => user !== null),
            map((user) => new loginUser({ form: user, withoutRouting: true }))
        ))
    );

    constructor(
        private actions$: Actions,
        private auth: AuthService,
        private globalService: GlobalService,
        private spinner: SplashScreenService,
        private store$: Store,
        private router: Router
    ) { }
}

function routesPermissions(rolePermissions) {
    /* 
     * Build an arrangement structure with the accesses allowed by user.
     */
    let acceptedRoutes: any = [];
    validatePathsAccess(gestionSubItems, rolePermissions, dashboard, acceptedRoutes)
    validatePathsAccess(locksSubItems, rolePermissions, locksInteligents, acceptedRoutes)
    validatePathsAccess(bikesharingSubItems, rolePermissions, bikesharingNav, acceptedRoutes)

    let SidenavItemAction: any = [
        new fromSidenav.AddSidenavItemAction(dashboard),
        new fromSidenav.AddSidenavItemAction(bikesharingNav),
        new fromSidenav.AddSidenavItemAction(locksInteligents)
    ]

    //Validate that the titles to display are not empty
    let dispatchSidenavItem = dispacTitlesToShow(SidenavItemAction, acceptedRoutes)

    // Send the created Menu structure to Redux/ngrx (you only need to send the Top Level Item, all dropdown items will be added automatically)
    return dispatchSidenavItem
}

function validatePathsAccess(sidenavItem: any, rolePermissions: any, title: any, acceptedRoutes: any) {
    sidenavItem.forEach((subItems: any) => {
        let comparison = rolePermissions.find((permission) => permission.id == subItems.permissionId)
        if (comparison) {
            acceptedRoutes.push(subItems.route);
            title.subItems.push(subItems);
        }
    })
}

function dispacTitlesToShow(SidenavItemAction: any, acceptedRoutes: Array<any>) {
    let dispacSidenavItemAction: any = [];
    SidenavItemAction.forEach(sidenavItem => {
        if (sidenavItem.payload.subItems.length != 0) {
            dispacSidenavItemAction.push(sidenavItem)
        }
    });
    return [dispacSidenavItemAction, acceptedRoutes]
}

