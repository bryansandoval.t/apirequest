import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { Observable, of, interval } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { exhaustMap, switchMap, tap, catchError, filter, map, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { HttpErrorResponse } from '@angular/common/http';

import { ApiService } from 'src/app/services/api.service';
import { ActionTypes, ApiError, buildSearchTripsFilter, setTripsFormErrors, tripFormValidation, organizationFormValidation, setOrganizationFormErrors, setStationFormErrors, stationFormValidation, updateStationTable, uploadOrganizationLogo } from '../global.actions';
import { State } from '../global.reducer';
import { ValidationService } from 'src/app/services/validation.service';
import { GlobalService } from 'src/app/services/global.service';
import * as validate from 'validate.js';

@Injectable()

export class ValidationsEffects {
    objectShape({ addObject, property, status, message }) {
        const object = { status: status, message: message }
        addObject[property] = object
    }

    @Effect()
    stationFormValidation$: Observable<Action> = this.actions$.pipe(
        ofType<stationFormValidation>(ActionTypes.stationFormValidation),
        withLatestFrom(this.store$.select((state: State) => state.apiData.modalStationInfo)),
        withLatestFrom(this.store$.select((state: State) => state.apiData.stationModalType)),
        exhaustMap((action) => {
            let addObject: any = new Object();

            const objectsKey: any = Object.keys(action[0][1]);
            if (action[0][1]['bikesCapacity'] <= '0') {
                this.objectShape({ addObject, property: 'bikesCapacity', status: true, message: 'La capacidad debe ser mayor que cero' });
            };
            if (isNaN(parseInt(action[0][1]['bikesCapacity']))) {
                this.objectShape({ addObject, property: 'bikesCapacity', status: true, message: 'La capacidad debe ser de tipo númerico' });
            };

            let openingTime = this.globalService.defineTimeFormat(action[0][1]['openingTime']);
            let closingTime = this.globalService.defineTimeFormat(action[0][1]['closingTime']);

            if (openingTime && closingTime && openingTime > closingTime) {
                this.objectShape({ addObject, property: 'closingTime', status: true, message: 'La hora final debe ser mayor a la hora de inicio' });
            };

            objectsKey.forEach(property => {
                if (action[0][1][property] == "" && property != 'state') {
                    this.objectShape({ addObject, property, status: true, message: `El campo es requerido` });
                };
            });

            delete addObject.electricBikes
            delete addObject.mechanicBikes
            if (Object.keys(addObject).length == 0 && action[1]['status']) {
                return [
                    new updateStationTable(),
                    new setStationFormErrors({ stationFormErrors: addObject })
                ];
            } else {
                return [new setStationFormErrors({ stationFormErrors: addObject })];
            };
        })
    );

    //orga_f_B
    @Effect()
    organizationFormValidation$ = this.actions$.pipe(
        ofType<organizationFormValidation>(ActionTypes.organizationFormValidation),
        withLatestFrom(this.store$.select((state: State) => state.apiData.organizationLogo)),
        withLatestFrom(this.store$.select((state: State) => state.user.modalOrganizationType)),
        exhaustMap((action) => {
            let addObject: any = new Object();
            const objectsKey: any = Object.keys(action[0][0].payload.modalOrganization);

            let errorLinkAppstore: any = validate({ website: action[0][0].payload.modalOrganization.linkAppstore }, { website: { url: true } });
            let errorLinkPlaystore: any = validate({ website: action[0][0].payload.modalOrganization.linkPlaystore }, { website: { url: true } });
            errorLinkAppstore ? this.objectShape({ addObject, property: 'linkAppstore', status: true, message: `No es una URL válida` }) : null;
            errorLinkPlaystore ? this.objectShape({ addObject, property: 'linkPlaystore', status: true, message: `No es una URL válida` }) : null;
            
            action[0][0].payload.modalOrganization.name.length <= 6 ? this.objectShape({ addObject, property: 'name', status: true, message: `El nombre debe tener más de 6 caracteres` }) : null;
            objectsKey.forEach(property => {
                !action[0][0].payload.modalOrganization[property] ? this.objectShape({ addObject, property, status: true, message: `El campo es requerido` }) : null;
            });
            if (action[0][1]['shouldLoad'] || action[1] == 'add') {
                !action[0][1]['logoInfo'] ? this.objectShape({ addObject, property: 'logo', status: true, message: `Debe agregar una imagen` }) : null
            }
            if (action[0][0].payload.shouldSubmit == 'submit' && Object.keys(addObject).length == 0) {
                return [
                    new uploadOrganizationLogo({ modalOrganization: action[0][0].payload.modalOrganization }),
                ];
            } else {
                return [new setOrganizationFormErrors({ organizationFormErrors: addObject })];
            };
        })
    )

    @Effect() // trip_f_C
    tripFormValidation$ = this.actions$.pipe(
        ofType<tripFormValidation>(ActionTypes.tripFormValidation),
        withLatestFrom(this.store$.select((state: State) => state.user.searchTripsForm)),
        map((action) => {
            let addObject: any = new Object();
            const objectsKey: any = Object.keys(action[1]);

            let endDate = action[1].endDate ? new Date(action[1].endDate) : ''
            let startDate = action[1].startDate ? new Date(action[1].startDate) : ''

            if (endDate && startDate) {
                console.log('error>',addObject)
                if (Date.parse(endDate.toString()) < Date.parse(startDate.toString())) {
                    this.objectShape({ addObject, property: 'endDate', status: true, message: 'La fecha final es menor que la inicial' });
                }
            }
            if (action[1].idNumber) {
                if (action[1].idNumber.length < 6) {
                    this.objectShape({ addObject, property: 'idNumber', status: true, message: 'Debe ingresar más de 6 caracteres' });
                }
            }
            
            const voidForm = objectsKey.find(property => action[1][property] != "");
            if(!voidForm){
                this.objectShape({ addObject, property: 'voidForm', status: true, message: 'Debe agregar algo para buscar' });
            }

            if (Object.keys(addObject).length == 0 && action[0].payload.searchTripsForm == 'submit') {
                console.log('-buscar-')
                return new buildSearchTripsFilter({ searchTripsForm: action[1] })
            } else {
                return new setTripsFormErrors({ tripsFormErrors: addObject })
            };

        })
    );

    constructor(
        private validation: ValidationService,
        private globalService: GlobalService,
        private actions$: Actions,
        private apiService: ApiService,
        private store$: Store<any>,
        private router: Router,
        private dialog: MatDialog
    ) { }
}
