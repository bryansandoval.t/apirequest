import { Action } from '@ngrx/store';
import { User } from 'src/app/models/user.model';
import { FormModel, DispatchPosibleActions } from '../interfaces/form.interfaces';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertIcon } from 'sweetalert2';
import { UserInfos } from '../models/userInfos.model';


export enum ActionTypes {
  //bikes
  getTotalBikes = "[user] getTotalBikes",
  getBikes = "[user] getBikes",
  setBikes = "[user] setBikes",
  stations = "[user] stations",
  getMasterListTypes = "[user] getMasterListTypes",
  saveTypesBikes = "[user] saveTypesBikes",
  saveStateBikes = "[user] saveStateBikes",
  getMasterListState = "[user] getMasterListState",
  modalTypeBike = "[user] modalTypeBike",
  searchBikeForm = "[user] searchBikeForm",
  setCurrentPageBike = "[user] setCurrentPageBike",
  modalBike = "[user] modalBike",
  selectedLock = "[user] selectedLock",
  totalLockBikes = "[user] totalLockBikes",
  postBike = "[user] postBike",
  patchLockBike = "[user] patchLockBike",
  getLocksBikes = "[user] getLocksBikes",
  saveLocksBikes = "[user] saveLocksBikes",
  getCountLocksBikes = "[user] getCountLocksBikes",
  totalBikes = "[user] totalBikes",
  setCurrentPageLocksBike = "[user] setCurrentPageLocksBike",
  searchLocksBikesForm = "[user] searchLocksBikesForm",
  showSearchPageBike = "[user] showSearchPageBike",
  showSearchPageLocksBike = "[user] showSearchPageLocksBike",
  locksBikesSearchResult = "[user] locksBikesSearchResult",
  bikesSearchResult = "[user] bikesSearchResult",
  setBikesSearch = "[user] setBikesSearch",
  showPageBikes = "[user] showPageBikes",

  // Organizations
  //orga_a_A
  setTableOrganization = "[user] setTableOrganization",
  //orga_a_C
  setOrganizationFormErrors = "[user] setOrganizationFormErrors",
  //orga_a_D
  organizationFormValidation = "[user] organizationFormValidation",
  //orga_a_E
  postOrganization = "[user] postOrganization",
  //orga_a_F
  putOrganization = "[user] putOrganization",
  //orga_a_G
  getOrganizations = "[user] getOrganizations",
  //orga_a_H
  setModalOrganization = "[user] setModalOrganization",
  //orga_a_I
  setModalOrganizationType = "[user] setModalOrganizationType",
  //orga_a_J
  setOrganizationLogo = "[user] setOrganizationLogo",
  uploadOrganizationLogo = "[user] uploadOrganizationLogo",
  setCloseModalOrganization = "[user] setCloseModalOrganization",

  getMasterListTypesBikes = "[user] getMasterListTypesBikes",
  getMasterListStateBikes = "[user] getMasterListStateBikes",
  showPageLocksBikes = "[user] showPageLocksBikes",
  bikePageSize = "[user] bikePageSize",
  skipPageBike = "[user] skipPageBike",
  skipPageLocks = "[user] skipPageLocks",
  getLocksSearch = "[user] getLocksSearch",
  filterSearchLocksBikes = "[user] filterSearchLocksBikes",
  getStationsBikes = "[user] getStationsBikes",
  saveStationsBikes = "[user] saveStationsBikes",
  patchBike = "[user] patchBike",
  decisionPatchLockBike = "[user] decisionPatchLockBike",
  patchDeleteLockBike = "[user] patchDeleteLockBike",
  getModalLocksSearch = "[user] getModalLocksSearch",
  //global
  validateEmailPassword = '[global] validateEmailPassword',
  setPasswordError = '[global] setPasswordError',
  clearValidationError = '[global] clearValidationError',
  ApiError = '[global] ApiError',
  partialValidationForm = '[global] partialValidationForm',
  saveApiData = '[global] saveApiData',
  UpdateData = '[global] UpdateData',
  saveData = '[global] saveData',
  getTableData = '[global] getTableData',
  getTableDataLength = '[global] getTableDataLength',
  closeDialog = '[global] closeDialog',
  deletedAt = '[global] deletedAt',
  showSweetAlert = "[global] showSweetAlert",
  htmlSweetAlert = "[global] htmlSweetAlert",
  logOutUser = "[global] logOutUser",

  saveStationsInfo = "[global] saveStationsInfo",
  setStationFormErrors = "[global] setStationFormErrors",
  setMosalStationInfo = "[global] setMosalStationInfo",
  saveStationSearch = "[global] saveStationSearch",
  updateStationTable = "[global] updateStationTable",
  stationFormValidation = "[global] stationFormValidation",
  getStations = "[global] getStations",
  //trips
  loadTripStates = "[global] loadTripStates",
  setTripStates = "[global] setTripStates",
  getTotalTrips = "[global] getTotalTrips",
  setTotalTrips = "[global] setTotalTrips",
  setCurrentPageTrips = "[global] setCurrentPageTrips",
  setSearchTripsForm = "[global] setSearchTripsForm",
  showPageTrips = "[global] showPageTrips",
  showSearchPageTrips = "[global] showSearchPageTrips",
  showGlobalPageTrips = "[global] showGlobalPageTrips",
  searchTrips = "[global] searchTrips",
  setTripsSearchResult = "[global] setTripsSearchResult",
  patchEndTrip = "[global] patchEndTrip",
  setTripsFormErrors = "[global] setTripsFormErrors",
  tripFormValidation = "[global] tripFormValidation",
  setModalTrip = "[global] setModalTrip",
  buildSearchFilter = "[global] buildSearchFilter",
  setTripsPageNumber = "[global] setTripsPageNumber",
  buildSearchTripsFilter = "[global] buildSearchTripsFilter",
  setTripsFormStatus = "[global] setTripsFormStatus",
  setLoadingTripsPage = "[global] setLoadingTripsPage",
  setCloseTripsModal = "[global] setCloseTripsModal",
  getReportTrips = "[global] getReportTrips",
  generateDownloadTrips = "[global] generateDownloadTrips",


  //user
  addUser = '[user] addUser',
  addUserInfo = '[user] addUserInfo',
  modifyUser = '[user] modifyUser',
  modifyUserInfo = '[user] modifyUserInfo',
  deleteUser = '[user] deleteUser',
  deleteUserInfo = '[user] deleteUserInfo',
  refreshUserTable = '[user] refreshUserTable',
  activeUser = '[user] activeUser',
  serverError = '[user] serverError',

  //user-finalUser
  saveUsers = '[user]saveUser',
  modalUser = '[user]modalUser',
  saveUserTrips = '[user]saveUserTrips',
  calculationDates = '[user]calculationDates',
  postPenaltys = '[user]postPenaltys',
  patchUser = '[user]patchUser',
  penaltyDates = '[user]penaltyDates',
  setSearchTerm = '[user]setSearchTerm',
  showPage = '[user]showPage',
  totalGetUsers = '[user]totalGetUsers',
  totalGetTrip = '[user]totalGetTrip',
  totalSearchTerm = '[user]totalSearchTerm',
  getPenaltyTypes = '[user]getPenaltyTypes',
  penaltyTypes = '[user]penaltyTypes',
  userPagination = '[user]userPagination',
  userPaginationTrip = '[user]userPaginationTrip',
  paginatorFinalUser = '[user]paginatorFinalUser',
  showPageSearch = '[user]showPageSearch',
  searchTerm = '[user]searchTerm',
  searchTermTrip = '[user]searchTermTrip',
  paginatorFinalUserSearch = '[user]paginatorFinalUserSearch',
  datasourceSearch = '[user]datasourceSearch',
  showPageTrip = '[user]showPageTrip',
  paginatorFinalUserTrip = '[user]paginatorFinalUserTrip',
  showPageSearchTrip = '[user]showPageSearchTrip',
  setSearchTermTrip = '[user]setSearchTermTrip',
  datasourceSearchTrip = '[user]datasourceSearchTrip',
  paginatorFinalUserSearchTrip = '[user]paginatorFinalUserSearchTrip',
  getStationsTrip = '[user]getStationsTrip',
  stationsTrips = '[user]stationsTrips',
  setLoadingDownload = '[user] setLoadingDownload',
  getReportUsers = '[user] getReportUsers',
  generateDownload = '[user] generateDownload',


  //auth
  loginUser = "[auth] loginUser",
  refreshTokenInit = "[auth] refreshTokenInit",
  refreshToken = "[auth] refreshToken",
  getUserData = "[auth] getUserData",
  getPermissions = "[auth] getPermissions",
  validateUser = "[auth] validateUser",
  setPermissions = "[auth] setPermissions",


  //admins
  setListOrganizations = "[user] setListOrganizations",
  setListRoles = "[user] setListRoles",
  setCurrentPageAdmins = "[user] setCurrentPageAdmins",
  setModalAdminSystems = "[user] setModalAdminSystems",
  setAdminSystemsFormErrors = "[user] setAdminSystemsFormErrors",
  setTotalUsersAdmins = "[user] setTotalUsersAdmins",
  setAdminSystemsFormStatus = "[user] setAdminSystemsFormStatus",
  setLoadingAdminSystemsPage = "[user] setLoadingAdminSystemsPage",
  setCloseAdminSystemsModal = "[user] setCloseAdminSystemsModal",
  setInfoFormAdmin = "[user] setInfoFormAdmin",
  setAdminFormValidation = "[user] setAdminFormValidation",
  setMasterList = "[user] setMasterList",
  showPageAdmin = "[user] showPageAdmin",
  getListOrganizations = "[user] getListOrganizations",
  getListRoles = "[user] getListRoles",
  getTotalUsersAdmins = "[user] getTotalUsersAdmins",
  getUsersAdmins = "[user] getUsersAdmins",
  showSearchPageAdmins = "[user] showSearchPageAdmins",
  adminsFormValidation = "[user] adminsFormValidation",
  updateAdminUser = "[user] updateAdminUser",
  createAdminUser = "[user] createAdminUser",
  postUserRol = "[user] postUserRol",
  patchUserRol = "[user] patchUserRol",
  changePasswordAdminUser = "[user] changePasswordAdminUser",
  getMasterList = "[user] getMasterList",
  showGlobalPageAdmins = "[user] showGlobalPageAdmins",


  //lock_
  setLoadingLockTable = "[user] setLoadingLockTable",
  setTotalLocks = "[user] setTotalLocks",
  setCurrentPageLock = "[user] setCurrentPageLock",
  setSelectedLockInfo = "[user] setSelectedLockInfo",
  setLockPageNumber = "[user] setLockPageNumber",
  setCurrentPageLockCommands = "[user] setCurrentPageLockCommands",
  setLoadingCommandsTable = "[user] setLoadingCommandsTable",
  setLockPageSize = "[user] setLockPageSize",
  rolUserLocks = "[user] rolUserLocks",
  setLocksSearchParams = "[user] setLocksSearchParams",
  setLocksSearchResult = "[user] setLocksSearchResult",
  setCommandsPageSize = "[user] setCommandsPageSize",
  setTotalLockCommands = "[user] setTotalLockCommands",
  setEditLockFormErrors = "[user] setEditLockFormErrors",
  getTotalLocks = "[user] getTotalLocks",
  showGlobalPageLocks = "[user] showGlobalPageLocks",
  showSearchPageLocks = "[user] showSearchPageLocks",
  showPageLocks = "[user] showPageLocks",
  getLockCommands = "[user] getLockCommands",
  changeLockPageSize = "[user] changeLockPageSize",
  searchBikesByNumber = "[user] searchBikesByNumber",
  searchLocks = "[user] searchLocks",
  getTotalLockCommands = "[user] getTotalLockCommands",
  showPageLockCommands = "[user] showPageLockCommands",
  setLockCommandsPageNumber = "[user] setLockCommandsPageNumber",
  validateEditLockForm = "[user] validateEditLockForm",
  validateUniqQrMac = "[user] validateUniqQrMac",
  patchLock = "[user] patchLock",
  setCloseModalLocks = "[user] setCloseModalLocks",
  validateLockOrganization = "[user] validateLockOrganization",
  //Support
  setLoadingTicketsTable = "[global] setLoadingTicketsTable",
  setTotalTickets = "[global] setTotalTickets",
  setCurrentPageTickets = "[global] setCurrentPageTickets",
  setSelectedTicketInfo = "[global] setSelectedTicketInfo",
  setTicketPageNumber = "[global] setTicketPageNumber",
  setSearchWordTickets = "[global] setSearchWordTickets",
  setMasterListTicket = "[global] setMasterListTicket",
  getTicketsPage = "[global] getTicketsPage",
  getTotalTickets = "[global] getTotalTickets",
  getMasterListTicket = "[global] getMasterListTicket",
  getStationsTicket = "[global] getStationsTicket",
  patchTicket = "[global] patchTicket",
  setTicketPageSize = "[global] setTicketPageSize",
  setTicketSearchStatus = "[global] setTicketSearchStatus",
  searchTickets = "[global] searchTickets",
  setTicketsSearchResult = "[global] setTicketsSearchResult",
  showSearchPageTickets = "[global] showSearchPageTickets",
  showPageTicket = "[global] showPageTicket",
  setCloseModalTickets = "[global] setCloseModalTickets",

  // Map
  setMapStationsSearchParams = "[global] setMapStationsSearchParams"
}

export class UpdateData implements Action {
  readonly type = ActionTypes.UpdateData;
  constructor(public payload: {
    data: any, filter: any, concatResult: boolean, replaceResult?: boolean, storeState: { store: string, storeState: string }
  }
  ) { }
}

export class setMapStationsSearchParams implements Action {
  readonly type = ActionTypes.setMapStationsSearchParams;
  constructor(public payload: { mapStationsSearchParams: Object }) { }
}
export class validateLockOrganization implements Action {
  readonly type = ActionTypes.validateLockOrganization;
  constructor(public payload: { lockPatch: any }) { }
}
export class setCloseModalLocks implements Action {
  readonly type = ActionTypes.setCloseModalLocks;
  constructor(public payload: { closeModalLocks: boolean }) { }
}
export class validateEditLockForm implements Action {
  readonly type = ActionTypes.validateEditLockForm;
  constructor(public payload: { lockPatch: any }) { }
}
export class patchLock implements Action {
  readonly type = ActionTypes.patchLock;
  constructor(public payload: { lockPatch: any }) { }
}

export class validateUniqQrMac implements Action {
  readonly type = ActionTypes.validateUniqQrMac;
  constructor(public payload: { lockPatch: any }) { }
}
export class getLockCommands implements Action {
  readonly type = ActionTypes.getLockCommands;
}
export class showPageLocks implements Action {
  readonly type = ActionTypes.showPageLocks;
  constructor(public payload: { lockPageNumber: number }) { }
}
export class showSearchPageLocks implements Action {
  readonly type = ActionTypes.showSearchPageLocks;
  constructor(public payload: { lockPageNumber: number }) { }
}
export class showGlobalPageLocks implements Action {
  readonly type = ActionTypes.showGlobalPageLocks;
  constructor(public payload: { lockPageNumber: number }) { }
}
export class getTotalLocks implements Action {
  readonly type = ActionTypes.getTotalLocks;
}
export class changeLockPageSize implements Action {
  readonly type = ActionTypes.changeLockPageSize;
  constructor(public payload: { pageLock: any }) { }
}
export class searchBikesByNumber implements Action {
  readonly type = ActionTypes.searchBikesByNumber;
  constructor(public payload: { searchTerm: any }) { }
}
export class searchLocks implements Action {
  readonly type = ActionTypes.searchLocks;
  constructor(public payload: { searchTerm: any, bikesIds: any }) { }
}
export class getTotalLockCommands implements Action {
  readonly type = ActionTypes.getTotalLockCommands;
}
export class showPageLockCommands implements Action {
  readonly type = ActionTypes.showPageLockCommands;
  constructor(public payload: { lockCommandsPageNumber: any }) { }
}
export class setLockCommandsPageNumber implements Action {
  readonly type = ActionTypes.setLockCommandsPageNumber;
  constructor(public payload: { lockCommandsPageNumber: number }) { }
}
export class setEditLockFormErrors implements Action {
  readonly type = ActionTypes.setEditLockFormErrors;
  constructor(public payload: { editLockFormErrors: Object }) { }
}
export class setTotalLockCommands implements Action {
  readonly type = ActionTypes.setTotalLockCommands;
  constructor(public payload: { totalLockCommands: number }) { }
}
export class setCommandsPageSize implements Action {
  readonly type = ActionTypes.setCommandsPageSize;
  constructor(public payload: { commandsPageSize: number }) { }
}
export class setLocksSearchResult implements Action {
  readonly type = ActionTypes.setLocksSearchResult;
  constructor(public payload: { locksSearchResult: Array<any> }) { }
}
export class setLocksSearchParams implements Action {
  readonly type = ActionTypes.setLocksSearchParams;
  constructor(public payload: {
    locksSearchParams: {
      searchTerm: string,
      isSearching: boolean,
      locksWithoutBikeState: boolean
    }
  }) { }
}
export class setLoadingCommandsTable implements Action {
  readonly type = ActionTypes.setLoadingCommandsTable;
  constructor(public payload: { loadingCommandsTable: boolean }) { }
}
export class setLockPageSize implements Action {
  readonly type = ActionTypes.setLockPageSize;
  constructor(public payload: { lockPageSize: number }) { }
}
export class rolUserLocks implements Action {
  readonly type = ActionTypes.rolUserLocks;
  constructor(public payload: { rol: any }) { }
}
export class setCurrentPageLockCommands implements Action {
  readonly type = ActionTypes.setCurrentPageLockCommands;
  constructor(public payload: { currentPageLockCommands: Array<any> }) { }
}
export class setLoadingLockTable implements Action {
  readonly type = ActionTypes.setLoadingLockTable;
  constructor(public payload: { loadingLockTable: boolean }) { }
}
export class setLockPageNumber implements Action {
  readonly type = ActionTypes.setLockPageNumber;
  constructor(public payload: { lockPageNumber: number }) { }
}
export class setSelectedLockInfo implements Action {
  readonly type = ActionTypes.setSelectedLockInfo;
  constructor(public payload: { selectedLockInfo: {} }) { }
}
export class setCurrentPageLock implements Action {
  readonly type = ActionTypes.setCurrentPageLock;
  constructor(public payload: { currentPageLocks: Array<any> }) { }
}
export class setTotalLocks implements Action {
  readonly type = ActionTypes.setTotalLocks;
  constructor(public payload: { totalLocks: number }) { }
}

export class validateEmailPassword implements Action {
  readonly type = ActionTypes.validateEmailPassword;
  constructor(public payload: { form, formModel: FormModel, dispatchFormAction?: DispatchPosibleActions }) { }
}
export class getUserData implements Action {
  readonly type = ActionTypes.getUserData;
  constructor(public payload: { userInfo: any, token: string }) { }
}
export class setCloseModalTickets implements Action {
  readonly type = ActionTypes.setCloseModalTickets;
  constructor(public payload: { closeModalTickets: boolean }) { }
}
export class setTripsFormStatus implements Action {
  readonly type = ActionTypes.setTripsFormStatus;
  constructor(public payload: { tripsFormStatus: boolean }) { }
}
export class getPermissions implements Action {
  readonly type = ActionTypes.getPermissions;
  constructor(public payload: { roleId: string }) { }
}

export class validateUser implements Action {
  readonly type = ActionTypes.validateUser;
  constructor(public payload: { user: any, roles: any, token: string }) { }
}

export class setPasswordError implements Action {
  readonly type = ActionTypes.setPasswordError;
  constructor(public payload: { startupErrors: any, whereErrorsSave: string }) { }
}

export class clearValidationError implements Action {
  readonly type = ActionTypes.clearValidationError;
  constructor(public payload: { errorsToClear: Array<any>, whereErrorsSave: string }) { }
}

export class partialValidationForm implements Action {
  readonly type = ActionTypes.partialValidationForm;
  constructor(public payload: { form, formModel: FormModel, input: string, dispatchFormAction?: DispatchPosibleActions }) { }
}

export class ApiError implements Action {
  readonly type = ActionTypes.ApiError;
  constructor(public payload: { error: HttpErrorResponse }) { }
}

export class saveApiData implements Action {
  readonly type = ActionTypes.saveApiData;
  constructor(public payload: { data: any, dataName: string }) { }
}

export class saveData implements Action {
  readonly type = ActionTypes.saveData;
  constructor(public payload: { data: any, dataName: string }) { }
}
export class getTableData implements Action {
  readonly type = ActionTypes.getTableData;
  constructor(public payload: {
    users?: any, data: any, filter: any, concatResult: boolean, replaceResult?: boolean, storeState: { store: string, storeState: string }
  }
  ) { }
}
export class getTableDataLength implements Action {
  readonly type = ActionTypes.getTableDataLength;
  constructor(public payload: { data: any, filter: any, storeState: { store: string, storeState: string } }) { }
}

export class closeDialog implements Action {
  readonly type = ActionTypes.closeDialog;
}

export class deletedAt implements Action {
  readonly type = ActionTypes.deletedAt;
  constructor(public payload: { data: any, form: any }) { }
}

export class showSweetAlert implements Action {
  readonly type = ActionTypes.showSweetAlert;
  constructor(public payload: { title: string, text: string, type: SweetAlertIcon, buttonText: string }) { }
}
export class saveStationsInfo implements Action {
  readonly type = ActionTypes.saveStationsInfo;
  constructor(public payload: { tableStations: Array<any> }) { }
}
export class setStationFormErrors implements Action {
  readonly type = ActionTypes.setStationFormErrors;
  constructor(public payload: { stationFormErrors: Object }) { }
}
export class setMosalStationInfo implements Action {
  readonly type = ActionTypes.setMosalStationInfo;
  constructor(public payload: { modalStationInfo: Object, stationModalType: Object }) { }
}
export class saveStationSearch implements Action {
  readonly type = ActionTypes.saveStationSearch;
  constructor(public payload: { searchStation: string }) { }
}
export class updateStationTable implements Action {
  readonly type = ActionTypes.updateStationTable;
}
export class stationFormValidation implements Action {
  readonly type = ActionTypes.stationFormValidation;
}
export class getStations implements Action {
  readonly type = ActionTypes.getStations;
}

export class htmlSweetAlert implements Action {
  readonly type = ActionTypes.htmlSweetAlert;
  constructor(public payload: { html: string }) { }
}

export class logOutUser implements Action {
  readonly type = ActionTypes.logOutUser;
}

//User
export class AddUser implements Action {
  readonly type = ActionTypes.addUser;
  constructor(public payload: { user: User, userInfos: UserInfos }) { }
}

export class AddUserInfo implements Action {
  readonly type = ActionTypes.addUserInfo;
  constructor(public payload: { data: any, userInfos: UserInfos, name: string }) { }
}

export class ModifyUser implements Action {
  readonly type = ActionTypes.modifyUser;
  constructor(public payload: { user: User, userInfo: UserInfos, route: { store: string, storeState: string } }) { }
}

export class ModifyUserInfo implements Action {
  readonly type = ActionTypes.modifyUserInfo;
  constructor(public payload: { userInfo: UserInfos, route: { store: string, storeState: string } }) { }
}

export class DeleteUser implements Action {
  readonly type = ActionTypes.deleteUser;
  constructor(public payload: { user: User, userId: string, userInfoId: string }) { }
}

export class ActiveUser implements Action {
  readonly type = ActionTypes.activeUser;
  constructor(public payload: { user: User, userInfo: UserInfos }) { }
}

export class DeleteUserInfo implements Action {
  readonly type = ActionTypes.deleteUserInfo;
  constructor(public payload: { userInfoId: string }) { }
}

export class RefreshUserTable implements Action {
  readonly type = ActionTypes.refreshUserTable;
  constructor(public payload: { data: { store: any, storeState: any } }) { }
}

export class serverError implements Action {
  readonly type = ActionTypes.serverError;
  constructor(public payload: { errorMessage: string }) { }
}
export class setPermissions implements Action {
  readonly type = ActionTypes.setPermissions;
  constructor(public payload: { permissions: string }) { }
}
export class getReportTrips implements Action {
  readonly type = ActionTypes.getReportTrips;
  constructor(public payload: { dateRangeTrips: any }) { }
}
export class generateDownloadTrips implements Action {
  readonly type = ActionTypes.generateDownloadTrips;
  constructor(public payload: { dateRangeTrips: any }) { }
}

// Actions support(tickets) start
export class setLoadingTicketsTable implements Action {
  readonly type = ActionTypes.setLoadingTicketsTable;
  constructor(public payload: { loadingTicketsTable: boolean }) { }
}
export class setTotalTickets implements Action {
  readonly type = ActionTypes.setTotalTickets;
  constructor(public payload: { totalTickets: number }) { }
}
export class setTicketPageSize implements Action {
  readonly type = ActionTypes.setTicketPageSize;
  constructor(public payload: { ticketPageSIze: { limit: number, pageNumber: number } }) { }
}
export class getTotalTickets implements Action {
  readonly type = ActionTypes.getTotalTickets;
}
export class getMasterListTicket implements Action {
  readonly type = ActionTypes.getMasterListTicket;
}
export class setCurrentPageTickets implements Action {
  readonly type = ActionTypes.setCurrentPageTickets;
  constructor(public payload: { currentPageTickets: Array<any> }) { }
}
export class setTicketSearchStatus implements Action {
  readonly type = ActionTypes.setTicketSearchStatus;
  constructor(public payload: { ticketSearchStatus: boolean }) { }
}
export class getTicketsPage implements Action {
  readonly type = ActionTypes.getTicketsPage;
}
export class setMasterListTicket implements Action {
  readonly type = ActionTypes.setMasterListTicket;
  constructor(public payload: { masterListTicket: Array<any> }) { }
}
export class showPageTicket implements Action {
  readonly type = ActionTypes.showPageTicket;
  constructor(public payload: { skip: number }) { }
}
export class getStationsTicket implements Action {
  readonly type = ActionTypes.getStationsTicket;
}
export class showSearchPageTickets implements Action {
  readonly type = ActionTypes.showSearchPageTickets;
}
export class setTicketsSearchResult implements Action {
  readonly type = ActionTypes.setTicketsSearchResult;
  constructor(public payload: { ticketsSearchResult: Array<any> }) { }
}
export class patchTicket implements Action {
  readonly type = ActionTypes.patchTicket;
  constructor(public payload: { state: string }) { }
}
export class setSearchWordTickets implements Action {
  readonly type = ActionTypes.setSearchWordTickets;
  constructor(public payload: { searchWordTickets: string }) { }
}
export class searchTickets implements Action {
  readonly type = ActionTypes.searchTickets;
}
export class setTicketPageNumber implements Action {
  readonly type = ActionTypes.setTicketPageNumber;
  constructor(public payload: { ticketPageNumber: number }) { }
}
export class setSelectedTicketInfo implements Action {
  readonly type = ActionTypes.setSelectedTicketInfo;
  constructor(public payload: { selectedTicketInfo: Object }) { }
}
// Actions support(tickets) end


//auth
export class loginUser implements Action {
  readonly type = ActionTypes.loginUser
  constructor(public payload: { form: any, withoutRouting?: boolean }) { }
}

export class refreshTokenInit implements Action {
  readonly type = ActionTypes.refreshTokenInit;
}

export class refreshToken implements Action {
  readonly type = ActionTypes.refreshToken;
}
export class saveUsers implements Action {
  readonly type = ActionTypes.saveUsers
  constructor(public payload: { users: any }) { }
}

export class modalUser implements Action {
  readonly type = ActionTypes.modalUser
  constructor(public payload: { user: any }) { }
}

export class saveUserTrips implements Action {
  readonly type = ActionTypes.saveUserTrips
  constructor(public payload: { trips: any }) { }
}

export class calculationDates implements Action {
  readonly type = ActionTypes.calculationDates
  constructor(public payload: { penaltyType: any }) { }
}

export class postPenaltys implements Action {
  readonly type = ActionTypes.postPenaltys
  constructor(public payload: { penalty: any }) { }
}

export class patchUser implements Action {
  readonly type = ActionTypes.patchUser
  constructor(public payload: { stateUser: any }) { }
}

export class penaltyDates implements Action {
  readonly type = ActionTypes.penaltyDates
  constructor(public payload: { dates: any }) { }
}

export class setSearchTerm implements Action {
  readonly type = ActionTypes.setSearchTerm
  constructor(public payload: { resultSearch: any }) { }
}

export class setSearchTermTrip implements Action {
  readonly type = ActionTypes.setSearchTermTrip
  constructor(public payload: { resultSearch: any }) { }
}

export class showPage implements Action {
  readonly type = ActionTypes.showPage
  constructor(public payload: { currentPage: any }) { }
}

export class showPageTrip implements Action {
  readonly type = ActionTypes.showPageTrip
  constructor(public payload: { currentPage: any }) { }
}

export class totalGetUsers implements Action {
  readonly type = ActionTypes.totalGetUsers
}

export class totalGetTrip implements Action {
  readonly type = ActionTypes.totalGetTrip
  constructor(public payload: { userId: any }) { }
}

export class totalSearchTerm implements Action {
  readonly type = ActionTypes.totalSearchTerm
  constructor(public payload: { term: any }) { }
}
export class getPenaltyTypes implements Action {
  readonly type = ActionTypes.getPenaltyTypes
}

export class penaltyTypes implements Action {
  readonly type = ActionTypes.penaltyTypes
  constructor(public payload: { penaltys: any }) { }
}

export class userPagination implements Action {
  readonly type = ActionTypes.userPagination
  constructor(public payload: { pagination: any }) { }
}
export class userPaginationTrip implements Action {
  readonly type = ActionTypes.userPaginationTrip
  constructor(public payload: { pagination: any }) { }
}

export class paginatorFinalUser implements Action {
  readonly type = ActionTypes.paginatorFinalUser
  constructor(public payload: { direction: any }) { }
}
export class paginatorFinalUserSearch implements Action {
  readonly type = ActionTypes.paginatorFinalUserSearch
  constructor(public payload: { direction: any }) { }
}

export class showPageSearch implements Action {
  readonly type = ActionTypes.showPageSearch
  constructor(public payload: { termSearch: any }) { }
}
export class searchTerm implements Action {
  readonly type = ActionTypes.searchTerm
  constructor(public payload: { term: any }) { }
}
export class searchTermTrip implements Action {
  readonly type = ActionTypes.searchTermTrip
  constructor(public payload: { term: any }) { }
}

export class datasourceSearch implements Action {
  readonly type = ActionTypes.datasourceSearch
  constructor(public payload: { sliceSearch: any }) { }
}

export class datasourceSearchTrip implements Action {
  readonly type = ActionTypes.datasourceSearchTrip
  constructor(public payload: { sliceSearch: any }) { }
}
export class paginatorFinalUserTrip implements Action {
  readonly type = ActionTypes.paginatorFinalUserTrip
  constructor(public payload: { direction: any }) { }
}

export class showPageSearchTrip implements Action {
  readonly type = ActionTypes.showPageSearchTrip
  constructor(public payload: { termSearch: any }) { }
}

export class paginatorFinalUserSearchTrip implements Action {
  readonly type = ActionTypes.paginatorFinalUserSearchTrip
  constructor(public payload: { direction: any }) { }
}

export class getStationsTrip implements Action {
  readonly type = ActionTypes.getStationsTrip
}

export class stationsTrips implements Action {
  readonly type = ActionTypes.stationsTrips
  constructor(public payload: { stations: any }) { }
}
export class setLoadingDownload implements Action {
  readonly type = ActionTypes.setLoadingDownload
  constructor(public payload: { loadingDownload: boolean }) { }
}
export class generateDownload implements Action {
  readonly type = ActionTypes.generateDownload
  constructor(public payload: { dateRangeUsers: any }) { }
}
export class getReportUsers implements Action {
  readonly type = ActionTypes.getReportUsers
  constructor(public payload: { dateRangeUsers: { startDate: any, endDate: any } }) { }
}

//trip_a_C
export class loadTripStates implements Action {
  readonly type = ActionTypes.loadTripStates
}

//trip_a_D
export class setTripStates implements Action {
  readonly type = ActionTypes.setTripStates
  constructor(public payload: { tripState: any }) { }
}

//trip_a_E
export class getTotalTrips implements Action {
  readonly type = ActionTypes.getTotalTrips
}

//trip_a_M
export class setTotalTrips implements Action {
  readonly type = ActionTypes.setTotalTrips
  constructor(public payload: { totalTrips: any }) { }
}

//trip_a_F
export class setCurrentPageTrips implements Action {
  readonly type = ActionTypes.setCurrentPageTrips
  constructor(public payload: { currentPageTrips: any }) { }
}

//trip_a_H
export class setSearchTripsForm implements Action {
  readonly type = ActionTypes.setSearchTripsForm
  constructor(public payload: { searchTripsForm: any }) { }
}

//trip_a_G
export class showPageTrips implements Action {
  readonly type = ActionTypes.showPageTrips
  constructor(public payload: { tripsPageNumber: any }) { }
}

//trip_a_I
export class showSearchPageTrips implements Action {
  readonly type = ActionTypes.showSearchPageTrips
  constructor(public payload: { tripsPageNumber: any }) { }
}

//trip_a_J
export class showGlobalPageTrips implements Action {
  readonly type = ActionTypes.showGlobalPageTrips
  constructor(public payload: { tripsPageNumber: any }) { }
}

//trip_a_K
export class searchTrips implements Action {
  readonly type = ActionTypes.searchTrips
  constructor(public payload: { filter: any }) { }
}

//trip_a_L
export class setTripsSearchResult implements Action {
  readonly type = ActionTypes.setTripsSearchResult
  constructor(public payload: { tripsSearchResult: any }) { }
}

//trip_a_Ñ
export class patchEndTrip implements Action {
  readonly type = ActionTypes.patchEndTrip
}

//trip_a_O
export class setTripsFormErrors implements Action {
  readonly type = ActionTypes.setTripsFormErrors
  constructor(public payload: { tripsFormErrors: any }) { }
}

//trip_a_P
export class tripFormValidation implements Action {
  readonly type = ActionTypes.tripFormValidation
  constructor(public payload: { searchTripsForm: any }) { }
}

//trip_a_Q
export class setModalTrip implements Action {
  readonly type = ActionTypes.setModalTrip
  constructor(public payload: { modalTrip: any }) { }
}

//trip_a_R
export class buildSearchFilter implements Action {
  readonly type = ActionTypes.buildSearchFilter
  constructor(public payload: { searchTripsForm: any }) { }
}

//trip_a_S
export class setTripsPageNumber implements Action {
  readonly type = ActionTypes.setTripsPageNumber
  constructor(public payload: { tripsPageNumber: any }) { }
}

//trip_a_R
export class buildSearchTripsFilter implements Action {
  readonly type = ActionTypes.buildSearchTripsFilter
  constructor(public payload: { searchTripsForm: any }) { }
}
//trip_a_P
export class setLoadingTripsPage implements Action {
  readonly type = ActionTypes.setLoadingTripsPage
  constructor(public payload: { loadingTripsPage: boolean }) { }
}
//trip_a_Q
export class setCloseTripsModal implements Action {
  readonly type = ActionTypes.setCloseTripsModal
  constructor(public payload: { closeTripsModal: boolean }) { }
}

//orga_a_A
export class setTableOrganization implements Action {
  readonly type = ActionTypes.setTableOrganization;
  constructor(public payload: { tableOrganizations: any }) { }
}

//orga_a_C
export class setOrganizationFormErrors implements Action {
  readonly type = ActionTypes.setOrganizationFormErrors;
  constructor(public payload: { organizationFormErrors: any }) { }
}

//orga_a_D
export class organizationFormValidation implements Action {
  readonly type = ActionTypes.organizationFormValidation;
  constructor(public payload: { modalOrganization: any, modalOrganizarionType: any, shouldSubmit: any }) { }
}

//orga_a_E
export class postOrganization implements Action {
  readonly type = ActionTypes.postOrganization
  constructor(public payload: { modalOrganization: any }) { }
}

//orga_a_F 
export class putOrganization implements Action {
  constructor(public payload: { modalOrganization: any }) { }
  readonly type = ActionTypes.putOrganization
}

//orga_a_G
export class getOrganizations implements Action {
  readonly type = ActionTypes.getOrganizations
}
export class setModalOrganization implements Action {
  readonly type = ActionTypes.setModalOrganization;
  constructor(public payload: { modalOrganization: any }) { }
}

//orga_a_I
export class setModalOrganizationType implements Action {
  readonly type = ActionTypes.setModalOrganizationType;
  constructor(public payload: { modalOrganizationType: any }) { }
}

//orga_a_J
export class setOrganizationLogo implements Action {
  readonly type = ActionTypes.setOrganizationLogo;
  constructor(public payload: { organizationLogo: Object }) { }
}
export class uploadOrganizationLogo implements Action {
  readonly type = ActionTypes.uploadOrganizationLogo;
  constructor(public payload: { modalOrganization: any }) { }
}
//orga_a_k
export class setCloseModalOrganization implements Action {
  readonly type = ActionTypes.setCloseModalOrganization;
  constructor(public payload: { closeModalOrganization: boolean }) { }
}

export class getTotalBikes implements Action {
  readonly type = ActionTypes.getTotalBikes
}

export class getBikes implements Action {
  readonly type = ActionTypes.getBikes
}

export class setBikes implements Action {
  readonly type = ActionTypes.setBikes
  constructor(public payload: { bikes: any }) { }
}

export class stations implements Action {
  readonly type = ActionTypes.stations
  constructor(public payload: { stations: any }) { }
}

export class saveTypesBikes implements Action {
  readonly type = ActionTypes.saveTypesBikes
  constructor(public payload: { types: any }) { }
}

export class getMasterListTypes implements Action {
  readonly type = ActionTypes.getMasterListTypes
  constructor(public payload: { idOrganization: any }) { }
}

export class getMasterListTypesBikes implements Action {
  readonly type = ActionTypes.getMasterListTypesBikes
}

export class saveStateBikes implements Action {
  readonly type = ActionTypes.saveStateBikes
  constructor(public payload: { state: any }) { }
}
export class getMasterListState implements Action {
  readonly type = ActionTypes.getMasterListState
  constructor(public payload: { idOrganization: any }) { }
}

export class getMasterListStateBikes implements Action {
  readonly type = ActionTypes.getMasterListStateBikes
}

export class modalTypeBike implements Action {
  readonly type = ActionTypes.modalTypeBike
  constructor(public payload: { type: any }) { }
}

export class searchBikeForm implements Action {
  readonly type = ActionTypes.searchBikeForm
  constructor(public payload: { form: any }) { }
}

export class setCurrentPageBike implements Action {
  readonly type = ActionTypes.setCurrentPageBike
  constructor(public payload: { currentPage: any }) { }
}

export class modalBike implements Action {
  readonly type = ActionTypes.modalBike
  constructor(public payload: { bike: any }) { }
}

export class selectedLock implements Action {
  readonly type = ActionTypes.selectedLock
  constructor(public payload: { lock: any }) { }
}

export class totalLockBikes implements Action {
  readonly type = ActionTypes.totalLockBikes
  constructor(public payload: { totalLocks: any }) { }
}

export class postBike implements Action {
  readonly type = ActionTypes.postBike
  constructor(public payload: { bike: any }) { }
}

export class patchLockBike implements Action {
  readonly type = ActionTypes.patchLockBike
  constructor(public payload: { idLock: any, idBike: any }) { }
}

export class patchBike implements Action {
  readonly type = ActionTypes.patchBike
  constructor(public payload: { idBike: any, formBike: any }) { }
}

export class getLocksBikes implements Action {
  readonly type = ActionTypes.getLocksBikes
}

export class saveLocksBikes implements Action {
  readonly type = ActionTypes.saveLocksBikes
  constructor(public payload: { locks: any }) { }
}

export class getCountLocksBikes implements Action {
  readonly type = ActionTypes.getCountLocksBikes
}

export class totalBikes implements Action {
  readonly type = ActionTypes.totalBikes
  constructor(public payload: { totalBikes: any }) { }
}

export class setCurrentPageLocksBike implements Action {
  readonly type = ActionTypes.setCurrentPageLocksBike
  constructor(public payload: { currentPage: any }) { }
}

export class searchLocksBikesForm implements Action {
  readonly type = ActionTypes.searchLocksBikesForm
  constructor(public payload: { form: any }) { }
}

export class showSearchPageBike implements Action {
  readonly type = ActionTypes.showSearchPageBike
}

export class showSearchPageLocksBike implements Action {
  readonly type = ActionTypes.showSearchPageLocksBike
}

export class locksBikesSearchResult implements Action {
  readonly type = ActionTypes.locksBikesSearchResult
  constructor(public payload: { locksSearch: any }) { }
}

export class bikesSearchResult implements Action {
  readonly type = ActionTypes.bikesSearchResult
  constructor(public payload: { bikesSearch: any }) { }
}

export class setBikesSearch implements Action {
  readonly type = ActionTypes.setBikesSearch
  constructor(public payload: { bikes: any }) { }
}

export class showPageBikes implements Action {
  readonly type = ActionTypes.showPageBikes
}

export class showPageLocksBikes implements Action {
  readonly type = ActionTypes.showPageLocksBikes
}

//revisar si se tiene que comentar !
export class bikePageSize implements Action {
  readonly type = ActionTypes.bikePageSize
}

export class skipPageBike implements Action {
  readonly type = ActionTypes.skipPageBike
  constructor(public payload: { skip: any }) { }
}

export class skipPageLocks implements Action {
  readonly type = ActionTypes.skipPageLocks
  constructor(public payload: { skip: any }) { }
}

export class getLocksSearch implements Action {
  readonly type = ActionTypes.getLocksSearch
  constructor(public payload: { bikeId: any }) { }
}

export class filterSearchLocksBikes implements Action {
  readonly type = ActionTypes.filterSearchLocksBikes
}

export class getStationsBikes implements Action {
  readonly type = ActionTypes.getStationsBikes
}

export class saveStationsBikes implements Action {
  readonly type = ActionTypes.saveStationsBikes
  constructor(public payload: { stations: any }) { }
}

export class decisionPatchLockBike implements Action {
  readonly type = ActionTypes.decisionPatchLockBike
  constructor(public payload: { idBike: any }) { }
}
export class patchDeleteLockBike implements Action {
  readonly type = ActionTypes.patchDeleteLockBike
  constructor(public payload: { idLock: any }) { }
}


export class getModalLocksSearch implements Action {
  readonly type = ActionTypes.getModalLocksSearch
}

//admins_a_A
export class setListOrganizations implements Action {
  readonly type = ActionTypes.setListOrganizations
  constructor(public payload: { listOrganizations: any }) { }
}

//admins_a_B
export class setListRoles implements Action {
  readonly type = ActionTypes.setListRoles
  constructor(public payload: { listRoles: any }) { }
}

//admins_a_C
export class setCurrentPageAdmins implements Action {
  readonly type = ActionTypes.setCurrentPageAdmins
  constructor(public payload: { currentPageAdmins: any }) { }
}

//admins_a_D
export class setModalAdminSystems implements Action {
  readonly type = ActionTypes.setModalAdminSystems
  constructor(public payload: { modalAdminSystems: any }) { }
}

//admins_a_E
export class setAdminSystemsFormErrors implements Action {
  readonly type = ActionTypes.setAdminSystemsFormErrors
  constructor(public payload: { adminSystemsFormErrors: any }) { }
}

//admins_a_F
export class setTotalUsersAdmins implements Action {
  readonly type = ActionTypes.setTotalUsersAdmins
  constructor(public payload: { totalUsersAdmins: any }) { }
}

//admins_a_G
export class setAdminSystemsFormStatus implements Action {
  readonly type = ActionTypes.setAdminSystemsFormStatus
  constructor(public payload: { adminSystemsFormStatus: any }) { }
}

//admins_a_H
export class setLoadingAdminSystemsPage implements Action {
  readonly type = ActionTypes.setLoadingAdminSystemsPage
  constructor(public payload: { loadingAdminSystemsPage: any }) { }
}

//admins_a_I
export class setCloseAdminSystemsModal implements Action {
  readonly type = ActionTypes.setCloseAdminSystemsModal
  constructor(public payload: { closeAdminSystemsModal: any }) { }
}

//admins_a_J
export class setInfoFormAdmin implements Action {
  readonly type = ActionTypes.setInfoFormAdmin
  constructor(public payload: { infoFormAdmin: any }) { }
}

//admins_a_k
export class setAdminFormValidation implements Action {
  readonly type = ActionTypes.setAdminFormValidation
  constructor(public payload: { adminFormValidation: any }) { }
}

//admins_a_L
export class setMasterList implements Action {
  readonly type = ActionTypes.setMasterList
  constructor(public payload: { masterList: any }) { }
}

//admins_a_M
export class showPageAdmin implements Action {
  readonly type = ActionTypes.showPageAdmin
  constructor(public payload: { adminPageNumber: any }) { }
}

//admins_a_N
export class getListOrganizations implements Action {
  readonly type = ActionTypes.getListOrganizations;
}

//admins_a_O
export class getListRoles implements Action {
  readonly type = ActionTypes.getListRoles;
}

//admins_a_P
export class getTotalUsersAdmins implements Action {
  readonly type = ActionTypes.getTotalUsersAdmins;
}

//admins_a_Q
export class getUsersAdmins implements Action {
  readonly type = ActionTypes.getUsersAdmins;
}

//admins_a_R
export class showSearchPageAdmins implements Action {
  readonly type = ActionTypes.showSearchPageAdmins;
  constructor(public payload: { searchTerm: any }) { }
}

//admins_a_S
export class adminsFormValidation implements Action {
  readonly type = ActionTypes.adminsFormValidation;
}

//admins_a_T
export class updateAdminUser implements Action {
  readonly type = ActionTypes.updateAdminUser;
}

//admins_a_U
export class createAdminUser implements Action {
  readonly type = ActionTypes.createAdminUser;
}
export class postUserRol implements Action {
  readonly type = ActionTypes.postUserRol;
  constructor(public payload: { userRol: any }) { }
}
export class patchUserRol implements Action {
  readonly type = ActionTypes.patchUserRol;
  constructor(public payload: { userInfo: any }) { }
}

//admins_a_V
export class changePasswordAdminUser implements Action {
  readonly type = ActionTypes.changePasswordAdminUser;
}

//admins_a_W
export class getMasterList implements Action {
  readonly type = ActionTypes.getMasterList;
}

//admins_a_X
export class showGlobalPageAdmins implements Action {
  readonly type = ActionTypes.showGlobalPageAdmins;
}

export type actions =
  partialValidationForm
  | getTotalBikes
  | getBikes
  | setBikes
  | stations
  | getMasterListTypesBikes
  | saveTypesBikes
  | saveStateBikes
  | getMasterListStateBikes
  | modalTypeBike
  | searchBikeForm
  | setCurrentPageBike
  | modalBike
  | selectedLock
  | totalLockBikes
  | postBike
  | patchLockBike
  | getLocksBikes
  | saveLocksBikes
  | getCountLocksBikes
  | totalBikes
  | setCurrentPageLocksBike
  | searchLocksBikesForm
  | showSearchPageBike
  | showSearchPageLocksBike
  | locksBikesSearchResult
  | bikesSearchResult
  | setBikesSearch
  | showPageBikes
  | bikePageSize
  | skipPageBike
  | skipPageLocks
  | getLocksSearch
  | filterSearchLocksBikes
  | getStationsBikes
  | saveStationsBikes
  | decisionPatchLockBike
  | patchBike
  | patchDeleteLockBike
  | getModalLocksSearch
  | partialValidationForm
  | setCloseModalOrganization
  | getMasterListState
  | setTableOrganization //orga_a_A
  | setOrganizationFormErrors //orga_a_C
  | organizationFormValidation //orga_a_D
  | postOrganization //orga_a_E
  | putOrganization //orga_a_F
  | getOrganizations //orga_a_G
  | setModalOrganization //orga_a_H
  | setModalOrganizationType //orga_a_I
  | setOrganizationLogo //orga_a_J
  | validateEmailPassword
  | setCloseTripsModal
  | buildSearchTripsFilter
  | setPermissions
  | getUserData
  | getPermissions
  | setMosalStationInfo
  | setLoadingTripsPage
  | validateUser
  | clearValidationError
  | setPasswordError
  | ApiError
  | saveApiData
  | saveData
  | getTableData
  | closeDialog
  | getTableDataLength
  | deletedAt
  | showSweetAlert
  | saveStationsInfo
  | setStationFormErrors
  | saveStationSearch
  | updateStationTable
  | stationFormValidation
  | getStations
  | htmlSweetAlert
  | logOutUser
  | AddUser
  | AddUserInfo
  | RefreshUserTable
  | serverError
  | ActiveUser
  | saveUsers
  | modalUser
  | saveUserTrips
  | calculationDates
  | postPenaltys
  | patchUser
  | penaltyDates
  | setSearchTerm
  | showPage
  | totalGetUsers
  | totalGetTrip
  | totalSearchTerm
  | getPenaltyTypes
  | penaltyTypes
  | userPagination
  | userPaginationTrip
  | paginatorFinalUser
  | showPageSearch
  | searchTerm
  | datasourceSearch
  | paginatorFinalUserTrip
  | showPageSearchTrip
  | setSearchTermTrip
  | datasourceSearchTrip
  | searchTermTrip
  | paginatorFinalUserSearchTrip
  | getStationsTrip
  | stationsTrips
  | loadTripStates //trip_a_C
  | setTripStates //trip_a_D
  | getTotalTrips //trip_a_E
  | setTotalTrips //trip_a_M
  | setCurrentPageTrips //trip_a_F
  | setSearchTripsForm //trip_a_H
  | showPageTrips //trip_a_G
  | showSearchPageTrips //trip_a_I
  | showGlobalPageTrips //trip_a_J
  | searchTrips //trip_a_K
  | setTripsSearchResult //trip_a_L
  | patchEndTrip //trip_a_Ñ
  | setTripsFormErrors //trip_a_O
  | tripFormValidation //trip_a_P
  | setModalTrip //trip_a_Q
  | buildSearchFilter //trip_a_R
  | setTripsPageNumber //trip_a_S
  | setTripsFormStatus //trip_a_O
  | getReportTrips //trip_a_X
  | generateDownloadTrips //trip_a_Y
  | setLoadingDownload
  | getReportUsers
  | setListOrganizations //admins_a_A
  | setListRoles //admins_a_B
  | setCurrentPageAdmins //admins_a_C
  | setModalAdminSystems //admins_a_D
  | setAdminSystemsFormErrors //admins_a_E
  | setTotalUsersAdmins //admins_a_F
  | setAdminSystemsFormStatus //admins_a_G
  | setLoadingAdminSystemsPage //admins_a_H
  | setCloseAdminSystemsModal //admins_a_I
  | setInfoFormAdmin //admins_a_J
  | setAdminFormValidation //admins_a_K
  | setMasterList//admins_a_L
  | showPageAdmin//admins_a_M
  | getListOrganizations//admins_a_N
  | getListRoles//admins_a_O
  | getTotalUsersAdmins//admins_a_P
  | getUsersAdmins//admins_a_Q
  | showSearchPageAdmins//admins_a_R
  | adminsFormValidation//admins_a_S
  | updateAdminUser//admins_a_T
  | createAdminUser//admins_a_U
  | changePasswordAdminUser//admins_a_V
  | getMasterList//admins_a_W
  | showGlobalPageAdmins//admins_a_X
  | patchUserRol//admins_a_
  | setLoadingLockTable
  | setTotalLocks
  | setCurrentPageLock
  | setSelectedLockInfo
  | setLockPageNumber
  | setCurrentPageLockCommands
  | setLoadingCommandsTable
  | setLockPageSize
  | rolUserLocks
  | setLocksSearchParams
  | setLocksSearchResult
  | setCommandsPageSize
  | setTotalLockCommands
  | setEditLockFormErrors
  | getTotalLocks
  | showGlobalPageLocks
  | showSearchPageLocks
  | showPageLocks
  | getLockCommands
  | changeLockPageSize
  | searchBikesByNumber
  | getTotalLockCommands
  | showPageLockCommands
  | setLockCommandsPageNumber
  | validateEditLockForm
  | validateUniqQrMac
  | setCloseModalLocks
  | validateLockOrganization
  | setLoadingTicketsTable//support_a_A
  | setTotalTickets//support_a_B
  | setCurrentPageTickets//support_a_C
  | setSelectedTicketInfo//support_a_D
  | setTicketPageNumber//support_a_E
  | setSearchWordTickets//support_a_F
  | setMasterListTicket//support_a_J
  | getTicketsPage//support_a_G
  | getTotalTickets//support_a_H
  | getMasterListTicket//support_a_I
  | getStationsTicket//support_a_J
  | patchTicket//support_a_L
  | setTicketPageSize//support_a_L
  | setTicketSearchStatus//support_a_K
  | setTicketsSearchResult//support_a_tsr
  | showSearchPageTickets//support_a_ssp
  | showPageTicket//support_a_spk
  | setCloseModalTickets//support_a_
  | setMapStationsSearchParams//Map_a_A
