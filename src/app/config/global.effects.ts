import { Injectable } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { ROUTER_REQUEST } from '@ngrx/router-store';
import { tap, withLatestFrom, map, mergeMap, filter, exhaustMap, catchError, concatMap, switchMap, delay, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  ActionTypes,
  validateEmailPassword,
  partialValidationForm,
  setPasswordError,
  clearValidationError,
  getTableData,
  getTableDataLength,
  ApiError,
  saveData,
  UpdateData,
  deletedAt, showSweetAlert, htmlSweetAlert, logOutUser, getStations, saveStationsInfo, updateStationTable, loadTripStates, getTotalTrips, showGlobalPageTrips, showPageTrips, showSearchPageTrips, buildSearchTripsFilter, searchTrips, setTripStates, setTotalTrips, setTripsPageNumber, setCurrentPageTrips, patchEndTrip, setTripsSearchResult, setTripsFormStatus, setLoadingTripsPage, setCloseTripsModal,
  getOrganizations, postOrganization, setTableOrganization, uploadOrganizationLogo, putOrganization, setCloseModalOrganization, getListOrganizations, setListOrganizations, getListRoles, setListRoles, getTotalUsersAdmins, setTotalUsersAdmins, getUsersAdmins, setCurrentPageAdmins, setLoadingAdminSystemsPage, showGlobalPageAdmins, showSearchPageAdmins, createAdminUser, postUserRol, setCloseAdminSystemsModal, updateAdminUser, changePasswordAdminUser,
  getReportUsers, generateDownload, setLoadingDownload, getReportTrips, generateDownloadTrips, setMasterListTicket, getMasterListTicket, getTotalTickets, setTotalTickets, getTicketsPage, setLoadingTicketsTable, setCurrentPageTickets, getStationsTicket, setSelectedTicketInfo, patchTicket, searchTickets, setTicketsSearchResult, setTicketPageSize, showSearchPageTickets, showPageTicket, setTicketSearchStatus, setCloseModalTickets, patchUserRol
} from './global.actions';
import { ValidationService } from '../services/validation.service';
import { SplashScreenService } from '../services/splash-screen.service';
import { UserEffects } from './effects/user.effects';
import { Observable, of, combineLatest, concat, forkJoin } from 'rxjs';
import { State } from './global.reducer';
import { ApiService } from '../services/api.service';
import { GlobalService } from "../services/global.service"
import { HttpErrorResponse } from '@angular/common/http';
import { UserInfos } from '../models/userInfos.model';
import { User } from '../models/user.model';
import { AuthEffects } from './effects/auth.effects';
import { ValidationsEffects } from './effects/validations.effects';
import { LogicEffects } from './effects/logic.effects';
import * as _ from 'lodash';
import { ExcelService } from '../services/excel.service';
@Injectable({ providedIn: 'root' })
export class GeneralEffects {
  public dataInfo;
  public elementExist;
  public tempData;
  public resultUsers;
  public tempResponse = [];
  constructor(
    private actions$: Actions,
    private excelService: ExcelService,
    private router: Router,
    private store$: Store,
    private validation: ValidationService,
    private spinner: SplashScreenService,
    private apiService: ApiService,
    private globalService: GlobalService
  ) { }

  @Effect({ dispatch: false })
  routing$ = this.actions$.pipe(
    ofType(ROUTER_REQUEST),
    withLatestFrom(this.store$.select((state: State) => state.apiData.token)),
    tap((values: Array<any>) => {
      if (!values[1]) {
        if (!values[0].payload.event.url.includes('/auth')) {
          // this.router.navigate(['/auth']);
        }
      }

    })
  );

  @Effect()
  validateEmailPassword$: Observable<Action> = this.actions$.pipe(
    ofType(ActionTypes.validateEmailPassword),
    map((action: validateEmailPassword) => {
      this.spinner.show();
      const form = { ...action.payload.form };
      const formModel = action.payload.formModel;
      const dispatchFormAction = action.payload.dispatchFormAction;
      for (const input of formModel.inputs) {
        if (input.front) {
          if (input.front.typeText) {
            if (input.front.typeText.type === 'number') {
              form[input.inputKey] = Number(form[input.inputKey]);
            }
          }
        }
      }
      const startupErrors = this.validation.validateForm(form, formModel);
      if (dispatchFormAction) {
        if (startupErrors) {
          this.spinner.hide();
          return new setPasswordError({ startupErrors, whereErrorsSave: action.payload.dispatchFormAction.whereErrorsSave });
        } else {
          return new dispatchFormAction.success({ form, formModel });
        }
      } else {
        // this.spinner.hide();
        return {
          type:
            '[global programing] Ups!! seems like you are programing this part',
        };
      }
    })
  );

  @Effect()
  partialValidationForm$ = this.actions$.pipe(
    ofType<partialValidationForm>(ActionTypes.partialValidationForm),
    map((action) => {
      const key = action.payload.input;
      const errors = this.validation.validateForm(action.payload.form, action.payload.formModel);
      const dispatchFormAction = action.payload.dispatchFormAction;
      if (dispatchFormAction) {
        if (errors) {
          if (errors[key]) {
            return new setPasswordError({ startupErrors: { [key]: errors[key] }, whereErrorsSave: dispatchFormAction.whereErrorsSave });
          } else {
            return new clearValidationError({ errorsToClear: [key], whereErrorsSave: dispatchFormAction.whereErrorsSave });
          }
        } else {
          return new clearValidationError({ errorsToClear: [key], whereErrorsSave: dispatchFormAction.whereErrorsSave });
        }
      } else {
        return {
          type:
            '[global programing] Ups!! seems like you are programing this part',
        };
      }
    })
  );

  @Effect()
  getTableData$ = this.actions$.pipe(
    ofType<getTableData>(ActionTypes.getTableData),
    tap(() => this.spinner.show()),
    mergeMap(action =>
      combineLatest(
        of(action),
        this.store$.select((state) => state[action.payload.storeState.store][action.payload.storeState.storeState]),
      ).pipe(take(1))
    ),
    tap((data) => {
      if (!(data[1].length == data[0].payload.filter.skip || !data[0].payload.concatResult)) {
        this.spinner.hide()
      }
    }),
    filter((data) => data[1].length === data[0].payload.filter.skip || !data[0].payload.concatResult),
    exhaustMap((data) => {
      return this.apiService.get(data[0].payload.data, data[0].payload.filter).pipe(
        map((response) => {
          response.map((result) => {
            result.adminName = '';
            if (result.adminId) {
              if (Array.isArray(result.adminId.adminsIds)) {
                if (result.adminId.adminsIds.length > 0) {
                  result.adminId.adminsIds.forEach(element => {
                    let elemento = data[0].payload.users.find(item => item.userId === element);
                    let name = elemento != undefined ? elemento.name : '';
                    if (result.adminName == '' && name != '') {
                      result.adminName = name;
                    } else {
                      if (result.adminName != '' && name != '') {
                        result.adminName += ',' + name;
                      }
                    }
                  });
                }
              }
            }
          });
          let dataInfo;
          if (data[0].payload.replaceResult) {
            dataInfo = [...data[1]];
            dataInfo.splice(data[0].payload.filter.skip, response.length, ...response);
          } else {
            dataInfo = data[0].payload.concatResult ? [...data[1], ...response] : response;
          }
          return new saveData({ data: dataInfo, dataName: data[0].payload.storeState.store + '.' + data[0].payload.storeState.storeState });
        }),
        tap(() => this.spinner.hide()),
        catchError((error: HttpErrorResponse) => {
          tap(() => { this.spinner.hide() });
          return of(new ApiError({ error }));
        })
      );
    })
  );


  @Effect()
  UpdateData$ = this.actions$.pipe(
    ofType<UpdateData>(ActionTypes.UpdateData),
    exhaustMap((userActions) => {
      return this.apiService.get('user-infos', { include: [{ 'relation': 'user' }] }).pipe(
        map(response => {
          return new getTableData({
            users: response,
            data: userActions.payload.data,
            filter: userActions.payload.filter,
            concatResult: userActions.payload.concatResult,
            replaceResult: userActions.payload.replaceResult,
            storeState: userActions.payload.storeState
          });
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  getStations$ = this.actions$.pipe(
    ofType<getStations>(ActionTypes.getStations),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      this.spinner.show();
      return this.apiService.get('stations', { where: { organizationId: action[1]['organizationId'] }, order: ['createdAt DESC'] }).pipe(
        map(response => {
          response.forEach(stationInfo => {
            stationInfo.closingTime = this.globalService.defineTimeFormat(stationInfo.closingTime)
            stationInfo.openingTime = this.globalService.defineTimeFormat(stationInfo.openingTime)
          });
          this.spinner.hide();
          return new saveStationsInfo({ tableStations: response });
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  updateStationTable$ = this.actions$.pipe(
    ofType<updateStationTable>(ActionTypes.updateStationTable),
    withLatestFrom(this.store$.select((state: State) => state.apiData.modalStationInfo)),
    withLatestFrom(this.store$.select((state: State) => state.apiData.stationModalType)),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      this.spinner.show();
      let station: any = {};
      station = {
        ...action[0][0][1],
        closingTime: this.globalService.defineTimeFormat(action[0][0][1]['closingTime']),
        openingTime: this.globalService.defineTimeFormat(action[0][0][1]['openingTime']),
        state: String(action[0][0][1]['state']) === "true",
        latitude: action[0][0][1]['latitude'].toString(),
        longitude: action[0][0][1]['longitude'].toString(),
        bikesCapacity: parseInt(action[0][0][1]['bikesCapacity']),
        organizationId: action[1]['organizationId']
      };
      if (action[0][1]['modalType'] == 'edit') {
        return this.apiService.put('stations', station, { setDeleteTime: true, setUpdateTime: true }).pipe(
          map((response) => new getStations()),
          catchError((error: HttpErrorResponse) => {
            return of(new ApiError({ error }))
          })
        )
      } else {
        station = { ...station, mechanicBikes: 0, electricBikes: 0 }
        return this.apiService.post('stations', station, { setCreateTime: true }).pipe(
          map((response) => new getStations()),
          catchError((error: HttpErrorResponse) => {
            return of(new ApiError({ error }))
          })
        )
      }
    })
  );

  @Effect()
  getTableDataLength$ = this.actions$.pipe(
    ofType<getTableDataLength>(ActionTypes.getTableDataLength),
    exhaustMap((userActions) => {
      return this.apiService.get(userActions.payload.data, userActions.payload.filter, { count: true }).pipe(
        map((response) =>
          new saveData({ data: response, dataName: userActions.payload.storeState.store + '.' + userActions.payload.storeState.storeState }
          ),
        ),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  deletedAt$ = this.actions$.pipe(
    ofType<deletedAt>(ActionTypes.deletedAt),
    exhaustMap((userActions) => {
      this.spinner.show();
      return this.apiService.put(userActions.payload.data, userActions.payload.form, { setDeleteTime: true, setUpdateTime: true }).pipe(
        map((response) => this.spinner.hide()),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      );
    })
  );

  @Effect({ dispatch: false })
  showSweetAlert$ = this.actions$.pipe(
    ofType<showSweetAlert>(ActionTypes.showSweetAlert),
    tap((action) => {
      this.globalService.showSweetAlert(action.payload.title, action.payload.text, action.payload.type, action.payload.buttonText)
    })
  );

  @Effect({ dispatch: false })
  htmlSweetAlert$ = this.actions$.pipe(
    ofType<htmlSweetAlert>(ActionTypes.htmlSweetAlert),
    tap((action) => {
      this.globalService.htmlSweetAlert(action.payload.html)
    })
  );

  @Effect()
  logOutUser$ = this.actions$.pipe(
    ofType<logOutUser>(ActionTypes.logOutUser),
    map(() => this.globalService.clearStorage()),
    switchMap((response) => of(
      new saveData({ data: User, dataName: "user" }),
      new saveData({ data: "", dataName: "token" }),
      new saveData({ data: UserInfos, dataName: "userInfo" }),
    )),
    tap(() => location.reload())
  )

  //orga_f_A
  @Effect()
  getOrganizations$ = this.actions$.pipe(
    ofType<getOrganizations>(ActionTypes.getOrganizations),
    exhaustMap((userActions) => {
      return this.apiService.get('organizations', {}).pipe(
        map(organizations => {
          this.spinner.hide();
          return new setTableOrganization({ tableOrganizations: organizations })
        }),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    }))

  @Effect() // trip_f_D
  loadTripStates$ = this.actions$.pipe(
    ofType<loadTripStates>(ActionTypes.loadTripStates),
    exhaustMap((action) => {
      const filter = {
        where: { table: { like: 'trips' } }
      }
      return this.apiService.get('master-lists', filter).pipe(
        map(masterLists => {
          this.spinner.hide();
          return new setTripStates({ tripState: masterLists });
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    }))
  @Effect()
  uploadOrganizationLogo$ = this.actions$.pipe(
    ofType<uploadOrganizationLogo>(ActionTypes.uploadOrganizationLogo),
    withLatestFrom(this.store$.select((state: State) => state.apiData.organizationLogo)),
    withLatestFrom(this.store$.select((state: State) => state.user.modalOrganizationType)),
    exhaustMap((action) => {
      this.spinner.show();
      var formData: any = new FormData();

      let form = _.cloneDeep(action[0][0].payload.modalOrganization);
      action[0][1]['logoInfo'] ? formData.append("upload", action[0][1]['logoInfo'].data) : null;

      if (action[1] == 'add') {
        return this.apiService.postImages("users/upload", formData, { useAuthHeaders: false, }).pipe(
          map((response) => {
            return new postOrganization({ modalOrganization: { ...form, logo: response.files[0].location, nit: 0 } })
          }),
        )
      } else {
        if (action[0][1]['shouldLoad']) {
          return this.apiService.postImages("users/upload", formData, { useAuthHeaders: false, }).pipe(
            map((response) => {
              return new putOrganization({ modalOrganization: { ...form, logo: response.files[0].location, nit: 0 } })
            }),
          )
        } else {
          return [new putOrganization({ modalOrganization: { ...form, nit: 0 } })]
        }
      }
    })
  )
  //orga_f_C
  @Effect()
  postOrganization$ = this.actions$.pipe(
    ofType<postOrganization>(ActionTypes.postOrganization),
    exhaustMap((action) => {
      return this.apiService.post('organizations', action.payload.modalOrganization, { setCreateTime: true }).pipe(
        exhaustMap((response) => {
          return [
            new setCloseModalOrganization({ closeModalOrganization: true }),
            new getOrganizations()
          ]
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
    })
  )
  //orga_f_D
  @Effect()
  putOrganization$ = this.actions$.pipe(
    ofType<putOrganization>(ActionTypes.putOrganization),
    withLatestFrom(this.store$.select((state: State) => state.user.modalOrganization)),
    exhaustMap((action) => {
      let organization: any = { ...action[1], ...action[0].payload.modalOrganization }

      return this.apiService.put('organizations', organization, { setDeleteTime: true, setUpdateTime: true }).pipe(
        exhaustMap((response) => {
          return [
            new setCloseModalOrganization({ closeModalOrganization: true }),
            new getOrganizations()
          ]
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
      return []
    })
  )

  @Effect() // trip_f_B
  getTotalTrips$ = this.actions$.pipe(
    ofType<getTotalTrips>(ActionTypes.getTotalTrips),
    exhaustMap((action) => {
      return this.apiService.get('trips', {}, { count: true }).pipe(
        exhaustMap(tripsCount => {
          this.spinner.hide();
          return [
            new setTotalTrips({ totalTrips: tripsCount.count }),
            new setTripsPageNumber({ tripsPageNumber: tripsCount.count }),
            new showGlobalPageTrips({ tripsPageNumber: 0 }),
            new setLoadingTripsPage({ loadingTripsPage: false })
          ];
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect() // trip_f_G
  showGlobalPageTrips$ = this.actions$.pipe(
    ofType<showGlobalPageTrips>(ActionTypes.showGlobalPageTrips),
    withLatestFrom(this.store$.select((state: State) => state.user.currentPageTrips)),
    exhaustMap((action) => {
      let pageNumber: number = action[0].payload.tripsPageNumber;
      const filter: any = {
        where: {},
        limit: 20,
        skip: pageNumber,
        include: [
          { 'relation': 'user' },
          { 'relation': 'startStation' },
          { 'relation': 'endStation' },
          { 'relation': 'bike' },
        ]
      }
      return this.apiService.get('trips', filter).pipe(
        exhaustMap(trips => {
          this.spinner.hide();
          return [
            new setLoadingTripsPage({ loadingTripsPage: false }),
            new setCurrentPageTrips({ currentPageTrips: trips })
          ];
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect() // trip_f_H
  showPageTrips$ = this.actions$.pipe(
    ofType<showPageTrips>(ActionTypes.showPageTrips),
    map((action) => {
      let pageNumber: number = action.payload.tripsPageNumber;
      return new showGlobalPageTrips({ tripsPageNumber: pageNumber })
    })
  );

  @Effect() // trip_f_
  buildSearchTripsFilter$ = this.actions$.pipe(
    ofType<buildSearchTripsFilter>(ActionTypes.buildSearchTripsFilter),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    withLatestFrom(this.store$.select((state: State) => state.apiData.tableStations)),
    exhaustMap((action) => {
      let endDate = action[0][0].payload.searchTripsForm.endDate ? new Date(action[0][0].payload.searchTripsForm.endDate) : ''
      let startDate = action[0][0].payload.searchTripsForm.startDate ? new Date(action[0][0].payload.searchTripsForm.startDate) : ''

      let addWhere: any = [];
      let addWhereFilter: any = [];
      let idNumber = action[0][0].payload.searchTripsForm.idNumber ? action[0][0].payload.searchTripsForm.idNumber : ''
      let userIds = [];
      let station: any;
      let usersSearch: any = [];

      let filterBicis = { where: { and: addWhere } };
      let filterTrip: any = {
        where: { and: addWhereFilter },
        include: [
          { 'relation': 'user' },
          { 'relation': 'startStation' },
          { 'relation': 'endStation' },
          { 'relation': 'bike' },
        ]
      };

      const filterUser = {
        where: {
          or: [
            { idNumber: { like: idNumber, options: "i" } },
            { email: { like: idNumber, options: "i" } },
          ]
        }
      }
      if (action[0][0].payload.searchTripsForm.nameStation) {
        station = action[1].find((station) =>
          station.name == action[0][0].payload.searchTripsForm.nameStation.name
        )
        addWhereFilter.push({ startStationId: { inq: [station.id] } })
      }

      action[0][0].payload.searchTripsForm.numberBike ?
        addWhere.push({ number: { like: action[0][0].payload.searchTripsForm.numberBike.toString() } }) : null;

      action[0][0].payload.searchTripsForm.stateTrip ?
        addWhereFilter.push({ state: { like: action[0][0].payload.searchTripsForm.stateTrip } }) : null;

      startDate ? addWhereFilter.push({ startDate: { gte: startDate.toString() } }) : null;
      endDate ? addWhereFilter.push({ endDate: { lte: endDate.toString() } }) : null;


      if (addWhere.length && idNumber) {
        return this.getBikesAndUser(addWhereFilter, filterBicis, usersSearch, userIds, filterTrip, filterUser)
      } else if (!addWhere.length && idNumber) {
        return this.getUsers(usersSearch, userIds, addWhereFilter, filterTrip, filterUser)
      } else if (addWhere.length && !idNumber) {
        return this.getBikes(filterBicis, addWhereFilter, filterTrip)
      } else if (!addWhere.length && !idNumber && addWhereFilter.length) {
        return [new searchTrips({ filter: filterTrip })]
      }

    })
  );

  getBikes(filterBicis, addWhereFilter, filterTrip) {
    return this.apiService.get("bikes", filterBicis).pipe(
      map((bikes: any) => {
        let bikeIds = [];
        bikes.forEach(bike => {
          bikeIds.push(bike.id);//lock.bikeId
        })
        addWhereFilter.push({ bikeId: { inq: bikeIds } });

        return new searchTrips({ filter: filterTrip })
      }),
      catchError((error: HttpErrorResponse) => {
        tap(() => this.spinner.hide())
        return of(new ApiError({ error }));
      })
    )
  }
  getUsers(usersSearch, userIds, addWhereFilter, filterTrip, filterUser) {
    return this.apiService.get("users", filterUser, { useAuthHeaders: true }).pipe(
      map((users: any) => {
        users.forEach(user => {
          usersSearch.push(user.id);//lock.bikeId
        })
        usersSearch.forEach(user => {
          userIds.push(user);
        })
        addWhereFilter.push({ userId: { inq: userIds } })
        return new searchTrips({ filter: filterTrip })
      })
    )
  }

  getBikesAndUser(addWhereFilter, filterBicis, usersSearch, userIds, filterTrip, filterUser) {
    return this.apiService.get("bikes", filterBicis).pipe(
      exhaustMap((bikes: any) => {
        let bikeIds = [];
        bikes.forEach(bike => {
          bikeIds.push(bike.id);//lock.bikeId
        })
        addWhereFilter.push({ bikeId: { inq: bikeIds } });
        return this.apiService.get("users", filterUser, { useAuthHeaders: true }).pipe(
          map((users: any) => {
            users.forEach(user => {
              usersSearch.push(user.id);//lock.bikeId
            })
            usersSearch.forEach(user => {
              userIds.push(user);
            })
            addWhereFilter.push({ userId: { inq: userIds } });
            return new searchTrips({ filter: filterTrip })
          })
        )
      }),
      catchError((error: HttpErrorResponse) => {
        tap(() => this.spinner.hide())
        return of(new ApiError({ error }));
      })
    )
  }

  @Effect() // trip_f_
  searchTrips$ = this.actions$.pipe(
    ofType<searchTrips>(ActionTypes.searchTrips),
    exhaustMap((action) => {
      return this.apiService.get("trips", action.payload.filter).pipe(
        exhaustMap((trips: any) => {
          return [
            new setTotalTrips({ totalTrips: trips.length }),
            new setTripsSearchResult({ tripsSearchResult: trips }),
            new setTripsPageNumber({ tripsPageNumber: trips.length }),
            new showSearchPageTrips({ tripsPageNumber: 0 })
          ]
        }),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      )
    })
  );


  @Effect() // trip_f_I
  showSearchPageTrips$ = this.actions$.pipe(
    ofType<showSearchPageTrips>(ActionTypes.showSearchPageTrips),
    withLatestFrom(this.store$.select((state: State) => state.user.tripsSearchResult)),
    exhaustMap((action) => {
      return [
        new setTripsFormStatus({ tripsFormStatus: true }),
        new setCurrentPageTrips({ currentPageTrips: action[1] })]
    })
  );

  @Effect() // trip_f_E
  patchEndTrip$ = this.actions$.pipe(
    ofType<patchEndTrip>(ActionTypes.patchEndTrip),
    withLatestFrom(this.store$.select((state: State) => state.user.modalTrip)),
    exhaustMap((action) => {
      let form: any = { ...action[1], state: 'finished' }
      delete form.bike
      delete form.endStation
      delete form.user

      let body = {
        trip: form,
        endStationId: form.startStation.id,
        userTripInformation: { distance: -1 }
      }
      return this.apiService.post('endTrip', body, {}).pipe(
        exhaustMap((response) => [
          new setCloseTripsModal({ closeTripsModal: true }),
          new showPageTrips({ tripsPageNumber: 0 })
        ]),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
    })
  );
  @Effect()
  generateDownloadTrips$ = this.actions$.pipe(
    ofType<generateDownloadTrips>(ActionTypes.generateDownloadTrips),
    map((action) => {
      let trips: any = []
      action.payload.dateRangeTrips.forEach((trip) => {
        trips.push({
          "Email": trip.user ? trip.user.email : "",
          "Num documento": trip.user ? trip.user.idNumber : "",
          "Tipo documento": trip.user ? trip.user.idType : "",
          "Fecha inicio": new Date(trip.startDate),
          "Fecha fin": new Date(trip.endDate),
          "Estacion inicio": trip.startStation.name,
          "Estacion fin": trip.endStation ? trip.endStation.name : "",
          "Numero bici": trip.bike.number,
          "Tiempo (min)": trip.time,
          "Kms": trip.distanceKm,
          "Co2 gr": trip.distanceKm != "0" || trip.distanceKm != "-1" ? parseFloat(trip.distanceKm) * 345 : 0,
          "Calorias": trip.distanceKm != "0" || trip.distanceKm != "-1" ? parseFloat(trip.distanceKm) * 30 : 0,
          "Organizacion": trip.organization.name
        })
      })

      this.excelService.exportAsExcelFile(trips, 'trips');
      return new setLoadingDownload({ loadingDownload: false })
    })
  );
  //admins_e_A
  @Effect()
  getListOrganizations$ = this.actions$.pipe(
    ofType<getListOrganizations>(ActionTypes.getListOrganizations),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((user) => {
      let filter: any = {};
      if (user[1]['roles'].indexOf("615f83e663996c6107cb0897") < 0) {
        filter.where = {id: user[1]['organizationId']}
      }
      return this.apiService.get("organizations", filter).pipe(
        map((organizations) => {
          return new setListOrganizations({ listOrganizations: organizations })
        })
      )
    })
  );

  //admins_e_A
  @Effect()
  getListRoles$ = this.actions$.pipe(
    ofType<getListRoles>(ActionTypes.getListRoles),
    exhaustMap((ListRoles) => {
      return this.apiService.get("roles", {}).pipe(
        map((roles) => {
          return new setListRoles({ listRoles: roles })
        })
      )
    })
  );

  @Effect()
  getReportTrips$ = this.actions$.pipe(
    ofType<getReportTrips>(ActionTypes.getReportTrips),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      const filter = {
        include: [
          { 'relation': 'startStation' },
          { 'relation': 'endStation' },
          { 'relation': 'bike' },
          { 'relation': 'user' },
          { 'relation': 'organization' }
        ],
        order: ['created_at DESC'],
        where: {
          and: [
            { organizationId: action[1]['organizationId'] },
            { createdAt: { gte: Date.parse(action[0].payload.dateRangeTrips.startDate) } },
            { createdAt: { lte: Date.parse(action[0].payload.dateRangeTrips.endDate) } },
          ]
        },
      }
      return this.apiService.get('trips', filter).pipe(
        exhaustMap(response => {
          if (response.length > 0) {
            return [new generateDownloadTrips({ dateRangeTrips: response })];
          }
          else {
            return [
              new setLoadingDownload({ loadingDownload: false }),
              new showSweetAlert({ title: 'Mensaje', text: 'No se encontraron prestamos en el rango de fechas ingresadas', type: 'error', buttonText: 'Aceptar' })
            ]
          }
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  showSearchPageAdmins$ = this.actions$.pipe(
    ofType<showSearchPageAdmins>(ActionTypes.showSearchPageAdmins),
    exhaustMap((action) => {
      let filter = {
        where: {
          and: [
            {
              or: [
                { name: { like: action.payload.searchTerm } },
                { idNumber: { like: action.payload.searchTerm } },
                { email: { like: action.payload.searchTerm } },
              ]
            },
            {
              or: [
                { roles: { like: '6142c9b6d97a767dbd8ad12f' } },
                { roles: { like: '615f83b163996c6107cb0896' } },
                { roles: { like: '615f83e663996c6107cb0897' } }
              ]
            }
          ]
        },
        include: [{ 'relation': 'organization' }],
        order: 'updated_at DESC',
        limit: 100,
      }
      return this.apiService.get('users', filter).pipe(
        exhaustMap((usersAdmins) => {
          return [
            new setCurrentPageAdmins({ currentPageAdmins: usersAdmins }),
            new setLoadingAdminSystemsPage({ loadingAdminSystemsPage: false }),
          ]
        }),
        catchError((error: HttpErrorResponse) => {
          this.spinner.hide();
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  getReportUsers$ = this.actions$.pipe(
    ofType<getReportUsers>(ActionTypes.getReportUsers),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      const filter = {
        include: [{ 'relation': 'organization' }],
        order: ['created_at DESC'],
        where: {
          and: [
            { roles: { like: '6142ca4bd97a767dbd8ad130' } },
            { organizationId: action[1]['organizationId'] },
            { created_at: { gte: Date.parse(action[0].payload.dateRangeUsers.startDate) } },
            { created_at: { lte: Date.parse(action[0].payload.dateRangeUsers.endDate) } },
          ]
        },
      }
      return this.apiService.get('users', filter).pipe(
        exhaustMap(response => {
          if (response.length > 0) {
            return [new generateDownload({ dateRangeUsers: response })];
          }
          else {
            return [
              new setLoadingDownload({ loadingDownload: false }),
              new showSweetAlert({ title: 'Mensaje', text: 'No se encontraron prestamos en el rango de fechas ingresado', type: 'error', buttonText: 'Aceptar' })
            ]
          }
        }
        ),
        catchError((error: HttpErrorResponse) => {
          tap(() => this.spinner.hide())
          return of(new ApiError({ error }));
        })
      );
    })
  );

  @Effect()
  generateDownload$ = this.actions$.pipe(
    ofType<generateDownload>(ActionTypes.generateDownload),
    map((action) => {
      let users: any = []
      action.payload.dateRangeUsers.forEach((user) => {
        users.push({
          "Email": user.email,
          "Nombre": user.name,
          "Primer apellido": user.firstLastname,
          "Segundo apellido": user.secondLastname,
          "Tipo Documento": user.idType,
          "Numero documento": user.idNumber,
          "Fecha nacimiento": new Date(user.birthdate),
          "Fecha registro": new Date(user.created_at),
          "Cantidad prestamos": user.tripCount,
          "Organizacion": user.organization.name
        })
      })

      this.excelService.exportAsExcelFile(users, 'users');
      return new setLoadingDownload({ loadingDownload: false })
    })
  );

  //admins_e_A
  @Effect()
  getUsersAdmins$ = this.actions$.pipe(
    ofType<getUsersAdmins>(ActionTypes.getUsersAdmins),
    exhaustMap(() => {
      let filter = {
        where: {
          or: [
            { roles: { like: '6142c9b6d97a767dbd8ad12f' } },
            { roles: { like: '615f83b163996c6107cb0896' } },
            { roles: { like: '615f83e663996c6107cb0897' } }
          ]
        },
        order: 'updated_at DESC',
        limit: 20,
        include: [{ 'relation': 'organization' }],
        skip: 0
      }
      return this.apiService.get('users', filter).pipe(
        switchMap((usersAdmins) => {
          return [
            new setCurrentPageAdmins({ currentPageAdmins: usersAdmins }),
            new setLoadingAdminSystemsPage({ loadingAdminSystemsPage: false }),
          ]
        })
      )
    })
  );

  //admins_e_A
  @Effect()
  getTotalUsersAdmins$ = this.actions$.pipe(
    ofType<getTotalUsersAdmins>(ActionTypes.getTotalUsersAdmins),
    exhaustMap(() => {
      let filter = {
        or: [{ roles: { like: '6142c9b6d97a767dbd8ad12f' } },
        { roles: { like: '615f83b163996c6107cb0896' } },
        { roles: { like: '615f83e663996c6107cb0897' } }]
      }
      return this.apiService.get('users', filter, { count: true }).pipe(
        switchMap((totalUsersAdmins) => {
          return [
            new setTotalUsersAdmins({ totalUsersAdmins: totalUsersAdmins }),
            new getUsersAdmins(),
          ]
        })
      )
    })
  );

  @Effect()
  showGlobalPageAdmins$ = this.actions$.pipe(
    ofType<showGlobalPageAdmins>(ActionTypes.showGlobalPageAdmins),
    withLatestFrom(this.store$.select((state: State) => state.user.adminPageNumber)),
    exhaustMap((action) => {
      let filter = {
        where: {
          or: [{ roles: { like: '6142c9b6d97a767dbd8ad12f' } },
          { roles: { like: '615f83b163996c6107cb0896' } },
          { roles: { like: '615f83e663996c6107cb0897' } }]
        },
        include: [{ 'relation': 'organization' }],
        limit: 20,
        order: 'updated_at DESC',
        skip: action[1]
      }
      return this.apiService.get('users', filter).pipe(
        switchMap((usersAdmins) => {
          return [
            new setCurrentPageAdmins({ currentPageAdmins: usersAdmins }),
            new setLoadingAdminSystemsPage({ loadingAdminSystemsPage: false }),
          ]
        })
      )
    })
  );

  @Effect()
  createAdminUser$ = this.actions$.pipe(
    ofType<createAdminUser>(ActionTypes.createAdminUser),
    withLatestFrom(this.store$.select((state: State) => state.user.infoFormAdmin)),
    exhaustMap((action: any) => {
      let userAdmin: any = _.cloneDeep(action[1]);
      userAdmin.username = userAdmin.email;
      userAdmin.origin = "dashboard";
      userAdmin.secondLastname = "-";
      userAdmin.idType = "Cédula";
      userAdmin.tripCount = 0;
      userAdmin.favoriteBike = "ninguna";
      userAdmin.position = "ninguna";
      userAdmin.profilePicture = "-";
      userAdmin.documents = "-";
      userAdmin.created_at = new Date();
      userAdmin.updated_at = new Date();
      userAdmin.birthdate = new Date(userAdmin.birthdate);
      userAdmin.roles = `[${userAdmin.roles}]`

      delete userAdmin.repeatPassword;

      return this.apiService.post('users', userAdmin, { setCreateTime: false }).pipe(
        map((usersAdmins) => {
          return new postUserRol({ userRol: { ...usersAdmins, roles: action[1].roles } })
        },
          catchError((error: HttpErrorResponse) => {
            return of(new ApiError({ error }));
          }))
      )

    })
  );

  @Effect()
  postUserRol$ = this.actions$.pipe(
    ofType<postUserRol>(ActionTypes.postUserRol),
    exhaustMap((action) => {
      let userRol = {
        userId: action.payload.userRol.id,
        roleId: action.payload.userRol.roles
      }
      return this.apiService.post('user-roles', userRol, { setCreateTime: true }).pipe(
        exhaustMap(() => {
          return [new getUsersAdmins(), new setCloseAdminSystemsModal({ closeAdminSystemsModal: true })]
        })
      )
    })
  );

  @Effect()
  updateAdminUser$ = this.actions$.pipe(
    ofType<updateAdminUser>(ActionTypes.updateAdminUser),
    withLatestFrom(this.store$.select((state: State) => state.user.infoFormAdmin)),
    exhaustMap((action) => {
      let userEdit: any = _.cloneDeep(action[1]);
      userEdit.roles = `[${userEdit.roles}]`;
      console.log('userEdit',userEdit)
      return this.apiService.patch('users', {id: userEdit.id, roles: userEdit.roles, accountState: userEdit.accountState }, { setDeleteTime: false, setUpdateTime: false }).pipe(
        exhaustMap(() => {
          return [new patchUserRol({userInfo: action[1]})]
        })
      )
    })
  );

  @Effect()
  patchUserRol$ = this.actions$.pipe(
    ofType<patchUserRol>(ActionTypes.patchUserRol),
    exhaustMap((action) => {
      let filter = {
        where: {
          userId: action.payload.userInfo.id
        }
      }
      return this.apiService.get('user-roles', filter).pipe(
        exhaustMap((userRole) => {
          return this.apiService.patch('user-roles', {...userRole[0], roleId: action.payload.userInfo.roles}).pipe(
            exhaustMap(() => {
              return [new getUsersAdmins(), new setCloseAdminSystemsModal({ closeAdminSystemsModal: true })]
            })
          )
        })
      )
    })
  );

  @Effect()
  changePasswordAdminUser$ = this.actions$.pipe(
    ofType<changePasswordAdminUser>(ActionTypes.changePasswordAdminUser),
    withLatestFrom(this.store$.select((state: State) => state.user.infoFormAdmin)),
    exhaustMap((action) => {
      return this.apiService.patch('users', {id: action[1]['id'], password: action[1]['password']}, { setDeleteTime: false, setUpdateTime: false }).pipe(
        exhaustMap(() => {
          return [new getUsersAdmins(), new setCloseAdminSystemsModal({ closeAdminSystemsModal: true })]
        })
      )
    })
  );

  @Effect() //support_f_gmlt
  getMasterListTicket$ = this.actions$.pipe(
    ofType<getMasterListTicket>(ActionTypes.getMasterListTicket),
    exhaustMap((action) => {
      const filter = {
        where: { table: { like: 'tickets' } }
      }
      return this.apiService.get('master-lists', filter).pipe(
        map(masterLists => {
          return new setMasterListTicket({ masterListTicket: masterLists });
        }
        )
      );
    })
  );

  @Effect() //support_f_B
  getTotalTickets$ = this.actions$.pipe(
    ofType<getTotalTickets>(ActionTypes.getTotalTickets),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user)),
    exhaustMap((action) => {
      const filter = { organizationId: action[1]['organizationId'] }
      return this.apiService.get('tickets', filter, { count: true }).pipe(
        exhaustMap(tickets => {
          return [new setTotalTickets({ totalTickets: tickets.count }), new getTicketsPage()];
        }
        )
      );
    })
  );

  @Effect() //support_f_A
  getTicketsPage$ = this.actions$.pipe(
    ofType<getTicketsPage>(ActionTypes.getTicketsPage),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user), this.store$.select((state: State) => state.apiData.ticketPageSIze)),
    exhaustMap((action) => {
      let ticketData = {
        "pageNumber": action[2]['pageNumber'],
        "organizationId": action[1]['organizationId'],
        "limit": action[2]['limit']
      }
      return this.apiService.post('getTicketsPage', ticketData).pipe(
        exhaustMap((response) => {
          return [new setCurrentPageTickets({ currentPageTickets: response }), new setLoadingTicketsTable({ loadingTicketsTable: false })]
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
    })
  );

  @Effect() //support_f_E
  patchTicket$ = this.actions$.pipe(
    ofType<patchTicket>(ActionTypes.patchTicket),
    withLatestFrom(this.store$.select((state: State) => state.apiData.selectedTicketInfo),
      this.store$.select((state: State) => state.apiData.ticketsSearchResult),
      this.store$.select((state: State) => state.apiData.ticketPageSIze),
      this.store$.select((state: State) => state.apiData.ticketSearchStatus)),
    exhaustMap((action) => {
      this.spinner.show()
      let state = {
        id: action[1]['id'],
        state: action[0].payload.state
      }
      return this.apiService.patch('tickets', state, { setUpdateTime: true }).pipe(
        exhaustMap((response) => {
          this.spinner.hide()
          if (action[4]) {
            let tickets = _.cloneDeep(action[2])
            tickets.forEach(tickectSearch => {
              if (tickectSearch.id == action[1]['id']) {
                return tickectSearch.state = action[0].payload.state
              }
            });
            return [
              new setTicketsSearchResult({ ticketsSearchResult: tickets }),
              new showSearchPageTickets()
            ]
          } else {
            return [new getTotalTickets(), new setTicketSearchStatus({ ticketSearchStatus: false }),
            new setCloseModalTickets({ closeModalTickets: true })]
          }
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
    })
  );

  @Effect() //support_f_C
  searchTickets$ = this.actions$.pipe(
    ofType<searchTickets>(ActionTypes.searchTickets),
    withLatestFrom(this.store$.select((state: State) => state.apiData.user), this.store$.select((state: State) => state.apiData.searchWordTickets)),
    exhaustMap((action) => {
      let searchTickets = {
        "userInfo": action[2],
        "organizationId": action[1]['organizationId']
      }
      return this.apiService.post('searchTicketsByUser', searchTickets).pipe(
        exhaustMap((searchTickets) => {
          return [
            new setTotalTickets({ totalTickets: searchTickets.length }),
            new setTicketsSearchResult({ ticketsSearchResult: searchTickets }),
            new showSearchPageTickets()
          ]
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
    })
  );

  @Effect() //support_f_G
  showPageTicket$ = this.actions$.pipe(
    ofType<showPageTicket>(ActionTypes.showPageTicket),
    withLatestFrom((this.store$.select((state: State) => state.apiData.ticketSearchStatus))),
    map((action) => {
      if (action[1]) {
        return new showSearchPageTickets();
      }
      else {
        return new getTicketsPage();
      }
    })
  );

  @Effect() //support_f_F
  showSearchPageTickets$ = this.actions$.pipe(
    ofType<showSearchPageTickets>(ActionTypes.showSearchPageTickets),
    withLatestFrom(this.store$.select((state: State) => state.apiData.ticketPageSIze), this.store$.select((state: State) => state.apiData.ticketsSearchResult)),
    exhaustMap((action) => {
      let dataSearch = action[2].slice(action[1]['pageNumber'], (action[1]['pageNumber'] + action[1]['limit']))
      return [new setCurrentPageTickets({ currentPageTickets: dataSearch }), new setLoadingTicketsTable({ loadingTicketsTable: false })]
    })
  );

  @Effect() //support_f_D
  getStationsTicket$ = this.actions$.pipe(
    ofType<getStationsTicket>(ActionTypes.getStationsTicket),
    withLatestFrom(this.store$.select((state: State) => state.apiData.selectedTicketInfo)),
    exhaustMap((action) => {
      let ticket = _.cloneDeep(action[1]);
      let stationIds: Array<any> = [];
      if (ticket['trip'].endStationId) {
        stationIds.push(ticket['trip'].endStationId)
      };
      if (ticket['trip'].startStationId) {
        stationIds.push(ticket['trip'].startStationId)
      };
      let filter = {
        where: { id: { inq: stationIds } }
      };
      return this.apiService.get('stations', filter).pipe(
        map((stations) => {
          stations.forEach(station => {
            if (station.id == ticket['trip'].endStationId) {
              ticket['trip'].endStation = station.name
            };
            if (ticket['trip'].startStationId && station.id == ticket['trip'].startStationId) {
              ticket['trip'].startStation = station.name
            };
          });
          return new setSelectedTicketInfo({ selectedTicketInfo: ticket })
        }),
        catchError((error: HttpErrorResponse) => {
          return of(new ApiError({ error }))
        })
      )
    })
  );
}


export const effects = [
  GeneralEffects,
  UserEffects,
  AuthEffects,
  ValidationsEffects,
  LogicEffects
];