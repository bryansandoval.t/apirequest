export interface State {
    loginFormErrors: any;
}

const initialState: State = {
    loginFormErrors: {}
};

export function authReducer(state = initialState, action): State {
    switch (action.type) {
        default: {
            return state;
        }
    }
}
