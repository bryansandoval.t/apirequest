//import { ActionTypes, actions } from '../global.actions';
import { Actions } from '@ngrx/effects';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { bikesharingNav } from 'src/app/models/route.model';
import * as GlobalActions from '../global.actions';
export interface State {
  'user-infos': any;
  'loadingDownload': boolean;
  'user-infosCount': number;
  'user-infosSearch': any;
  'user-infosSearchCount': number;
  'permissions': any;
  'usersFinalUser': any;
  'modalUser': any;
  'userTrips': any;
  'penaltyDates': any;
  'penaltyTypes': any;
  'userPagination': any;
  'userPaginationTrip': any;
  'setSearchTerm': any;
  'searchTerm': any;
  'datasourceSearch': any;
  'totalGetTrip': any;
  'setSearchTermTrip': any;
  'setBikes': any;
  'stations': any;
  'stationsBikes': any;
  'typesBikes': any;
  'stateBikes': any;
  'modalTypeBike': any;
  'modalBike': any;
  'selectedLock': any;
  'searchBikesForm': any;
  'setcurrentPageBike': any;
  'saveLocksBikes': any;
  'totalBikes': any;
  'totalLockBikes': any;
  'currentPageLocksBike': any;
  'searchLocksBikesForm': any;
  'bikesSearchResult': any;
  'locksBikesSearchResult': any;
  'setBikesSearch': any;
  //orga_v_A
  'tableOrganizations': any;
  //orga_v_B
  'organizationFormErrors': any;
  //orga_v_C
  'modalOrganization': any;
  //orga_v_D
  'modalOrganizationType': string;
  //orga_v_E
  'organizationLogo': object;

  'closeModalOrganization': boolean;
  'bikePageSize': any;
  'skipPageBike': any;
  'locksPageSize': any;
  'skipPageLocks': any;
  'totalLocksBikes': any;
  'datasourceSearchTrip': any;
  'stationsTrips': any;
  //trip_v_A
  'searchTripsForm': any;
  //trip_v_B
  'tripStates': Array<any>;
  //trip_v_C
  'currentPageTrips': Array<any>;
  //trip_v_D
  'tripsPageSize': number;
  //trip_v_E
  'totalTrips': number;
  //trip_v_F
  'tripsSearchResult': Array<any>;
  //trip_v_G
  'modalTrip': any;
  //trip_v_H
  'tripsFormErrors': Object;
  //trip_v_J
  'tripsPageNumber': number;

  //admins
  'closeAdminSystemsModal': boolean; //admins_v_A
  'loadingAdminSystemsPage': boolean; //admins_v_B
  'adminSystemsFormStatus': boolean; //admins_v_C
  'totalUsersAdmins': number; //admins_v_D
  'adminSystemsFormErrors': object; //admins_v_E
  'modalAdminSystems': object; //admins_v_F
  'currentPageAdmins': Array<any>; //admins_v_G
  'listRoles': Array<any>; //admins_v_H
  'listOrganizations': Array<any>; //admins_v_I
  'infoFormAdmin': object; //admins_v_J
  'adminFormValidation': string; //admins_v_K
  'masterList': Array<any>;//admins_v_L
  'adminPageNumber': number;//admins_v_M
  //lock_
  'loadingLockTable': boolean;//lock_v_A
  'totalLocks': number;//lock_v_B
  'currentPageLocks': Array<any>;//lock_v_C
  'selectedLockInfo': Object;//lock_v_D
  'lockPageNumber': number;//lock_v_E
  'currentPageLockCommands': Array<any>;//lock_v_F
  'loadingCommandsTable': boolean;//lock_v_G
  'lockPageSize': number;//lock_v_H
  'rolUserLocks': string;//lock_v_I
  'locksSearchParams': Object;//lock_v_J
  'locksSearchResult': Array<any>;//lock_v_K
  'commandsPageSize': number;//lock_v_L
  'totalLockCommands': number;//lock_v_M
  'editLockFormErrors': Object;//lock_v_N
  'lockCommandsPageNumber': number;//lock_v_O
  'closeModalLocks': boolean,
}

const initialState: State = {
  'user-infos': [],
  'loadingDownload': false,
  'user-infosCount': 0,
  'user-infosSearch': [],
  'user-infosSearchCount': 0,
  'usersFinalUser': [],
  'modalUser': {},
  'userTrips': [],
  'penaltyDates': {},
  'penaltyTypes': [],
  'userPagination': {
    totalPage: 0,
    currentPage: 1,
    cantLeft: false,
    cantRigth: true,
    pageSize: 20,
    limit: 20,
    skip: 0
  },
  'userPaginationTrip': {
    totalPage: 0,
    currentPage: 1,
    cantLeft: false,
    cantRigth: true,
    pageSize: 20,
    limit: 20,
    skip: 0
  },
  'setSearchTerm': [],
  'searchTerm': "",
  'datasourceSearch': [],
  'totalGetTrip': "",
  'setSearchTermTrip': [],
  'datasourceSearchTrip': [],
  'stationsTrips': [],
  'permissions': [],
  'setBikes': [],
  'stations': [],
  'stationsBikes': [],
  'typesBikes': [],
  'stateBikes': [],
  'modalTypeBike': "crear",
  'modalBike': {},
  'selectedLock': {},
  'totalLocksBikes': 0,
  //orga_v_A
  'tableOrganizations': [],
  //orga_v_B  
  'organizationFormErrors': {
    name: { error: false, touched: false, errorMessage: "" },
    logo: { error: false, touched: false, errorMessage: "" },
    linkAppstore: { error: false, touched: false, errorMessage: "" },
    linkPlaystore: { error: false, touched: false, errorMessage: "" },
  },
  //orga_v_C
  'modalOrganization': {},
  //orga_v_D
  'modalOrganizationType': "create",
  //orga_v_E
  'organizationLogo': {
    deberíaCargar: false,
    logoInfo: ''
  },
  //orga_v_F
  'closeModalOrganization': false,
  'searchBikesForm': {
    isSearch: false,
    imeiOqr: "",
    station: ""
  },
  'setcurrentPageBike': 1,
  'saveLocksBikes': [],
  'totalBikes': 0,
  'totalLockBikes': 0,
  'currentPageLocksBike': 1,
  'searchLocksBikesForm': {
    isSearch: false,
    imeiOqr: ""
  },
  'bikesSearchResult': [],
  'locksBikesSearchResult': 0,
  'setBikesSearch': [],
  'bikePageSize': 20,
  'skipPageBike': 0,
  'locksPageSize': 20,
  'skipPageLocks': 0,
  //trips
  //trip_v_A
  'searchTripsForm': {},
  //trip_v_B
  'tripStates': [],
  //trip_v_C
  'currentPageTrips': [],
  //trip_v_D
  'tripsPageSize': 0,
  //trip_v_E
  'totalTrips': 0,
  //trip_v_F
  'tripsSearchResult': [],
  //trip_v_G
  'modalTrip': {},
  //trip_v_H
  'tripsFormErrors': {},
  //trip_v_J
  'tripsPageNumber': 0,

  //admins
  'closeAdminSystemsModal': false, //admins_v_A
  'loadingAdminSystemsPage': false, //admins_v_B
  'adminSystemsFormStatus': false, //admins_v_C
  'totalUsersAdmins': 0, //admins_V_D
  'adminSystemsFormErrors': {}, //admins_v_E
  'modalAdminSystems': {}, //admins_v_F 
  'currentPageAdmins': [], //admins_v_G
  'listRoles': [], //admins_v_H
  'listOrganizations': [], //admins_v_I
  'infoFormAdmin': {}, //admins_v_J
  'adminFormValidation': "", //admins_v_K
  'masterList': [],//admins_v_L
  'adminPageNumber': 0,//admins_v_M
  //lock_
  'loadingLockTable': false,//lock_v_A
  'totalLocks': 0,//lock_v_B
  'currentPageLocks': [],//lock_v_C
  'selectedLockInfo': {},//lock_v_D
  'lockPageNumber': 0,//lock_v_E
  'currentPageLockCommands': [],//lock_v_F
  'loadingCommandsTable': false,//lock_v_G
  'lockPageSize': 20,//lock_v_H
  'rolUserLocks': "",//lock_v_I
  'locksSearchParams': {
    searchTerm:'',
    isSearching:false,
    locksWithoutBikeState: false
  },//lock_v_J
  'locksSearchResult': [],//lock_v_K
  'commandsPageSize': 20,//lock_v_L
  'totalLockCommands': 0,//lock_v_M
  'editLockFormErrors': {
    qr: '',
    mac: '',
    simNumber: '',
  },//lock_v_N
  'lockCommandsPageNumber': 0,//lock_v_O
  'closeModalLocks': false,//lock_v_P
};


export function userReducer(state = initialState, action: GlobalActions.actions): State {
  switch (action.type) {
    case GlobalActions.ActionTypes.setEditLockFormErrors: //lock_r_N
      return {
        ...state,
        editLockFormErrors: action.payload.editLockFormErrors
      };
    case GlobalActions.ActionTypes.setLockCommandsPageNumber: //lock_r_O
      return {
        ...state,
        lockCommandsPageNumber: action.payload.lockCommandsPageNumber
      };
    case GlobalActions.ActionTypes.setCloseModalLocks: //lock_r_P
      return {
        ...state,
        closeModalLocks: action.payload.closeModalLocks
      };
    case GlobalActions.ActionTypes.setTotalLockCommands: //lock_r_M
      return {
        ...state,
        totalLockCommands: action.payload.totalLockCommands
      };
    case GlobalActions.ActionTypes.setLocksSearchResult: //lock_r_K
      return {
        ...state,
        locksSearchResult: action.payload.locksSearchResult
      };
    case GlobalActions.ActionTypes.setCommandsPageSize: //lock_r_L
      return {
        ...state,
        commandsPageSize: action.payload.commandsPageSize
      };
    case GlobalActions.ActionTypes.rolUserLocks: //lock_r_I
      return {
        ...state,
        rolUserLocks: action.payload.rol
      };
    case GlobalActions.ActionTypes.setLocksSearchParams: //lock_r_J
      return {
        ...state,
        locksSearchParams: action.payload.locksSearchParams
      };
    case GlobalActions.ActionTypes.setLoadingCommandsTable: //lock_r_G
      return {
        ...state,
        loadingCommandsTable: action.payload.loadingCommandsTable
      };
    case GlobalActions.ActionTypes.setLockPageSize: //lock_r_H
      return {
        ...state,
        lockPageSize: action.payload.lockPageSize
      };
    case GlobalActions.ActionTypes.setLockPageNumber: //lock_r_E
      return {
        ...state,
        lockPageNumber: action.payload.lockPageNumber
      };
    case GlobalActions.ActionTypes.setTotalLocks: //lock_r_B
      return {
        ...state,
        totalLocks: action.payload.totalLocks
      };
    case GlobalActions.ActionTypes.setLoadingLockTable: //lock_r_A
      return {
        ...state,
        loadingLockTable: action.payload.loadingLockTable
      };
    case GlobalActions.ActionTypes.setSelectedLockInfo: //lock_r_D
      return {
        ...state,
        selectedLockInfo: action.payload.selectedLockInfo
      };
    case GlobalActions.ActionTypes.setCurrentPageLock: //lock_r_C
      return {
        ...state,
        currentPageLocks: action.payload.currentPageLocks
      };
    case GlobalActions.ActionTypes.setCurrentPageLockCommands: //lock_r_F
      return {
        ...state,
        currentPageLockCommands: action.payload.currentPageLockCommands
      };
    case GlobalActions.ActionTypes.setPermissions:
      return {
        ...state,
        permissions: action.payload.permissions
      };
    case GlobalActions.ActionTypes.serverError:
      break;
    default: {
      return state;
    };
    case GlobalActions.ActionTypes.saveUsers:
      return {
        ...state,
        usersFinalUser: action.payload.users
      }
    case GlobalActions.ActionTypes.setCloseModalOrganization:
      return { //orga_r_F
        ...state,
        closeModalOrganization: action.payload.closeModalOrganization
      }
    case GlobalActions.ActionTypes.modalUser:
      return {
        ...state,
        modalUser: action.payload.user
      }
    case GlobalActions.ActionTypes.saveUserTrips:
      return {
        ...state,
        userTrips: action.payload.trips
      }
    case GlobalActions.ActionTypes.penaltyDates:
      return {
        ...state,
        penaltyDates: action.payload.dates
      }
    case GlobalActions.ActionTypes.penaltyTypes:
      return {
        ...state,
        penaltyTypes: action.payload.penaltys
      }
    case GlobalActions.ActionTypes.userPagination:
      return {
        ...state,
        userPagination: action.payload.pagination
      }
    case GlobalActions.ActionTypes.userPaginationTrip:
      return {
        ...state,
        userPaginationTrip: action.payload.pagination
      }
    case GlobalActions.ActionTypes.setSearchTerm:
      return {
        ...state,
        setSearchTerm: action.payload.resultSearch
      }

    case GlobalActions.ActionTypes.setSearchTermTrip:
      return {
        ...state,
        setSearchTermTrip: action.payload.resultSearch
      }
    case GlobalActions.ActionTypes.searchTerm:
      return {
        ...state,
        searchTerm: action.payload.term
      }
    case GlobalActions.ActionTypes.datasourceSearch:
      return {
        ...state,
        datasourceSearch: action.payload.sliceSearch
      }

    case GlobalActions.ActionTypes.datasourceSearchTrip:
      return {
        ...state,
        datasourceSearchTrip: action.payload.sliceSearch
      }

    case GlobalActions.ActionTypes.setLoadingDownload:
      return {
        ...state,
        loadingDownload: action.payload.loadingDownload
      }

    case GlobalActions.ActionTypes.totalGetTrip:
      return {
        ...state,
        totalGetTrip: action.payload.userId
      }

    case GlobalActions.ActionTypes.stationsTrips:
      return {
        ...state,
        stationsTrips: action.payload.stations
      }
    //orga_r_A
    case GlobalActions.ActionTypes.setTableOrganization:
      return {
        ...state,
        tableOrganizations: action.payload.tableOrganizations
      }

    //orga_r_B
    case GlobalActions.ActionTypes.setOrganizationFormErrors:
      return {
        ...state,
        organizationFormErrors: action.payload.organizationFormErrors
      };
    //orga_r_C
    case GlobalActions.ActionTypes.setModalOrganization:
      return {
        ...state,
        modalOrganization: action.payload.modalOrganization
      };

    //orga_r_D
    case GlobalActions.ActionTypes.setModalOrganizationType:
      return {
        ...state,
        modalOrganizationType: action.payload.modalOrganizationType
      };

    //orga_r_E
    case GlobalActions.ActionTypes.setOrganizationLogo:
      return {
        ...state,
        organizationLogo: action.payload.organizationLogo
      };
    case GlobalActions.ActionTypes.setBikes:
      return {
        ...state,
        setBikes: action.payload.bikes
      }

    case GlobalActions.ActionTypes.saveStationsBikes:
      return {
        ...state,
        stationsBikes: action.payload.stations
      }

    case GlobalActions.ActionTypes.saveTypesBikes:
      return {
        ...state,
        typesBikes: action.payload.types
      }

    case GlobalActions.ActionTypes.saveStateBikes:
      return {
        ...state,
        stateBikes: action.payload.state
      }

    case GlobalActions.ActionTypes.setBikesSearch:
      return {
        ...state,
        searchBikesForm: action.payload.bikes
      }

    case GlobalActions.ActionTypes.modalTypeBike:
      return {
        ...state,
        modalTypeBike: action.payload.type
      }

    case GlobalActions.ActionTypes.searchBikeForm:
      return {
        ...state,
        searchBikesForm: action.payload.form
      }

    case GlobalActions.ActionTypes.setCurrentPageBike:
      return {
        ...state,
        setcurrentPageBike: action.payload.currentPage
      }

    case GlobalActions.ActionTypes.modalBike:
      return {
        ...state,
        modalBike: action.payload.bike
      }

    case GlobalActions.ActionTypes.selectedLock:
      return {
        ...state,
        selectedLock: action.payload.lock
      }

    case GlobalActions.ActionTypes.saveLocksBikes:
      return {
        ...state,
        saveLocksBikes: action.payload.locks
      }

    case GlobalActions.ActionTypes.totalBikes:
      return {
        ...state,
        totalBikes: action.payload.totalBikes
      }

    case GlobalActions.ActionTypes.totalLockBikes:
      return {
        ...state,
        totalLocksBikes: action.payload.totalLocks
      }

    case GlobalActions.ActionTypes.setCurrentPageLocksBike:
      return {
        ...state,
        currentPageLocksBike: action.payload.currentPage
      }

    case GlobalActions.ActionTypes.searchLocksBikesForm:
      return {
        ...state,
        searchLocksBikesForm: action.payload.form
      }

    case GlobalActions.ActionTypes.bikesSearchResult:
      return {
        ...state,
        bikesSearchResult: action.payload.bikesSearch
      }

    case GlobalActions.ActionTypes.locksBikesSearchResult:
      return {
        ...state,
        locksBikesSearchResult: action.payload.locksSearch
      }

    case GlobalActions.ActionTypes.skipPageBike:
      return {
        ...state,
        skipPageBike: action.payload.skip
      }

    case GlobalActions.ActionTypes.skipPageLocks:
      return {
        ...state,
        skipPageLocks: action.payload.skip
      }

    //trip_r_A
    case GlobalActions.ActionTypes.setTotalTrips:
      return {
        ...state,
        totalTrips: action.payload.totalTrips
      }

    //trip_r_B
    case GlobalActions.ActionTypes.setTripStates:
      return {
        ...state,
        tripStates: action.payload.tripState
      }

    //trip_r_D
    case GlobalActions.ActionTypes.setTripsSearchResult:
      return {
        ...state,
        tripsSearchResult: action.payload.tripsSearchResult
      }

    //trip_r_E
    case GlobalActions.ActionTypes.setSearchTripsForm:
      return {
        ...state,
        searchTripsForm: action.payload.searchTripsForm
      }

    //trip_r_F
    case GlobalActions.ActionTypes.setCurrentPageTrips:
      return {
        ...state,
        currentPageTrips: action.payload.currentPageTrips
      }

    //trip_r_H
    case GlobalActions.ActionTypes.setTripsFormErrors:
      return {
        ...state,
        tripsFormErrors: action.payload.tripsFormErrors
      }

    //trip_r_I
    case GlobalActions.ActionTypes.setModalTrip:
      return {
        ...state,
        modalTrip: action.payload.modalTrip
      }

    //trip_r_J
    case GlobalActions.ActionTypes.setTripsPageNumber:
      return {
        ...state,
        tripsPageNumber: action.payload.tripsPageNumber
      }

    //admins
    //admins_r_A
    case GlobalActions.ActionTypes.setListOrganizations:
      return {
        ...state,
        listOrganizations: action.payload.listOrganizations
      }

    //admins_r_B
    case GlobalActions.ActionTypes.setListRoles:
      return {
        ...state,
        listRoles: action.payload.listRoles
      }

    //admins_r_C
    case GlobalActions.ActionTypes.setCurrentPageAdmins:
      return {
        ...state,
        currentPageAdmins: action.payload.currentPageAdmins
      }

    //admins_r_D
    case GlobalActions.ActionTypes.setModalAdminSystems:
      return {
        ...state,
        modalAdminSystems: action.payload.modalAdminSystems
      }

    case GlobalActions.ActionTypes.setInfoFormAdmin:
      return {
        ...state,
        infoFormAdmin: action.payload.infoFormAdmin
      }

    //admins_r_E
    case GlobalActions.ActionTypes.setAdminSystemsFormErrors:
      return {
        ...state,
        adminSystemsFormErrors: action.payload.adminSystemsFormErrors
      }

    //admins_r_F
    case GlobalActions.ActionTypes.setTotalUsersAdmins:
      return {
        ...state,
        totalUsersAdmins: action.payload.totalUsersAdmins
      }

    //admins_r_G
    case GlobalActions.ActionTypes.setAdminSystemsFormStatus:
      return {
        ...state,
        adminSystemsFormStatus: action.payload.adminSystemsFormStatus
      }

    //admins_r_H
    case GlobalActions.ActionTypes.setLoadingAdminSystemsPage:
      return {
        ...state,
        loadingAdminSystemsPage: action.payload.loadingAdminSystemsPage
      }

    //admins_r_I
    case GlobalActions.ActionTypes.setCloseAdminSystemsModal:
      return {
        ...state,
        closeAdminSystemsModal: action.payload.closeAdminSystemsModal
      }

    //admins_r_J
    case GlobalActions.ActionTypes.setAdminFormValidation:
      return {
        ...state,
        adminFormValidation: action.payload.adminFormValidation
      }
    //admins_r_K
    case GlobalActions.ActionTypes.setMasterList:
      return {
        ...state,
        masterList: action.payload.masterList
      }

    //admins_L
    case GlobalActions.ActionTypes.showPageAdmin:
      return {
        ...state,
        adminPageNumber: action.payload.adminPageNumber
      }
  }
}

