import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ActionTypes, actions } from '../global.actions';

export interface State {

}

const initialState: State = {

};

export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
    if (formGroup.get('password').value === formGroup.get('repeatPassword').value)
        return null;
    else
        return { passwordMismatch: true };
};

export function interfaceReducer(state = initialState, action: actions): State {
    switch (action.type) {
        /* case ActionTypes.serverError:
            break; */

        default: {
            return state;
        }
    }
}