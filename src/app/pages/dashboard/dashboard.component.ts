import { Component, OnInit } from '@angular/core';
import { ROUTE_TRANSITION } from '../../app.animation';
import { Store } from '@ngrx/store';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'elastic-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class DashboardComponent implements OnInit {

  width = {
    single: (100) + '%',
    double: (100 / 2) + '%',
    triple: (100 / 3) + '%',
  };

  userInfo: User;
  public username = '';

  constructor(private store: Store) { }

  ngOnInit() {
    this.store.select((state) => state['apiData']['user']).subscribe((elem: User) => {
      this.userInfo = elem;
      if (this.userInfo) {
        this.username = this.userInfo.username;
      }
    });
  }

}
