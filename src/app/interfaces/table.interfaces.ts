import { FormModel, DispatchPosibleActions } from '../interfaces/form.interfaces';
import { Observable } from 'rxjs';
import { tableTypes } from '../constants/table.constants';

export interface TableInfo {
  tableColumns?: Array<TableColumns>;
  filterInclude?: any;
  filterProperties?: any;
}

export interface TableColumns {
  name: string;
  property: any;
  visible: boolean;
  isModelProperty?: boolean;
  displayFn?: any,
  state?:any;
  isCOP?: boolean,
  addHours?: boolean;
  stateBoolean?: boolean;
  img?: boolean;
  typeLink?: boolean;
}

export interface Column {
  header: string;
  propertyPath: string;
  type: string;
  width?: string;
  decimals?: number;
  dateFormater?(d:Date): string;
  parser?(d:any): any;
  labels?: Array<any>;
  actions?: Array<any>;
  buttonAdd?: Array<any>;
  isCurrency?: boolean;
}

export const tripsColumns = [
  {
    header: 'Fecha inicio',
    propertyPath: 'startDate',
    type: tableTypes.dateTime
  },
  {
    header: 'Estacion inicio',
    propertyPath: 'startStation.name',
    type: tableTypes.string,
  },
  {
    header: 'Fecha fin',
    propertyPath: 'endDate',
    type: tableTypes.dateTime
  },
  {
    header: 'Estacion fin',
    propertyPath: 'endStation.name',
    type: tableTypes.string,
  },
  {
    header: 'Email',
    propertyPath: 'user.email',
    type: tableTypes.string,
    width: "195px"
  },
  {
    header: 'N° Identificación',
    propertyPath: 'user.idNumber',
    type: tableTypes.string,
    width: "195px"
  },
  {
    header: 'Bicicleta',
    propertyPath: 'bike.number',
    type: tableTypes.string,
    width: "100px"
  },
  {
    header: 'Tipo Bicicleta',
    propertyPath: 'bike.type',
    type: tableTypes.label,
    labels: [
      { value: 'electric', label: 'Electrica', bgcolor: '#b3e5fc', fontColor: '#23547b' },
      { value: 'mechanic', label: 'Mecanica', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'error', label: 'Error', bgcolor: '#ffcdd2', fontColor: '#c63737' }
    ]
  },
  {
    header: 'Estado',
    propertyPath: 'state',
    type: tableTypes.label,
    labels: [
      { value: 'active', label: 'Activo', bgcolor: 'rgb(165 241 181)', fontColor: '#c63737' },
      { value: 'help', label: 'Ayuda', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'survey', label: 'Encuesta', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'finished', label: 'Finalizado', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'finishing', label: 'Finalizando', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'error', label: 'Error', bgcolor: '#ffcdd2', fontColor: '#c63737' }
    ]
  },
  {
    header: 'Acciones',
    propertyPath: '',
    type: tableTypes.actions,
    width: "100px",
    actions: [
      { label: 'Editar', type: 'edit' },
    ]
  }
];

//inicializar columnas
export const adminColumns = [
  {
    header: 'Nombre',
    propertyPath: 'name',
    type: tableTypes.string
  },
  {
    header: 'Apellido',
    propertyPath: 'firstLastname',
    type: tableTypes.string
  },
  {
    header: 'Correo',
    propertyPath: 'email',
    type: tableTypes.string,
  },
  {
    header: 'Cédula',
    propertyPath: 'idNumber',
    type: tableTypes.string
  },
  {
    header: 'Fecha de nacimiento',
    propertyPath: 'birthdate',
    type: tableTypes.date,
  },
  {
    header: 'Organización',
    propertyPath: 'organization.name',
    type: tableTypes.string,
  },
  {
    header: 'Estado',
    propertyPath: 'accountState',
    type: tableTypes.label,
    labels: [
      { value: 'active', label: 'Activo', bgcolor: 'rgb(165 241 181)', fontColor: '#c63737' },
      { value: 'inactive', label: 'Inactivo', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'error', label: 'Error', bgcolor: '#ffcdd2', fontColor: '#c63737' }
    ]
  },
  {
    header: 'Acciones',
    propertyPath: '',
    type: tableTypes.actions,
    width: "100px",
    actions: [
      { label: 'Editar', type: 'edit' },
      { label: 'Cambiar contraseña', type: 'changePassword' },
    ]
  }
];

export const locksColumns = [
  {
    header: 'IMEI',
    propertyPath: 'imei',
    type: tableTypes.string
  },
  {
    header: 'Qr',
    propertyPath: 'qrNumber',
    type: tableTypes.string,
  },
  {
    header: 'Estado de carga',
    propertyPath: 'battery',
    type: tableTypes.string
  },
  {
    header: 'Fecha del último comando',
    propertyPath: 'lastCommandDate',
    type: tableTypes.dateTime,
  },
  {
    header: 'Último comando enviado',
    propertyPath: 'lockCommands.command',
    type: tableTypes.string,
  },
  {
    header: 'Mac',
    propertyPath: 'mac',
    type: tableTypes.string,
  },
  {
    header: 'Bicicleta',
    propertyPath: 'bike.number',
    type: tableTypes.string,
  },
  {
    header: 'Organización',
    propertyPath: 'organization.name',
    type: tableTypes.string,
  },
  {
    header: 'Acciones',
    propertyPath: '',
    type: tableTypes.actions,
    width: "100px",
    actions: [
      { label: 'Historial', type: 'history' },
      { label: 'Editar', type: 'edit' },
    ]
  }
]

export const historysColumns = [
  {
    header: 'Fecha',
    propertyPath: 'date',
    type: tableTypes.dateTime,
  },
  {
    header: 'IMEI',
    propertyPath: 'lock.imei',
    type: tableTypes.string
  },
  {
    header: 'Commentario',
    propertyPath: 'commmand',
    type: tableTypes.string
  },
  {
    header: 'Dirección',
    propertyPath: 'direction',
    type: tableTypes.string,
  },
  {
    header: 'Estado de batería',
    propertyPath: 'lock.battery',
    type: tableTypes.string,
  },
  {
    header: 'Geolocación',
    propertyPath: 'geoinfo',
    type: tableTypes.string,
  },
  {
    header: 'Señal',
    propertyPath: 'signal',
    type: tableTypes.string,
  },
  {
    header: 'Estado del candado',
    propertyPath: 'lockStatus',
    type: tableTypes.string,
  },
]

export const ticketsColumns = [
  {
    header: 'Nombre',
    propertyPath: 'user.name',
    type: tableTypes.string,
    width: "100px"
  },
  {
    header: 'Apellido',
    propertyPath: 'user.firstLastname',
    type: tableTypes.string,
    width: "100px"
  },
  {
    header: 'Email',
    propertyPath: 'user.email',
    type: tableTypes.string,
    width: "200px"
  },
  {
    header: 'Cédula',
    propertyPath: 'user.idNumber',
    type: tableTypes.string,
    width: "200px"
  },
  {
    header: 'Estado de ticket',
    propertyPath: 'state',
    type: tableTypes.label,
    labels: [
      { value: 'solved', label: 'Solucionado', bgcolor: 'rgb(165 241 181)', fontColor: '#c63737' },
      { value: 'created', label: 'Creado', bgcolor: '#ffcdd2', fontColor: '#c63737' },
      { value: 'error', label: 'Error', bgcolor: '#ffcdd2', fontColor: '#c63737' }
    ]
  },
  {
    header: 'Fecha de creación',
    propertyPath: 'createdAt',
    type: tableTypes.dateTime
  },
  {
    header: 'Tipo de problema',
    propertyPath: 'type',
    type: tableTypes.string,
  },
  {
    header: 'Descripción del problema',
    propertyPath: 'description',
    type: tableTypes.string,
  },
  {
    header: 'Número de bicicleta',
    propertyPath: 'trip.bike.number',
    type: tableTypes.string,
    width: "100px"
  },
  {
    header: 'IMEI Candado',
    propertyPath: 'trip.bike.lock.imei',
    type: tableTypes.string,
  },
  {
    header: 'Acciones',
    propertyPath: '',
    type: tableTypes.actions,
    width: "100px",
    actions: [
      { label: 'Revisar ticket', type: 'watchTicket' },
    ]
  }
]
//tripUser
export const tripUserModal: TableInfo = {
  tableColumns: [
   // { name: 'Id', property: 'id', visible: true, isModelProperty: true },
    { name: 'Fecha Inicio', property: 'startDate', visible: true, isModelProperty: true, isCOP:true },
    { name: 'Fecha Fin', property: 'endDate', visible: true, isModelProperty: true, isCOP:true },
    { name: 'Estacion Inicio', property: 'fromStation', visible: true, isModelProperty: true},
    { name: 'Estacion Fin', property: 'toStation', visible: true, isModelProperty: true},
    { name: 'Bicicleta', property: 'numberBike', visible: true, isModelProperty: true},
    { name: 'Tipo Bicicleta', property: 'typeBike', visible: true, isModelProperty: true},
    { name: 'Estado', property: 'state', visible: true, isModelProperty: true , state:true},
  ],
  filterInclude: [
    { relation: 'bike' }
  ],

};

// Users Finals
export const TestInfoTable: TableInfo = {
  tableColumns: [
    { name: 'Nombre', property: 'name', visible: true, isModelProperty: true },
    { name: 'Apellido', property: 'lastname', visible: true, isModelProperty: true },
    { name: 'Correo', property: 'email', visible: true, isModelProperty: true },
    //{ name: 'Rol', property: 'user.rol', visible: true, isModelProperty: true },
    { name: 'Cedula', property: 'idNumber', visible: true, isModelProperty: true},
    { name: 'Fecha Nacimiento', property: 'birthdate', visible: true, isModelProperty: true, isCOP:true},
    { name: 'genero', property: 'gender', visible: true, isModelProperty: true},
    { name: 'recorridos', property: 'tripCount', visible: true, isModelProperty: true},
    { name: 'Estado', property: 'accountState', visible: true, isModelProperty: true, state:true},
    { name: 'Sanciones', property: 'penaltyInfo', visible: true, isModelProperty: true },
    { name: 'Acciones', property: 'actions', visible: true },
  ],
  filterInclude: [
    { relation: 'penalties' }
  ],

};

//Tabla de prestamos
export const TripsTable: TableInfo = {
  tableColumns: [
    { name: 'Id', property: 'id', visible: true, isModelProperty: true },
    { name: 'Fecha inicio', property: 'startDate', visible: true, isModelProperty: true },
    { name: 'Fecha fin', property: 'endDate', visible: true, isModelProperty: true },
    { name: 'Estacion inicio', property: 'startStation', visible: true, isModelProperty: true },
    { name: 'Estacion fin', property: 'endStation', visible: true, isModelProperty: true },
    { name: 'Usuario', property: 'user', visible: true, isModelProperty: true },
    { name: 'Bicicleta', property: 'bike', visible: true, isModelProperty: true },
    { name: 'Tipo Bicicleta', property: 'bikeType', visible: true, isModelProperty: true },
    { name: 'Estado', property: 'state', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],

};


//Tabla de Bicis
export const BikesTable: TableInfo = {
  tableColumns: [
    { name: 'Numero', property: 'id', visible: true, isModelProperty: true },
    { name: 'Tipo Bicicleta', property: 'bikeType', visible: true, isModelProperty: true },
    { name: 'Imei candado', property: 'lock', visible: true, isModelProperty: true },
    { name: 'QR candado', property: 'lockQr', visible: true, isModelProperty: true },
    { name: 'Prestamos Realizados', property: 'startStation', visible: true, isModelProperty: true },
    { name: 'Estado', property: 'state', visible: true, isModelProperty: true },
    { name: 'Bateria', property: 'battery', visible: true, isModelProperty: true },
    { name: 'Estacion actual', property: 'station', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],

};

//Organizations

export const OrganizationTable: TableInfo = {
  tableColumns: [
    { name: 'Logo', property: 'logo', visible: true, isModelProperty: true, img:true },
    { name: 'Nombre', property: 'name', visible: true, isModelProperty: true },
    { name: 'Android', property: 'linkPlaystore', visible: true, isModelProperty: true, typeLink: true },
    { name: 'Apple', property: 'linkAppstore', visible: true, isModelProperty: true, typeLink: true},
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],

};

// Tarjetas Nfc

export const NfcCardsTable: TableInfo = {
  tableColumns: [
    { name: 'Numero', property: 'number', visible: true, isModelProperty: true },
    { name: 'Estado', property: 'state', visible: true, isModelProperty: true },
    { name: 'Usuario', property: 'user', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],

};

// Administradores del sistema

export const AdminSystemsTable: TableInfo = {
  tableColumns: [
    { name: 'Correo', property: 'email', visible: true, isModelProperty: true },
    { name: 'Nombre', property: 'name', visible: true, isModelProperty: true },
    { name: 'Cedula', property: 'dni', visible: true, isModelProperty: true },
    { name: 'Edad', property: 'age', visible: true, isModelProperty: true },
    { name: 'Cargo', property: 'post', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};

// Mapas

export const MapsTable: TableInfo = {
  tableColumns: [
    { name: 'Ver información la estación', property: 'informationStation', visible: true, isModelProperty: true },
    { name: 'Numero de bicicletas disponibles', property: 'bikesAvailable', visible: true, isModelProperty: true },
    { name: 'Tipo de bicicletas', property: 'tipoBikes', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};
// Estaciones

export const StationsTable: TableInfo = {
  tableColumns: [
    { name: 'Nombre', property: 'name', visible: true, isModelProperty: true },
    { name: 'Dirección', property: 'address', visible: true, isModelProperty: true },
    { name: 'Horarios de estación', property: 'openingTime', visible: true, isModelProperty: true, addHours: true},
    { name: 'Bicicletas eléctricas', property: 'electricBikes', visible: true, isModelProperty: true },
    { name: 'Bicicletas mecánicas', property: 'mechanicBikes', visible: true, isModelProperty: true },
    { name: 'Capacidad', property: 'bikesCapacity', visible: true, isModelProperty: true },
    { name: 'Estado', property: 'state', visible: true, isModelProperty: true, stateBoolean: true },
    { name: 'Actions', property: 'actions', visible: true},
  ],
  filterInclude: [
  ],
};

// Rebalanceo

export const RebalancingTable: TableInfo = {
  tableColumns: [
    { name: 'title 1', property: 'propertya', visible: true, isModelProperty: true },
    { name: 'title 1', property: 'propertyb', visible: true, isModelProperty: true },
    { name: 'title 1', property: 'propertyc', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};

// Configuración

export const SettingTable: TableInfo = {
  tableColumns: [
    { name: 'title 1', property: 'propertya', visible: true, isModelProperty: true },
    { name: 'title 1', property: 'propertyb', visible: true, isModelProperty: true },
    { name: 'title 1', property: 'propertyc', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};

// Tickets

export const TicketsTable: TableInfo = {
  tableColumns: [
    { name: 'title 1', property: 'propertya', visible: true, isModelProperty: true },
    { name: 'title 1', property: 'propertyb', visible: true, isModelProperty: true },
    { name: 'title 1', property: 'propertyc', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};

// Candados

export const LocksTable: TableInfo = {
  tableColumns: [
    { name: 'IMEI', property: 'imei', visible: true, isModelProperty: true },
    { name: 'QR', property: 'qr', visible: true, isModelProperty: true },
    { name: 'Estado de carga', property: 'status', visible: true, isModelProperty: true },
    { name: 'Estado (abierto/cerrado)', property: 'statusLock', visible: true, isModelProperty: true },
    { name: 'Fecha último comando', property: 'endComand', visible: true, isModelProperty: true },
    { name: 'Último comando enviado', property: 'endComandEnv', visible: true, isModelProperty: true },
    { name: 'Bicicleta', property: 'bike', visible: true, isModelProperty: true },
    { name: 'Mac', property: 'mac', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};

// Comando candados

export const ComandLocksTable: TableInfo = {
  tableColumns: [
    { name: 'Fecha', property: 'Date', visible: true, isModelProperty: true },
    { name: 'Imei', property: 'Imei', visible: true, isModelProperty: true },
    { name: 'Comando', property: 'Comand', visible: true, isModelProperty: true },
    { name: 'Mensaje', property: 'Messege', visible: true, isModelProperty: true },
    { name: 'Bateria', property: 'battery', visible: true, isModelProperty: true },
    { name: 'Estado', property: 'status', visible: true, isModelProperty: true },
    { name: 'Señal', property: 'señal', visible: true, isModelProperty: true },
    { name: 'Latitud', property: 'latitude', visible: true, isModelProperty: true },
    { name: 'Longitud', property: 'length', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ],
  filterInclude: [
  ],
};

export interface FormModalInput {
  formModel: FormModel;
  dispatchFormAction?: DispatchPosibleActions;
  errors$?: Observable<any>;
  data?: any;
}
