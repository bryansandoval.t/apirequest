import { Type } from '@angular/compiler/src/core';
import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Subject, Observable } from 'rxjs';

export interface InputOption {
    id: number;
    name: string;
}

export const errorDateValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
    if (Date.parse(formGroup.get('startDate').value) < Date.parse(formGroup.get('endDate').value))
        return null;
    else
        return { erorDate: true };
};

export interface FormInput {
    inputKey: string;
    targetModel?: string;
    value?: any;
    disablePartialValidation?: boolean;
    valueChangeSubject?: Subject<any>;
    front?: | {
        readonly?: boolean
        inputFlex?: fxResponsiveOptions
        inputFlexOffset?: fxResponsiveOptions
        label?: string;
        labelClass?: string; //only bootstrap classes the front has a global and specific class for all labels
        inputClass?: string; //only bootstrap classes the front has a global and specific class for all inputs
        class?: string; //only bootstrap classes the front has a global and specific class for all conteiners
        validationClasses?: { fail: string, success: string };
        typeText?:
        {
            type: "email" | "password" | "text" | "number" | "date" | "textarea" | "select" | "autocomplete";
            placeholder?: string;
            options?: Array<any>;
            optionsAsync?: Observable<Array<InputOption>>
        };
        typeFile?:
        {
            type: "file";
            buttonText?: string;
            buttonClass?: string; //only bootstrap classes the front has a global and specific class for all conteiners
            previewImages?: boolean;
            maxFiles?: number;
        }
    };
    validate?: emailValidate | numericalValidate | textValidate | dateValidate | fileValidate;
}

interface emailValidate {
    email: {
        isEmail?: boolean;
        required?: boolean;
        regExp?: RegExp;
        equality?: string;
    }
}

interface numericalValidate {
    numerical: {
        min?: number;
        max?: number;
        required?: boolean;
        regExp?: RegExp;
        equality?: string;
    }
}

interface textValidate {
    text: {
        minLength?: number;
        maxLength?: number;
        required?: boolean;
        regExp?: RegExp;
        equality?: string;
    }
}

interface dateValidate {
    date: {
        isPastDate?: boolean;
        isFutureDate?: boolean;
        isAdult?: boolean;
        required?: boolean;
        regExp?: RegExp;
        equality?: string;
    }
}

interface fileValidate {
    file: {
        required?: boolean;
        regExp?: RegExp;
    }
}

export interface FormModel {
    formKey: string;
    title?: string;
    titleClass?: string; //only bootstrap classes the front has a specific class
    titleFlex?: fxResponsiveOptions
    titleFlexOffset?: fxResponsiveOptions
    buttonText?: string;
    buttonClass?: string;//only bootstrap classes the front has a and specific class
    buttonFlex?: fxResponsiveOptions
    buttonFlexOffset?: fxResponsiveOptions
    buttonContainerClass?: string;
    inputs?: Array<FormInput>
}

export interface DispatchPosibleActions {
    success: Type,
    whereErrorsSave: string
}

export interface DispatchBrandActions {
    success: Type,
}

interface fxResponsiveOptions {
    fxFlex: string
    gt_lg?: string
    gt_md?: string
    gt_sm?: string
    gt_xs?: string
    lt_xl?: string
    lt_lg?: string
    lt_md?: string
    lt_sm?: string
    xl?: string
    lg?: string
    md?: string
    sm?: string
    xs?: string
}



export const defaultForm: FormModel = {
    formKey: "login",
    title: "Login",
    buttonText: "Sign In",
    buttonClass: "btn btn-dark mx-auto",
    titleClass: "text-center",
    inputs: [
        {
            inputKey: "email",
            front: {
                label: "Email",
                class: "col-12 my-3",
                typeText: { type: "text", placeholder: "Enter your email" }
            },
            validate: {
                email: {
                    isEmail: true,
                    required: true
                }
            }
        },
        {
            inputKey: "password",
            front: {
                label: "Password",
                class: "col-12 mb-3",
                typeText: { type: "password", placeholder: "Enter password" }
            },
            validate: {
                text: {
                    minLength: 8,
                    required: true
                },
            }
        }
    ]
}
