// Angular Imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { PageHeaderModule } from './../modules/core/page-header/page-header.module';
import { BreadcrumbsModule } from './../modules/core/breadcrumbs/breadcrumbs.module';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
// Component Imports
import { GeneralFormComponent } from './general-form/general-form.component';
import { TableComponent } from './ui/table/table.component';
import { ListModule } from '../modules/core/list/list.module';
import { CustomerCreateUpdateComponent } from './ui/table/customer-create-update/customer-create-update.component';
import { FormModalComponent } from './ui/table/form-modal/form-modal.component';
import { DataTableComponent } from './ui/data-table/data-table.component';
// Prime Imports
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { FilterService, PrimeNGConfig } from 'primeng/api';
import { ListComponent } from '../modules/core/list/list.component';

@NgModule({
  declarations: [
    GeneralFormComponent,
    TableComponent,
    ListComponent,
    CustomerCreateUpdateComponent,
    FormModalComponent,
    DataTableComponent
  ],
  providers: [FilterService, PrimeNGConfig],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatCardModule,
    MatListModule,
    TableModule,
    MultiSelectModule,
    DropdownModule,
    // Core
    PageHeaderModule,
    BreadcrumbsModule,
    ListModule,
  ],
  entryComponents: [
    CustomerCreateUpdateComponent,
    FormModalComponent
  ],
  exports: [
    GeneralFormComponent,
    TableComponent,
    DataTableComponent,
    ListComponent,
    CustomerCreateUpdateComponent,
    FormModalComponent,
    // Material
    FormsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatCardModule,
    PageHeaderModule,
    BreadcrumbsModule,
    MatListModule
  ]
})
export class GeneralModule { }
