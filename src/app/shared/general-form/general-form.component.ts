import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormModel, defaultForm, DispatchPosibleActions } from '../../interfaces/form.interfaces';
import { FormGroup, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { validateEmailPassword, partialValidationForm } from '../../config/global.actions';
import { switchMap, delay, tap, filter, takeUntil } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { ErrorStateMatcher } from '@angular/material/core';
import { componentDestroyed } from '../../utils/component-destroyed';

export class ErrorMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-general-form',
  templateUrl: './general-form.component.html',
  styleUrls: ['./general-form.component.scss']
})

export class GeneralFormComponent implements OnInit, OnDestroy {

  @Input() formModel: FormModel = defaultForm;
  @Input() dispatchFormAction?: DispatchPosibleActions;
  @Input() errors$: Observable<any> = of([]);

  matchers = {};

  formGroup: FormGroup = new FormGroup({});
  files = [];

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit(): void {
    this.renderForm();
  }

  renderForm() {
    this.formModel.buttonFlex = this.formModel.buttonFlex ? this.formModel.buttonFlex : { fxFlex: '100' }
    this.formModel.buttonFlexOffset = this.formModel.buttonFlexOffset ? this.formModel.buttonFlexOffset : { fxFlex: '0' }
    this.formModel.titleFlex = this.formModel.titleFlex ? this.formModel.titleFlex : { fxFlex: '100' }
    this.formModel.titleFlexOffset = this.formModel.titleFlexOffset ? this.formModel.titleFlexOffset : { fxFlex: '0' }
    this.formModel.inputs.forEach((input) => {
      input.front.inputFlex = input.front.inputFlex ? input.front.inputFlex : { fxFlex: '100' };
      input.front.inputFlexOffset = input.front.inputFlexOffset ? input.front.inputFlexOffset : { fxFlex: '0' };
      input.front.validationClasses = input.front.validationClasses ? input.front.validationClasses : { fail: '', success: '' };
      this.formGroup.addControl(input.inputKey, new FormControl(''));
      this.formGroup.controls[input.inputKey].setValue(input.value ? input.value : '');
      this.matchers[input.inputKey] = new ErrorMatcher();
      this.formGroup.controls[input.inputKey].valueChanges.pipe(
        takeUntil(componentDestroyed(this)),
        tap((event$) => {
          if (input.valueChangeSubject) {
            input.valueChangeSubject.next(event$);
          }
        }),
        filter(() => !input.disablePartialValidation),
        switchMap((event$) => of({}).pipe(
          delay(1000),
          tap((fun) => this.onChange(input.inputKey)))
        )
      ).subscribe();
    });
    this.errors$.pipe(
      takeUntil(componentDestroyed(this))
    ).subscribe((errors) => {
      for (const errorKey in errors) {
        this.formGroup.controls[errorKey].setErrors({ [errorKey]: errors[errorKey] })
      }
    });
  }

  ngOnDestroy() { }

  onSubmit() {
    this.store.dispatch(new validateEmailPassword({
      form: this.formGroup.value,
      formModel: this.formModel,
      dispatchFormAction: this.dispatchFormAction ? this.dispatchFormAction : null
    }));
  }

  onChange(input: string) {
    this.store.dispatch(new partialValidationForm({
      form: this.formGroup.value,
      formModel: this.formModel,
      input: input,
      dispatchFormAction: this.dispatchFormAction ? this.dispatchFormAction : null
    }));
  }

  receptFilesOutput(filesOutput, inputKey: string) {
    const filesAmount = filesOutput.length;
    for (let i = 0; i < filesAmount; i++) {
      this.files.push({
        image64: filesOutput[i].image
      });
      this.formGroup.controls[inputKey].setValue(this.files);
    }
  }
  optionDisplayWith(option) {
    return option.name;
  }
}