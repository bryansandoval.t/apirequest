import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { Column } from 'src/app/interfaces/table.interfaces';
import { tableTypes } from 'src/app/constants/table.constants';
import * as _ from 'lodash';


@Component({
  selector: 'elastic-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, OnChanges {

  //Declaracion de variables componente tabla
  @Input() columns: Column[];
  @Input() dataPage: any[];
  @Input() loading: boolean;
  @Input() totalRecords: number;
  @Input() pageSize: number;
  tableTypes: any = tableTypes;
  fixedData: any[];
  @Output() rowAction = new EventEmitter<any>();
  @Output() changePage = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.fixData();
  }

  ngOnChanges(changes: any) {
    this.fixData();
  }

  fixData(){
    let fixedData =[];
    this.dataPage.forEach(row=>{
      let newRow:any ={};
      newRow.dataRow = row;
      this.columns.forEach(col=>{
        newRow[col.propertyPath] = this.getObjectProperty(row, col.propertyPath);
        if(col.type == this.tableTypes.date || col.type == this.tableTypes.dateTime){
          if(col.dateFormater){
            newRow[col.propertyPath] = col.dateFormater(newRow[col.propertyPath]);
          }
        }
        if(col.type == this.tableTypes.number){
          if(col.decimals){
            newRow[col.propertyPath] = _.round(newRow[col.propertyPath],col.decimals);
          } 
          if(col.isCurrency){
            newRow[col.propertyPath] = "$"+newRow[col.propertyPath];
          }
        }
        if(col.type == this.tableTypes.label){
          let currentLabel = col.labels.find(label=>{
            return label.value == newRow[col.propertyPath];
          });
          if(currentLabel){
            newRow[col.propertyPath+'style'] = {
              'background-color':currentLabel.bgcolor,
              'color':currentLabel.fontColor
            };
            newRow[col.propertyPath] = currentLabel.label;
          }else{
            newRow[col.propertyPath+'style'] = {
              'background-color':'black',
              'color':'white'
            };
            newRow[col.propertyPath] = "Error";
          }
        }
      });
      fixedData.push(newRow);
    });
    //this.fixedData = fixedData;
    this.setFixedData(fixedData);
  }

  //Pseudo reducer
  setFixedData(fixedData){
    this.fixedData = fixedData;
  }

  getObjectProperty(object, path) {
    if (path === undefined || path === null) {
     return object;
    }
    const parts = path.split('.');
    return parts.reduce((object, key) => {
      if(!object){
        return undefined;
      }
      return object[key];
    }, object);
  }

  emitRowAction(row,type){
    let info = {row,type};
    this.rowAction.emit(info);
  }

  

  loadPage(event: LazyLoadEvent) {
      this.changePage.emit(event);
  }

}
