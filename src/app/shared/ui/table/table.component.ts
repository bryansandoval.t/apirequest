import { Component, Input, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListColumn } from '../../../modules/core/list/list-column.model';
import { ListDataSource } from '../../../modules/core/list/list-datasource';
import { ListDatabase } from '../../../modules/core/list/list-database';
import { componentDestroyed } from '../../../utils/component-destroyed';
import { filter, takeUntil } from 'rxjs/operators';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import { TableInfo } from '../../../interfaces/table.interfaces';
import { Store } from '@ngrx/store';
import { getTableData, getTableDataLength, UpdateData } from '../../../config/global.actions';
import { FormModalComponent } from './form-modal/form-modal.component';
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { ApiService } from '../../../services/api.service';
import { cloneDeep } from 'lodash-es';
import { ActiveUser } from 'src/app/config/global.actions';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {

  subject$: ReplaySubject<any> = new ReplaySubject<any>(1);
  data$: Observable<any>;
  users: any;

  @Input() tableData: any;
  @Input() tableName: string;
  @Input() tableDataSearch: any;
  @Input() columns: ListColumn[];
  @Input() addFormModalInput: FormModalInput;
  @Input() modifyFormModalInput: FormModalInput;
  @Input() filterInclude: any;
  @Input() filterProperty: any = {};
  @Input() emitTableEvents = false;
  @Output() create: EventEmitter<any> = new EventEmitter<any>();
  @Output() modify: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();
  @Output() manageMembers: EventEmitter<any> = new EventEmitter<any>();
  @Input() manageClub = false;
  @Output() approve: EventEmitter<any> = new EventEmitter<any>();

  dataInfo$: Observable<Array<TableInfo>>;
  resultsLength$: Observable<any>;
  dataInfoSearch$: Observable<Array<TableInfo>>;
  resultsLengthSearch$: Observable<any>;
  dataInfoSubscription: Subscription;
  resultsLengthSubscription: Subscription;
  dataInfoSearchSubscription: Subscription;
  resultsLengthSearchSubscription: Subscription;
  filterLength = 0;
  filterWhere: any;
  resultsInfo: any;

  route$ = this.store.select((state) => state.router.state.url)
  routeSubscription: Subscription;
  challengeSearch: boolean = false;
  trackingSearch: boolean = false;

  pageIndex = 0;
  pageSize = 10;
  dataSource: ListDataSource<any> | null;
  datasourceSearch: ListDataSource<any> | null;
  database: ListDatabase<any>;

  date;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private store: Store<any>,
    private apiService: ApiService) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }


  ngOnInit() {
    let date = new Date()
    this.date = date.toISOString();
    this.store.dispatch(new UpdateData({
      data: this.tableData.storeState,
      filter: {
        'where': this.filterProperty, 'limit': this.pageSize, 'skip': 0, 'include': this.filterInclude, order: ['created_at DESC'],
      },
      concatResult: false, storeState: this.tableData
    }));
    this.store.dispatch(new getTableDataLength({
      data: this.tableData.storeState,
      filter: this.filterProperty, storeState: { store: this.tableData.store, storeState: this.tableData.storeState + 'Count' }
    }));
    console.log(this.tableData.store)
    console.log(this.tableData.storeState)
    this.dataInfo$ = this.store.select((state) => state[this.tableData.store][this.tableData.storeState]);
    this.resultsLength$ = this.store.select((state) => state[this.tableData.store][this.tableData.storeState + 'Count']);
    this.dataInfoSubscription = this.dataInfo$.subscribe((elements1) => {
      this.resultsInfo = cloneDeep(elements1);
      if (this.challengeSearch) {
        this.resultsInfo.forEach((info) => {
          info['startDate'] = info['startDate'].split('Z')[0];
          info['endDate'] = info['endDate'].split('Z')[0];
        })
      }
      console.log(this.resultsInfo)
      this.dataSource = this.dataParse(this.resultsInfo);
    });
    this.routeSubscription = this.route$.subscribe((url) => {
      if (url == '/events/events' || url == '/events/challenges') {
        this.challengeSearch = true;
        let date = new Date()
        this.date = date.toISOString();
      }
      if (url == '/tracking/trackings') {
        this.trackingSearch = true;
      }
    })
  }

  updateItem(row){
  }

  dataParse(elements: any) {
    this.users = elements;
    this.subject$.next(this.users);
    this.data$ = this.subject$.asObservable();
    const database = new ListDatabase();
    this.data$.pipe(
      takeUntil(componentDestroyed(this)),
      filter<any>(Boolean)
    ).subscribe((users) => {
      this.users = users;
      database.dataChange.next(users);
    });
    return new ListDataSource(database, this.sort, this.paginator, this.columns);
  }

  createItem() {
    if (this.emitTableEvents) {
      this.create.emit();
    } else {
      this.dialog.open(FormModalComponent, {
        data: {
          formModel: this.addFormModalInput.formModel,
          dispatchFormAction: this.addFormModalInput.dispatchFormAction
        },
      }).afterClosed().subscribe((item: any) => {
        if (item) {
          this.users.unshift(item);
          this.subject$.next(this.users);
        }
      });
    }
  }

  deleteItem(item) {
    if (this.emitTableEvents) {
      const skip = this.pageIndex * this.pageSize;
      this.delete.emit({ item: item, skip: skip });
    } else {
      this.users.splice(this.users.findIndex((existingItem) => existingItem.id === item.id), 1);
      this.subject$.next(this.users);
    }
  }

  activeUser(item) {
    const user = { ...item.user };
    const userInfo = { ...item }
    delete userInfo.user;
    this.store.dispatch(new ActiveUser({ user: user, userInfo: userInfo }));
  }

  async onFilterChange(value) {
    this.filterLength = value.length;
    this.paginator.pageIndex = 0;
    if (!this.dataSource) {
      return;
    }
    if (this.filterLength > 2) {
      /*if (this.challengeSearch || this.trackingSearch) {
        //ChallengeSearch
        if (this.challengeSearch) {
          if (this.filterLength > 3) {
            this.filterChallenge(value);
            this.dataInfoSearch$ = this.store.select((state) => state[this.tableDataSearch.store][this.tableDataSearch.storeState]);
            this.resultsLengthSearch$ = this.store.select((state) =>
              state[this.tableDataSearch.store][this.tableDataSearch.storeState + 'Count']);
            this.dataInfoSearchSubscription = this.dataInfoSearch$.subscribe((elements1) => {
              let elements = cloneDeep(elements1);
              if (this.challengeSearch) {
                elements.forEach((info) => {
                  info['startDate'] = info['startDate'].split('Z')[0];
                  info['endDate'] = info['endDate'].split('Z')[0];
                })
              }
              this.datasourceSearch = this.dataParse(elements);
            });
          } else {
            this.datasource();
          }
        }
        //TrackingSearch
        if (this.trackingSearch) {
          await this.filterTrackings(value);
          this.dataInfoSearch$ = this.store.select((state) => state[this.tableDataSearch.store][this.tableDataSearch.storeState]);
          this.resultsLengthSearch$ = this.store.select((state) =>
            state[this.tableDataSearch.store][this.tableDataSearch.storeState + 'Count']);
          this.dataInfoSearchSubscription = this.dataInfoSearch$.subscribe((elements1) => {
            let elements = cloneDeep(elements1);
            this.datasourceSearch = this.dataParse(elements);
          });
        }
      } else {
        const filterProperties = this.columns.filter((elements) => { return elements.isModelProperty; }).map((data) => data.property);
        let filter = filterProperties.map((items8) => {
          return {
            and: [
              { [items8]: { 'like': value, 'options': 'i' } },
              { 'deleted_at': { inq: [null, false] } }
            ]
          };
        });
        this.filterWhere = { where: { or: filter }, skip: 0, limit: this.pageSize, include: this.filterInclude, order: ['created_at DESC'] };
        let filterCount = { or: filter };
        // Dispatch actions
        if (this.manageClub) {
          this.store.dispatch(new UpdateData({
            data: this.tableData.storeState, filter: this.filterWhere, concatResult: false, storeState: this.tableDataSearch
          }));
        } else {
          this.store.dispatch(new getTableData({
            data: this.tableData.storeState, filter: this.filterWhere, concatResult: false, storeState: this.tableDataSearch
          }));
        }
        this.store.dispatch(new getTableDataLength({
          data: this.tableData.storeState,
          filter: filterCount, storeState: { store: this.tableDataSearch.store, storeState: this.tableDataSearch.storeState + 'Count' }
        }));
        this.dataInfoSearch$ = this.store.select((state) => state[this.tableDataSearch.store][this.tableDataSearch.storeState]);
        this.resultsLengthSearch$ = this.store.select((state) =>
          state[this.tableDataSearch.store][this.tableDataSearch.storeState + 'Count']);
        this.dataInfoSearchSubscription = this.dataInfoSearch$.subscribe((elements) => {
          let elementos = cloneDeep(elements);
          if (this.challengeSearch) {
            elementos.forEach((info) => {
              info['startDate'] = info['startDate'].split('Z')[0];
              info['endDate'] = info['endDate'].split('Z')[0];
            })
          }
          if (this.filterLength >= 3) {
            this.datasourceSearch = this.dataParse(elementos);
          }
        });
      }*/
    } else {
      this.datasource();
    }
  }

  datasource() {
    if (this.challengeSearch) {
      this.resultsInfo.forEach((info) => {
        info['startDate'] = info['startDate'].split('Z')[0];
        info['endDate'] = info['endDate'].split('Z')[0];
      })
    }
    this.dataSource = this.dataParse(this.resultsInfo);
  }

  pagination(data) {
    this.pageIndex = data.pageIndex;
    const skip = this.pageIndex * this.pageSize;
    if (this.filterLength < 3) {
      this.store.dispatch(new UpdateData({
        data: this.tableData.storeState,
        filter: { 'where': this.filterProperty, 'limit': this.pageSize, 'skip': skip, 'include': this.filterInclude, order: ['created_at DESC'] },
        concatResult: true, storeState: { store: this.tableData.store, storeState: this.tableData.storeState }
      }));
    } else {
      this.filterWhere = { where: this.filterWhere.where, skip: skip, limit: this.pageSize };
      this.store.dispatch(new UpdateData({
        data: this.tableData.storeState,
        filter: this.filterWhere,
        concatResult: true,
        storeState: this.tableDataSearch
      }));
    }
  }

  ngOnDestroy() {
    this.dataInfoSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
    if (this.dataInfoSearchSubscription) {
      this.dataInfoSearchSubscription.unsubscribe();
    }
  }

  tryButton() {
  }

}
