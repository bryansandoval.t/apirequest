import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormModalInput } from 'src/app/interfaces/table.interfaces';
import { defaultForm } from 'src/app/interfaces/form.interfaces';
import { of } from 'rxjs';
import { Action } from '@ngrx/store';

@Component({
  selector: 'elastic-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss']
})
export class FormModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: FormModalInput,
    private dialogRef: MatDialogRef<FormModalComponent>,
  ) {
    defaults.formModel = defaults.formModel ? defaults.formModel : defaultForm;
    defaults.dispatchFormAction = defaults.dispatchFormAction ? defaults.dispatchFormAction : {
      success: testAction,
      whereErrorsSave: 'global.test'
    };
    defaults.errors$ = defaults.errors$ ? defaults.errors$ : of([]);
    defaults.data = defaults.data ? defaults.data : {};
  }

  ngOnInit() {
  }

}

class testAction implements Action {
  readonly type = 'Fail';
}
