import { Injectable } from '@angular/core';
import { FormModel } from '../interfaces/form.interfaces';
import { validate } from 'validate.js';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  validateForm(form$: any, formModel$: FormModel): any {
    const constraints = {};
    const form = { ...form$ };
    const formModel = { ...formModel$ };
    let errors: any = {};
    for (const inputIndex in form) {
      if (form[inputIndex] === '') {
        form[inputIndex] = null;
      }
    }
    for (const input of formModel.inputs) {
      if (input.validate) {
        const validationType = Object.keys(input.validate)[0];
        const validationObject = input.validate[validationType];
        for (let validationIndex in validationObject) {
          const constraint = this.transformToConstraints(validationIndex, validationObject[validationIndex], input.front.label || input.inputKey, constraints[input.inputKey]);
          if (constraint) {
            if (!constraints[input.inputKey]) constraints[input.inputKey] = {};
            constraints[input.inputKey] = Object.assign(constraints[input.inputKey], constraint)
          }
        }
      }
    }
    errors = validate(form, constraints)
    return errors ? errors : null;
  }

  private transformToConstraints(validationIndex, value, labelName: string, actualConstraint: any): any {
    let constraint: any = actualConstraint ? actualConstraint : {};
    if (value) {
      switch (validationIndex) {
        case 'isEmail':
          constraint.email = { message: `^No es un correo electrónico válido` };
          break;
        case 'required':
          constraint.presence = { message: `^El campo ${labelName.toLowerCase()} es requerido` };
          break;
        case 'regExp':
          constraint.format = {
            pattern: value,
            flags: 'i',
            message: `^El campo ${labelName.toLowerCase()} no cumple con las condiciones`
          };
          break;
        case 'equality':
          constraint.equality = {
            attribute: value,
            message: `^El campo ${labelName.toLowerCase()} no coincide`
          };
          break;
        case 'min':
        case 'max':
          const numericality = constraint.numericality ? constraint.numericality : {};
          let currentConstraint = {};
          if (validationIndex === 'max') {
            currentConstraint = { lessThan: value };
          } else if (validationIndex === 'min') { currentConstraint = { greaterThan: value }; }
          const numericalityConstraint = Object.assign(numericality, currentConstraint);
          constraint.numericality = Object.assign(numericalityConstraint, {
            message: this.numericalityMessage(numericalityConstraint, labelName)
          });
          break;
        case 'minLength':
        case 'maxLength':
          const lenght = constraint.lenght ? constraint.lenght : {};
          let currentConstraintLenght = {};
          if (validationIndex === 'manLenght') {
            currentConstraintLenght = { maxLength: value };
          } else if (validationIndex === 'minLenght') { currentConstraintLenght = { minLenght: value }; }
          const lenghtConstraint = Object.assign(lenght, currentConstraintLenght);
          constraint.length = Object.assign(lenghtConstraint, {
            message: this.lenghtMessage(lenghtConstraint, labelName)
          });
          break;
        case 'isPastDate':
        case 'isFutureDate':
        case 'isAdult':
          let date = new Date();
          if (validationIndex === 'isAdult') {
            date = new Date(date.getTime() - (31556900000) * 18);
          }// 18 años
          constraint.datetime = {
            latest: validationIndex === ('isPastDate' || 'isAdult') ? date : null,
            earliest: validationIndex === 'isFutureDate' ? date : null,
            message: `^You must select a valid date`
          };
          break;
      }
    }
    if (constraint === {}) { constraint = null; }
    return constraint;
  }

  numericalityMessage(constraints, label): string {
    if (constraints.greaterThan && constraints.lessThan) {
      return `^${label} must be between ${constraints.greaterThan} and ${constraints.lessThan} years`;
    } else if (constraints.lessThan) {
      return `^${label} must be less than ${constraints.lessThan} years`;
    } else if (constraints.greaterThan) {
      return `^${label} must be greater than ${constraints.greaterThan} years`;
    }
  }

  lenghtMessage(constraints, label): string {
    if (constraints.minLenght && constraints.maxLength) {
      return `^${label} must be between ${constraints.minLenght} and ${constraints.maxLength} characters`;
    } else if (constraints.minLenght) {
      return `^${label} must have more than ${constraints.lessThan} characters`;
    } else if (constraints.maxLength) {
      return `^${label} must have less than ${constraints.greaterThan} characters`;
    }
  }
}



