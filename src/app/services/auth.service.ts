import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { ApiService } from './api.service';
import { tap, map, exhaustMap, take } from 'rxjs/operators';
import { User } from '../models/user.model';
import * as _ from 'lodash';


interface registerUser {
  user: any;
  userInfo: any;
  token: any;
}

interface loginUser {
  user: any;
  token: any;
}

interface forgotPassword { }

interface restorePassword { }

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(
    private apiService: ApiService
  ) { }


  loginUser(form): Observable<loginUser> {
    const loginInfo = { user: form.email, password: form.password };

    return this.apiService.post('users/login', loginInfo, { useAuthHeaders: false, setCreateTime: false })
      .pipe(
        map((loginResponse) => loginResponse)
      );
  }

  getUserRoles(token: string, userInfo: any) {
    return this.apiService.get('users' + '/', { 'where': { 'id': userInfo.id }, 'include': [{ 'relation': 'userRoles' }] }, { token: token })
      .pipe(
        exhaustMap((userResponse) =>
          this.apiService.get('roles' + '/', {}, { token: token }).pipe(
            map((rolesResponse: any) => {
              if (!rolesResponse[0]) {
                throw throwError({
                  message: `Roles not found.`,
                  name: "NotFoundError",
                  statusCode: 404
                }).pipe(take(1)).subscribe();
              }
              const userAndRoles: any = {
                user: [...userResponse],
                roles: [...rolesResponse],
                token: token,
                password: userInfo.password
              };
              return userAndRoles;
            })
          )
        )
      );
  }

  getPermissionsData(roleIds: Array<any>) {
    return this.apiService.get('role-permissions' + '/', { 'where': { 'roleId': {'inq':roleIds} }})
      .pipe(
        exhaustMap((rolePermissionsResponse: any) =>
          this.apiService.get('permissions' + '/', {}).pipe(
            map((permissionsResponse: any) => {
              let access: any = [];
              rolePermissionsResponse.forEach((rolePermission: any) => {
                let permiso = permissionsResponse.find((permissions: any)=>permissions.id == rolePermission.permissionId)
                if(permiso){
                  access.push(permiso)
                }
              });
              access = _.uniqBy(access, 'id');
              return access
            })
          )
        )
      );
  }


  refreshToken(user: string, password: string) {
    return this.apiService.post('users/login', { user, password }, { useAuthHeaders: false, setCreateTime: false })
      .pipe(
        map((loginResponse) => loginResponse.token)
      );
  }


  forgotPassword(form): Observable<forgotPassword> {
    return this.apiService.post('resetpassword',
      { email: form.email },
      { useAuthHeaders: false, setCreateTime: false });
  }

  restorePassword(form): Observable<restorePassword> {
    return of({});
  }


}
