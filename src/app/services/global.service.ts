import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import Swal from 'sweetalert2';
import { SweetAlertIcon } from 'sweetalert2';

declare var require: any;
const CryptoJS = require('crypto-js');

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }

  public setStorage(variable: string, data: object) {
    const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), variable).toString();
    const cripVariableName = this.encryptingString(variable);
    localStorage.setItem(cripVariableName, ciphertext);
  }

  public getStorage(variable: string): Observable<any> {
    return new Observable((observer => {
      const cripVariableName = this.encryptingString(variable);
      const localStorageVariable = localStorage.getItem(cripVariableName);
      let decryptedData = null;
      if (localStorageVariable) {
        const bytes = CryptoJS.AES.decrypt(localStorageVariable, variable);
        decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      observer.next(decryptedData);
      observer.complete();
    }));
  }

  encryptingString(string: string): string {
    const enprictedLetters = [];
    for (let index = 0; index < string.length; index++) {
      enprictedLetters.push((-string.charCodeAt(index) & 0xFF));
      enprictedLetters.push((string.charCodeAt(index) - 5 & 0xFF));
      enprictedLetters.push((string.charCodeAt(index) + 5 & 0xFF));
      enprictedLetters.push((string.charCodeAt(index) + 2 & 0xFF));
      enprictedLetters.push((string.charCodeAt(index) - 2 & 0xFF));
    }
    return String.fromCharCode(...enprictedLetters);
  }

  public clearStorage() {
    localStorage.clear();
  }


  public removeStorage(key) {
    localStorage.removeItem(this.encryptingString(key));
  }

  /**
     * Show Sweet Alert
     * @param {string} type - Can be success, error, warning, info, question
     */
  showSweetAlert(title: string, text: string, type: SweetAlertIcon, buttonText: string): Observable<any> {
    return from(Swal.fire({
      title: title,
      html: text,
      icon: type,
      confirmButtonText: buttonText
    }))
  }

  topEndSweetAlert(text: string, type: SweetAlertIcon, time: number): Observable<any> {
    return from(Swal.fire({
      position: 'top-end',
      icon: type,
      title: text,
      showConfirmButton: false,
      timer: time
    }))
  }

  /**
    * Show Html Sweet Alert
    * 
    */
  htmlSweetAlert(html: string): Observable<any> {
    return from(Swal.fire({
      html: html,
      showCloseButton: true,
      showConfirmButton: false
    }))
  }

  defineTimeFormat(time) {
    if (typeof time === 'number') {
      let hour = (time / 60) | 0;
      let min = (((time / 60) % 1) * 60) | 0;
      if (hour > 12) {
        hour = hour - 12;
        return `${hour}:${min < 10 ? `0${min}` : min} PM`;
      } else {
        return `${hour}:${min < 10 ? `0${min}` : min} AM`;
      }
    } else if (typeof time === 'string') {
      const timeArray: Array<any> = time.split(" ")
      const defineMeridiem = timeArray[1]
      const timeHHMM = timeArray[0].split(":")
      if (defineMeridiem == 'PM') {
        const totalMin = timeHHMM[0] == '12' ? ((parseInt(timeHHMM[0]) * 60) + parseInt(timeHHMM[1]))
          : (((parseInt(timeHHMM[0]) + 12) * 60) + parseInt(timeHHMM[1]));
        return totalMin;
      } else {
        const totalMin = timeHHMM[0] == '12' ? parseInt(timeHHMM[1])
          : (parseInt(timeHHMM[0]) * 60) + parseInt(timeHHMM[1]);
        return totalMin;
      }
    }
  }

}
