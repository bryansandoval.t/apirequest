import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, switchMap, tap, take, filter } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import * as _ from 'lodash';

export interface postConfig {
  useAuthHeaders?: boolean;
  setCreateTime?: boolean;
  token?: string;
}

export interface putConfig {
  useAuthHeaders?: boolean;
  setUpdateTime?: boolean;
  setDeleteTime?: boolean;
  token?: string;
}

export interface getConfig {
  useAuthHeaders?: boolean;
  token?: string;
  count?: boolean;
}

export interface deleteConfig {
  useAuthHeaders?: boolean;
  token?: string;
}
import { Store } from '@ngrx/store';

const defaultPostConfig: postConfig = { useAuthHeaders: true, setCreateTime: true };
const defaultPutConfig: putConfig = { useAuthHeaders: true, setUpdateTime: true };
const defaultGetConfig: getConfig = { useAuthHeaders: true };
const defaultDeleteConfig: deleteConfig = { useAuthHeaders: true };

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private ENV = environment.apiUrl;
  private token$ = this.store$.select((state: any) => state.apiData.token);

  constructor(public http: HttpClient, public store$: Store) { }

  public post(url: string, dataInfo: any, config?: postConfig): Observable<any> {
    let data = _.cloneDeep(dataInfo)
    config = { ...defaultPostConfig, ...config };
    if (config.setCreateTime) {
      data.createdAt = new Date();
      data.updatedAt = data.createdAt;
    }
    if (config.useAuthHeaders) {
      return this.setAuthHeaders(config.token).pipe(
        switchMap(headers => this.http.post(`${this.ENV}${url}`, data, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.post(`${this.ENV}${url}`, data);
    }
  }

  public postImages(url: string, data: any, config?: postConfig): Observable<any> {
    config = { ...defaultPostConfig, ...config };
    if (config.useAuthHeaders) {
      return this.setAuthContentHeaders(config.token).pipe(
        switchMap(headers => this.http.post(`${this.ENV}${url}`, data, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.post(`${this.ENV}${url}`, data);
    }
  }

  public put(url: string, dataInfo: any, config?: putConfig): Observable<any> {
    let data = _.cloneDeep(dataInfo)
    config = { ...defaultPutConfig, ...config };
    if (config.setUpdateTime) {
      data.updatedAt = new Date();
    }
    if (config.setDeleteTime) {
      data.deletedAt = new Date();
    }
    if (config.useAuthHeaders) {
      return this.setAuthHeaders(config.token).pipe(
        switchMap(headers => this.http.put(`${this.ENV}${url}/${data.id}`, data, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.put(`${this.ENV}${url}/${data.id}`, data);
    }
  }

  public patch(url: string, dataInfo: any, config?: putConfig): Observable<any> {
    let data = _.cloneDeep(dataInfo)
    config = { ...defaultPutConfig, ...config };
    if (config.setUpdateTime) {
      data.updatedAt = new Date();
    }
    if (config.setDeleteTime) {
      data.deletedAt = new Date();
    }
    if (config.useAuthHeaders) {
      return this.setAuthHeaders(config.token).pipe(
        switchMap(headers => this.http.patch(`${this.ENV}${url}/${data.id}`, data, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.patch(`${this.ENV}${url}/${data.id}`, data);
    }
  }

  public 
  get(url: string, filter: any, config?: getConfig): Observable<any> {
    config = { ...defaultGetConfig, ...config };

    const fullUrl = config.count ? url + '/' + 'count' + '?where=' + JSON.stringify(filter) : url + '?filter=' + JSON.stringify(filter);

    if (config.useAuthHeaders) {
      return this.setAuthHeaders(config.token).pipe(
        switchMap(headers => this.http.get(`${this.ENV}${fullUrl}`, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.get(`${this.ENV}${fullUrl}`);
    }
  }

  public getById(url: string, id: string, config?: getConfig): Observable<any> {
    config = { ...defaultGetConfig, ...config };
    if (config.useAuthHeaders) {
      return this.setAuthHeaders(config.token).pipe(
        switchMap(headers => this.http.get(`${this.ENV}/${url}/${id}`, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.get(`${this.ENV}/${url}/${id}`);
    }
  }

  public delete(url: string, id: any, config?: deleteConfig): Observable<any> {
    config = { ...defaultDeleteConfig, ...config };
    if (config.useAuthHeaders) {
      return this.setAuthHeaders(config.token).pipe(
        switchMap(headers => this.http.delete(`${this.ENV}/${url}/${id}`, {
          headers: headers,
        })
        )
      );
    } else {
      return this.http.delete(`${this.ENV}/${url}/${id}`);
    }
  }

  public setAuthHeaders(token_: string): Observable<HttpHeaders> {
    if (!token_) {
      return this.token$.pipe(
        filter((token) => token),
        take(1),
        map((token) => {
          return new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          });
        })
      );
    } else {
      return of(new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token_}`,
      }));
    }
  }

  public setAuthContentHeaders(token_: string): Observable<HttpHeaders> {
    if (!token_) {
      return this.token$.pipe(
        filter((token) => token),
        take(1),
        map((token) => {
          return new HttpHeaders({
            //'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          });
        })
      );
    } else {
      return of(new HttpHeaders({
        //'Content-Type': 'application/json',
        Authorization: `Bearer ${token_}`,
      }));
    }
  }
}
