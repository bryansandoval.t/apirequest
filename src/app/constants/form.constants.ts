import { FormModel, FormInput } from '../interfaces/form.interfaces';
import { of, Subject } from 'rxjs';

const emailInput: FormInput = {
  inputKey: 'email',
  targetModel: 'user',
  front: {
    inputFlex: { fxFlex: '100' },
    label: 'Ingrese el correo electrónico',
    typeText: { type: 'email', placeholder: 'ejemplo@email.com' }
  },
  validate: {
    email: {
      isEmail: true,
      required: true
    }
  }
};

const passwordInput: FormInput = {
  inputKey: 'password',
  targetModel: 'user',
  front: {
    inputFlex: { fxFlex: '100' },
    label: 'Ingresa la contraseña',
    typeText: { type: 'password', placeholder: '********' }
  },
  validate: {
    text: {
      // regExp: new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{1,}'),
      minLength: 8,
      required: true
    },
  }
};

// Forms

export const loginFormModel: FormModel = {
  formKey: "login",
  buttonText: "Iniciar sesión",
  buttonClass: "btn btn-dark mx-auto",
  buttonFlex: { fxFlex: '100' },
  titleClass: "text-center",
  inputs: [
    emailInput,
    passwordInput
  ]
};

export const forgotPasswordFormModel: FormModel = {
  formKey: "forgot-password",
  title: "Forgot password",
  buttonText: "Send",
  buttonClass: "btn btn-dark mx-auto",
  titleClass: "text-center",
  inputs: [emailInput]
};

// Users

export const addUserFormModel: FormModel = {
  formKey: 'add-user',
  buttonText: 'Añadir usuario administrador',
  buttonClass: 'btn btn-dark mx-auto',
  titleClass: 'text-center',
  inputs: [
    {
      inputKey: 'name',
      targetModel: 'user-info',
      front: {
        label: 'Apellido',
        typeText: { type: 'text', placeholder: 'Apellido completo' }
      },
      validate: {
        text: {
          required: true
        }
      }
    },
    emailInput,
    passwordInput,
    {
      inputKey: 'rol',
      targetModel: 'user',
      front: {
        label: 'Seleccionar rol',
        typeText: {
          type: 'select',
          options: [
            { value: 'Admin', option: 'Administrador' },
            { value: 'User', option: 'Usuario' },
          ]
        }
      },
      validate: {
        text: {
          required: true
        }
      }
    }
  ]
};

export const modifyUserFormModel: FormModel = {
  ...addUserFormModel,
  formKey: "modify-user",
  buttonText: "Modificar usuario",
};





