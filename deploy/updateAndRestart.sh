#!/bin/bash

set -e

echo "Set Environment Production."
#export NODE_ENV=PROD

#pm2 stop "API PROD"
#pm2 delete "API PROD"
#pm2 status

rm -rf /home/bitnami/dashboard/bicycle_app_dashboard

echo "Cloning.."
#sleep 3

cd /home/bitnami/dashboard
git clone git@gitlab.com:kloustrdev/bicycle_app_dashboard.git
echo "Cloned"

cd /home/bitnami/dashboard/bicycle_app_dashboard

git checkout main

echo "Compile Web"
npm install

#export NODE_OPTIONS="--max-old-space-size=8192"
ng build --prod --build-optimizer false --base-href "/admin/" --source-map=false 
rm -rf /opt/bitnami/apache/htdocs/admin/*

cp -a dist/. /opt/bitnami/apache/htdocs/admin
echo "Server running"