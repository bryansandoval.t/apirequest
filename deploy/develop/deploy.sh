#!/bin/bash

# any future command that fails will exit the script
echo "deployment Bicycle Dashboard"
set -e
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

chmod +x ./deploy/develop/updateAndRestart.sh

echo "disable hostkey."
set -e
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

DEPLOY_SERVERS=$DEPLOY_SERVERS

ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL SERVERS ${ALL_SERVERS}"


for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  ssh bitnami@${server} 'bash -s' < ./deploy/develop/updateAndRestart.sh
done
