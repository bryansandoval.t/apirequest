#!/bin/bash

set -e

echo "Set Environment Develop Bicycle"
#export NODE_ENV=DEV

#pm2 stop "API DEV"
#pm2 delete "API DEV"
#pm2 status

rm -rf /home/bitnami/dashboard/bicycle_app_dashboard

echo "Cloning.."
cd /home/bitnami/dashboard
git clone git@gitlab.com:kloustrdev/bicycle_app_dashboard.git
echo "Cloned"

cd /home/bitnami/dashboard/bicycle_app_dashboard

git checkout develop

echo "Compile Web"
#npm install

#export NODE_OPTIONS="--max-old-space-size=8192"
#ng build --prod --no-aot --build-optimizer false --base-href "/admin/" --source-map=false 
#rm -rf /opt/bitnami/apache/htdocs/admin/*

#cp -a dist/. /opt/bitnami/apache/htdocs/admin
echo "Server running"
